package com.yuhhu.app.pays;

import com.yuhhu.app.util.IabHelper;
import com.yuhhu.app.util.IabResult;
import com.yuhhu.app.util.Inventory;
import com.yuhhu.app.util.Purchase;
import com.yuhhu.app.utils.Log;
import com.yuhhu.app.utils.PostClass;
import com.yuhhu.app.utils.Settings;

import org.json.JSONObject;

public class InAppOdeme {

    static final String ITEM_SKU = "android.test.purchased";  // ürün alımının başarılı olup olmadığını test etmek için kullanılıyor

    static final String TAG = "INAPP DENEME";
    static final int RC_REQUEST = 10001;

    public IabHelper mHelper;
    String urun;
    String krediid;

    public InAppOdeme(final String urun, String krediid) {
        this.urun = urun;
        this.krediid = krediid;

        start();
    }

    private void start(){
        String base64EncodedPublicKey = Settings.inapp_base64;

        mHelper = new IabHelper(Settings.activity, base64EncodedPublicKey);
        mHelper.enableDebugLogging(true);

        Log.d(TAG, "Starting setup.");
        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                Log.d(TAG, "Setup finished.");
                if (!result.isSuccess()) {
                    return;
                }

                // Have we been disposed of in the meantime? If so, quit.
                if (mHelper == null) return;

                // IAB is fully set up. Now, let's get an inventory of stuff we own.
                Log.d(TAG, "Setup successful. Querying inventory.");
                mHelper.queryInventoryAsync(mGotInventoryListener);
            }
        });
    }

    public void inappsatinal(String urun) {
        Log.d(TAG, "Upgrade button clicked; launching purchase flow for upgrade.");

        String payload = "";

        mHelper.launchPurchaseFlow(Settings.activity, urun, RC_REQUEST, mPurchaseFinishedListener, payload);   // item_sku yerine urun var
    }

    // Listener that's called when we finish querying the items and subscriptions we own
    IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
            Log.d(TAG, "Query inventory finished.");

            // Have we been disposed of in the meantime? If so, quit.
            if (mHelper == null) return;

            // Is it a failure?
            if (result.isFailure()) {
                Log.d(TAG, result.getMessage());
                return;
            }

            Log.d("satin", "ürün satın alma");
            inappsatinal(urun);

            Log.d(TAG, "Query inventory was successful.");

            Log.d(TAG, "Initial inventory query finished; enabling main UI.");

        }
    };

    // Callback for when a purchase is finished
    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
            Log.d(TAG, "Purchase finished: " + result + ", purchase: " + purchase);

            // if we were disposed of in the meantime, quit.
            if (mHelper == null) return;

            if (!verifyDeveloperPayload(purchase)) {
                return;
            }

            try {
                //alınan ürünün tüketilebilir olmasını sağlar(tekrar satın alınabilir)
                mHelper.consumeAsync(purchase, mConsumeFinishedListener);
            } catch (Exception e) {
            }

            Log.d(TAG, "Purchase successful.");
            urun_faturasini_gonder(purchase, false);

        }
    };

    public void urun_faturasini_gonder(final Purchase p, boolean restore) {
        if (p == null) return;

        String faturaid = "";
        String paket_adi = "";
        String tokenkodu = "";
        String json = "";
        String zaman = "";
        String signature = "";

        try {
            faturaid = p.getOrderId();
        } catch (Exception e) {
        }
        try {
            paket_adi = p.getPackageName();
        } catch (Exception e) {
        }
        try {
            tokenkodu = p.getToken();
        } catch (Exception e) {
        }
        try {
            json = p.getOriginalJson();
        } catch (Exception e) {
        }
        try {
            zaman = p.getPurchaseTime() + "";
        } catch (Exception e) {
        }
        try {
            signature = p.getSignature();
        } catch (Exception e) {
        }

        // faturayı upload et
        String postdata = "&sku=" + PostClass.encodeString(this.urun)
                + "&invoice=" + PostClass.encodeString(faturaid)
                + "&token=" + PostClass.encodeString(tokenkodu);

        PostClass.PostData("inapp/pay", postdata, new PostClass.OnDataLoaded() {
            @Override
            public void onDataLoad(Boolean success, String response) {
                if (success == true) {
                    JSONObject json = null;
                    JSONObject message = null;
                    try {
                        json = new JSONObject(response);

                        if (json.has("message")) {
                            message = json.getJSONObject("message");
                            final String mesaj = message.getString("text");
                            final String button = message.getString("button");

                            Settings.activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    new android.support.v7.app.AlertDialog.Builder(Settings.activity)
                                            .setMessage(mesaj)
                                            .setPositiveButton(button, null)
                                            .show();
                                }
                            });
                        }
                    } catch (Exception e) {
                    }
                }
            }
        });

    }

    IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
        public void onConsumeFinished(Purchase purchase, IabResult result) {
            Log.d(TAG, "Consumption finished. Purchase: " + purchase + ", result: " + result);

            if (mHelper == null) return;

            if (result.isSuccess()) {
                Log.d(TAG, "Consumption successful. Provisioning.");
            } else {
                //complain("Error while consuming: " + result);
            }
            Log.d(TAG, "End consumption flow.");
        }
    };

    /**
     * Verifies the developer payload of a purchase.
     */
    boolean verifyDeveloperPayload(Purchase p) {
        if (p == null) return false;
        String payload = p.getDeveloperPayload();

        return true;
    }

}