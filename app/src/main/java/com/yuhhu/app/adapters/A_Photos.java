package com.yuhhu.app.adapters;


import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;
import com.yuhhu.app.R;
import com.yuhhu.app.fontsUtils.FontsText;
import com.yuhhu.app.fragments.F_Tab5Profil;
import com.yuhhu.app.main.MainActivity;
import com.yuhhu.app.model.ModelGalery;
import com.yuhhu.app.utils.MyDialogCenter;
import com.yuhhu.app.utils.PostClass;
import com.yuhhu.app.utils.ScreenSize;
import com.yuhhu.app.utils.Settings;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class A_Photos extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<ModelGalery> photos;
    boolean canDelete;

    public A_Photos(List<ModelGalery> photos, boolean canDelete) {
        this.photos = photos;
        this.canDelete = canDelete;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(Settings.activity).inflate(R.layout.f_photos_item, parent, false);
        Typeface tf = Typeface.createFromAsset(Settings.activity.getAssets(), "fonts/Ubuntu-Regular.ttf");
        ViewGroup myMostParentLayout = (ViewGroup) view.findViewById(R.id.fontroot);
        FontsText.setFontToAllChilds(myMostParentLayout, tf);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final ViewHolder refHolder = (ViewHolder) holder;
        final ModelGalery row = photos.get(position);

        int photoWidth = row.getPhoto().getEn();
        int photoHeight = row.getPhoto().getBoy();

        int[] sizes = ScreenSize.getScreenResolution(Settings.activity);
        int width = sizes[0];

        int newWidth = width;
        int newHeight = (int) Math.floor((double) photoHeight * ((double) newWidth / (double) photoWidth));

        refHolder.image.getLayoutParams().width = newWidth;
        refHolder.image.getLayoutParams().height = newHeight;

        refHolder.image.setMaxScale(8);
        refHolder.progressBar.setVisibility(View.VISIBLE);
        Glide.with(Settings.activity)
                .load(row.getPhoto().getUrl())
                .asBitmap()
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap bitmap, GlideAnimation anim) {
                        refHolder.image.setImage(ImageSource.cachedBitmap(bitmap));
                        refHolder.progressBar.setVisibility(View.GONE);
                    }
                });

        if (canDelete)
            refHolder.imageEngelle.setVisibility(View.VISIBLE);

        refHolder.imageEngelle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<String> list = new ArrayList<>(Arrays.asList(Settings.dilBilgisi("btn_delete", Settings.activity.getString(R.string.btn_delete)), Settings.dilBilgisi("btn_give_up", Settings.activity.getString(R.string.btn_give_up)))); //butonlar
                MyDialogCenter dialog = new MyDialogCenter(Settings.activity, R.style.MyDialogCenter, "!", "", Settings.dilBilgisi("delete", Settings.activity.getString(R.string.delete)), list, new MyDialogCenter.OnActionButtonListener() {
                    @Override
                    public void onClickList(int pos) {
                        switch (pos) {
                            case 0:
                                sil(row.getId());
                                notifyItemRemoved(photos.indexOf(row));
                                photos.remove(photos.indexOf(row));
                                F_Tab5Profil.f.getData();

                                if (photos.size() < 1)
                                    MainActivity.main.onBackPressed();
                                break;
                            case 1:

                                break;
                        }
                    }

                    @Override
                    public void onClickListWithEdit(int position, String text) {

                    }
                });
                dialog.show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return photos.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imageEngelle;
        SubsamplingScaleImageView image;
        ProgressBar progressBar;

        public ViewHolder(View itemView) {
            super(itemView);
            image = (SubsamplingScaleImageView) itemView.findViewById(R.id.image);
            imageEngelle = (ImageView) itemView.findViewById(R.id.image_engelle);
            progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar);
        }
    }

    private void sil(int id) {
        String postdata = "&photo_id=" + PostClass.encodeString(String.valueOf(id));
        PostClass.PostData("user/delete_galery", postdata, new PostClass.OnDataLoaded() {
            @Override
            public void onDataLoad(Boolean success, String response) {
                if (success == true) {
                    JSONObject obj = null;
                    try {
                        obj = new JSONObject(response);
                        if (obj.has("data")) {
                            JSONObject data = obj.getJSONObject("data");
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

}
