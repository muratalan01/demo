package com.yuhhu.app.adapters;

import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.yuhhu.app.R;
import com.yuhhu.app.fontsUtils.FontsText;
import com.yuhhu.app.model.ModelTaslak;
import com.yuhhu.app.utils.Settings;

import java.util.List;

public class A_F_Main extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<ModelTaslak> taslaklar;

    public A_F_Main(List<ModelTaslak> taslaklar) {
        this.taslaklar = taslaklar;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(Settings.activity).inflate(R.layout.f_tab0_item, parent, false);
        Typeface tf = Typeface.createFromAsset(Settings.activity.getAssets(), "fonts/Ubuntu-Regular.ttf");
        ViewGroup myMostParentLayout = (ViewGroup) view.findViewById(R.id.fontroot);
        FontsText.setFontToAllChilds(myMostParentLayout, tf);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHolder refHolder = (ViewHolder) holder;
        final ModelTaslak row = taslaklar.get(position);
    }

    @Override
    public int getItemCount() {
        return taslaklar.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {

        public ViewHolder(View itemView) {
            super(itemView);

        }
    }

}

