package com.yuhhu.app.adapters;


import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.yuhhu.app.R;
import com.yuhhu.app.fontsUtils.FontsText;
import com.yuhhu.app.fragments.F_Tab1;
import com.yuhhu.app.fragments.F_Tab2Detay;
import com.yuhhu.app.main.MainActivity;
import com.yuhhu.app.model.ModelSearch;
import com.yuhhu.app.utils.MyDialogCenter;
import com.yuhhu.app.utils.PostClass;
import com.yuhhu.app.utils.ScreenSize;
import com.yuhhu.app.utils.Settings;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class A_Tab2 extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<ModelSearch> searches;

    OnLoadMoreListener loadMoreListener;
    boolean isLoading = false, isMoreDataAvailable = true;

    public A_Tab2(List<ModelSearch> searches) {
        this.searches = searches;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(Settings.activity).inflate(R.layout.f_tab2_item, parent, false);
        Typeface tf = Typeface.createFromAsset(Settings.activity.getAssets(), "fonts/Ubuntu-Regular.ttf");
        ViewGroup myMostParentLayout = (ViewGroup) view.findViewById(R.id.fontroot);
        FontsText.setFontToAllChilds(myMostParentLayout, tf);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        if(position>=getItemCount()-1 && isMoreDataAvailable && !isLoading && loadMoreListener!=null){
            isLoading = true;
            loadMoreListener.onLoadMore();
        }


        final ViewHolder refHolder = (ViewHolder) holder;
        final ModelSearch row = searches.get(position);

        int[] sizes = ScreenSize.getScreenResolution(Settings.activity);
        int width = sizes[0] / 2;
        int height = sizes[1] / 3;

        refHolder.image.getLayoutParams().width = width;
        refHolder.image.getLayoutParams().height = height;

        refHolder.progressBar.setVisibility(View.VISIBLE);
        Glide.with(Settings.activity)
                .load(row.getPhoto())
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        refHolder.progressBar.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        refHolder.progressBar.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(refHolder.image);

        refHolder.textName.setText(row.getName() + ", " + row.getAge());
        refHolder.textLocation.setText(row.getDistance());

        if (row.is_like())
            refHolder.image_like.setImageResource(R.drawable.icon_heart);
        else
            refHolder.image_like.setImageResource(R.drawable.icon_heart_empty);

        refHolder.image_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!row.is_like()) {
                    searches.get(position).setIs_like(true);
                    notifyItemChanged(position);
                    like(row.getId());
                } else {
                    searches.get(position).setIs_like(false);
                    notifyItemChanged(position);
                    unLike(row.getId());
                }

                F_Tab1.f.isRefreshFollow = true;
            }
        });

        refHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                F_Tab2Detay f = new F_Tab2Detay();
                f.user = String.valueOf(row.getId());

                MainActivity.main.barController.addFragment(f);
            }
        });

    }

    @Override
    public int getItemCount() {
        return searches.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        ImageView image, image_like;
        ProgressBar progressBar;
        TextView textName, textLocation;

        public ViewHolder(View itemView) {
            super(itemView);
            image = (ImageView)itemView.findViewById(R.id.image);
            image_like = (ImageView)itemView.findViewById(R.id.image_like);

            textName = (TextView)itemView.findViewById(R.id.text_name);
            textLocation = (TextView)itemView.findViewById(R.id.text_location);
            progressBar = (ProgressBar)itemView.findViewById(R.id.progressBar);
        }
    }

    private void like(int id){
        String postdata = "&user=" + PostClass.encodeString(String.valueOf(id));
        PostClass.PostData("profile/like", postdata, new PostClass.OnDataLoaded() {
            @Override
            public void onDataLoad(Boolean success, String response) {
                if (success == true) {
                    JSONObject obj = null;
                    try {
                        obj = new JSONObject(response);
                        if (obj.has("data")) {
                            JSONObject data = obj.getJSONObject("data");
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void unLike(int id){
        String postdata = "&user=" + PostClass.encodeString(String.valueOf(id));
        PostClass.PostData("profile/unLike", postdata, new PostClass.OnDataLoaded() {
            @Override
            public void onDataLoad(Boolean success, String response) {
                if (success == true) {
                    JSONObject obj = null;
                    try {
                        obj = new JSONObject(response);
                        if (obj.has("data")) {
                            JSONObject data = obj.getJSONObject("data");
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }


    public void setMoreDataAvailable(boolean moreDataAvailable) {
        isMoreDataAvailable = moreDataAvailable;
    }

    /* notifyDataSetChanged is final method so we can't override it
      call adapter.notifyDataChanged(); after update the list
      */
    public void notifyDataChanged(){
        notifyDataSetChanged();
        isLoading = false;
    }

    public interface OnLoadMoreListener{
        void onLoadMore();
    }

    public void setLoadMoreListener(OnLoadMoreListener loadMoreListener) {
        this.loadMoreListener = loadMoreListener;
    }

}

