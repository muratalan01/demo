package com.yuhhu.app.adapters;

import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.target.Target;
import com.yuhhu.app.R;
import com.yuhhu.app.fontsUtils.FontsText;
import com.yuhhu.app.fragments.F_Comment;
import com.yuhhu.app.fragments.F_MediaDetay;
import com.yuhhu.app.fragments.F_Tab2Detay;
import com.yuhhu.app.fragments.F_Tab5Profil;
import com.yuhhu.app.main.MainActivity;
import com.yuhhu.app.model.ModelPost;
import com.yuhhu.app.utils.MyDialogCenter;
import com.yuhhu.app.utils.PostClass;
import com.yuhhu.app.utils.Settings;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class A_TabPosts extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<ModelPost> posts;

    OnLoadMoreListener loadMoreListener;
    boolean isLoading = false, isMoreDataAvailable = true;

    /*
        post gönderilerindeki yorum sayısının güncelleneceği sayfayı belirtmek için
       1 -> F_Tab1 deki herkes kısmının yorum sayısı yenilenecek
       2 -> F_Tab1 deki favori kısmının yorum sayısı yenilenecek
       3 -> F_Tab2detay deki yorum sayısı yenilenecek
       4 -> F_Tab5Profil deki yorum sayısı yenilenecek
    */
    int refPageCommentCount = 0 ;

    public A_TabPosts(List<ModelPost> posts, int refPageCommentCount) {
        this.posts = posts;
        this.refPageCommentCount = refPageCommentCount;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(Settings.activity).inflate(R.layout.f_tab1_item, parent, false);
        Typeface tf = Typeface.createFromAsset(Settings.activity.getAssets(), "fonts/Ubuntu-Regular.ttf");
        ViewGroup myMostParentLayout = (ViewGroup) view.findViewById(R.id.fontroot);
        FontsText.setFontToAllChilds(myMostParentLayout, tf);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        if(position>=getItemCount()-1 && isMoreDataAvailable && !isLoading && loadMoreListener!=null){
            isLoading = true;
            loadMoreListener.onLoadMore();
        }


        final ViewHolder refHolder = (ViewHolder) holder;
        final ModelPost row = posts.get(position);

        Glide.with(Settings.activity).load(row.getUser().getUrl())
                .asBitmap()
                .placeholder(R.drawable.user_placeholder_cycle)
                .into(new BitmapImageViewTarget(refHolder.imageUser) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(Settings.activity.getResources(), resource);
                        circularBitmapDrawable.setCircular(true);
                        refHolder.imageUser.setImageDrawable(circularBitmapDrawable);
                    }
                });

        if (row.getUrl().equals("")){
            refHolder.imagePost.setVisibility(View.GONE);
            refHolder.progressBar.setVisibility(View.GONE);
        }else {
            refHolder.imagePost.setVisibility(View.VISIBLE);
            refHolder.progressBar.setVisibility(View.VISIBLE);
            Glide.with(Settings.activity)
                    .load(row.getUrl())
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            refHolder.progressBar.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            refHolder.progressBar.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(refHolder.imagePost);
        }


        refHolder.textName.setText(row.getUser().getName());
        refHolder.textPost.setText(row.getText());
        refHolder.textTime.setText(row.getTime());

        if (row.getText().equals(""))
            refHolder.textPost.setVisibility(View.GONE);
        else
            refHolder.textPost.setVisibility(View.VISIBLE);

        refHolder.textLike.setText( Settings.dilBilgisi("like_count", Settings.activity.getString(R.string.like_count)).replace("[]", String.valueOf(row.getLike_count())) );
        refHolder.textComment.setText( Settings.dilBilgisi("comment_count", Settings.activity.getString(R.string.comment_count)).replace("[]", String.valueOf(row.getComment_count())) );

        if (row.is_like())
            refHolder.imageLike.setImageResource(R.drawable.icon_heart);
        else
            refHolder.imageLike.setImageResource(R.drawable.icon_heart_empty);

        refHolder.imageLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!row.is_like()) {
                    posts.get(position).setIs_like(true);
                    posts.get(position).setLike_count(posts.get(position).getLike_count() + 1);
                    notifyItemChanged(position);
                    like(row.getId());
                }else {
                    posts.get(position).setIs_like(false);
                    posts.get(position).setLike_count(posts.get(position).getLike_count() - 1);
                    notifyItemChanged(position);
                    unLike(row.getId());
                }
            }
        });

        refHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                F_Comment f = new F_Comment();
                f.postId = row.getId();
                f.postPosition = position;
                f.refPageCommentCount = refPageCommentCount;

                Settings.fragmentActivity.getSupportFragmentManager().beginTransaction().replace(R.id.container_extra, f).addToBackStack("").commit();
            }
        });

        refHolder.r_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (refPageCommentCount == 3 || refPageCommentCount == 4) return;

                F_Tab2Detay f = new F_Tab2Detay();
                f.user = String.valueOf(row.getUser().getId());

                MainActivity.main.barController.addFragment(f);
            }
        });

        refHolder.imagePost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                F_MediaDetay f = new F_MediaDetay();
                f.photo = row.getUrl();
                f.comment = row.getText();

                Settings.fragmentActivity.getSupportFragmentManager().beginTransaction().replace(R.id.container_extra, f).addToBackStack("").commit();
            }
        });

        refHolder.imagePost.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (row.getUser().getId() != Settings.user.getId()) return false;

                List<String> list = new ArrayList<>(Arrays.asList(Settings.dilBilgisi("btn_delete", Settings.activity.getString(R.string.btn_delete)), Settings.dilBilgisi("btn_give_up", Settings.activity.getString(R.string.btn_give_up)))); //butonlar
                MyDialogCenter dialog = new MyDialogCenter(Settings.activity, R.style.MyDialogCenter, "!", "", Settings.dilBilgisi("title_post_delete", Settings.activity.getString(R.string.title_post_delete)), list, new MyDialogCenter.OnActionButtonListener() {
                    @Override
                    public void onClickList(int pos) {
                        switch (pos) {
                            case 0:
                                sil(row.getId());
                                notifyItemRemoved(posts.indexOf(row));
                                posts.remove(posts.indexOf(row));
                                break;
                            case 1:

                                break;
                        }
                    }

                    @Override
                    public void onClickListWithEdit(int position, String text) {

                    }
                });
                dialog.show();

                return false;
            }
        });

        refHolder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (row.getUser().getId() != Settings.user.getId()) return false;

                List<String> list = new ArrayList<>(Arrays.asList(Settings.dilBilgisi("btn_delete", Settings.activity.getString(R.string.btn_delete)), Settings.dilBilgisi("btn_give_up", Settings.activity.getString(R.string.btn_give_up)))); //butonlar
                MyDialogCenter dialog = new MyDialogCenter(Settings.activity, R.style.MyDialogCenter, "!", "", Settings.dilBilgisi("title_post_delete", Settings.activity.getString(R.string.title_post_delete)), list, new MyDialogCenter.OnActionButtonListener() {
                    @Override
                    public void onClickList(int pos) {
                        switch (pos) {
                            case 0:
                                sil(row.getId());
                                notifyItemRemoved(posts.indexOf(row));
                                posts.remove(posts.indexOf(row));
                                break;
                            case 1:

                                break;
                        }
                    }

                    @Override
                    public void onClickListWithEdit(int position, String text) {

                    }
                });
                dialog.show();

                return false;
            }
        });

    }

    @Override
    public int getItemCount() {
        return posts.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageUser, imagePost, imageLike;
        ProgressBar progressBar;
        TextView textPost, textName, textLike, textComment, textTime;

        RelativeLayout r_user;

        public ViewHolder(View itemView) {
            super(itemView);
            imageUser = (ImageView)itemView.findViewById(R.id.image_user);
            imagePost = (ImageView)itemView.findViewById(R.id.image_post);
            imageLike = (ImageView)itemView.findViewById(R.id.image_like);

            textPost = (TextView)itemView.findViewById(R.id.text_post);
            textName = (TextView)itemView.findViewById(R.id.text_name);
            textLike = (TextView)itemView.findViewById(R.id.text_like);
            textComment = (TextView)itemView.findViewById(R.id.text_comment);
            textTime = (TextView)itemView.findViewById(R.id.text_time);

            progressBar = (ProgressBar)itemView.findViewById(R.id.progressBar);

            r_user = (RelativeLayout)itemView.findViewById(R.id.r_user);
        }
    }

    private void like(int id){
        String postdata = "&post_id=" + PostClass.encodeString(String.valueOf(id));
        PostClass.PostData("user/post_like", postdata, new PostClass.OnDataLoaded() {
            @Override
            public void onDataLoad(Boolean success, String response) {
                if (success == true) {
                    JSONObject obj = null;
                    try {
                        obj = new JSONObject(response);
                        if (obj.has("data")) {
                            JSONObject data = obj.getJSONObject("data");
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void unLike(int id){
        String postdata = "&post_id=" + PostClass.encodeString(String.valueOf(id));
        PostClass.PostData("user/post_unLike", postdata, new PostClass.OnDataLoaded() {
            @Override
            public void onDataLoad(Boolean success, String response) {
                if (success == true) {
                    JSONObject obj = null;
                    try {
                        obj = new JSONObject(response);
                        if (obj.has("data")) {
                            JSONObject data = obj.getJSONObject("data");
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void sil(int id){
        String postdata = "&post_id=" + PostClass.encodeString(String.valueOf(id));
        PostClass.PostData("user/post_delete", postdata, new PostClass.OnDataLoaded() {
            @Override
            public void onDataLoad(Boolean success, String response) {
                if (success == true) {
                    JSONObject obj = null;
                    try {
                        obj = new JSONObject(response);
                        if (obj.has("data")) {
                            JSONObject data = obj.getJSONObject("data");
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    public void setMoreDataAvailable(boolean moreDataAvailable) {
        isMoreDataAvailable = moreDataAvailable;
    }

    /* notifyDataSetChanged is final method so we can't override it
      call adapter.notifyDataChanged(); after update the list
      */
    public void notifyDataChanged(){
        notifyDataSetChanged();
        isLoading = false;
    }

    public interface OnLoadMoreListener{
        void onLoadMore();
    }

    public void setLoadMoreListener(OnLoadMoreListener loadMoreListener) {
        this.loadMoreListener = loadMoreListener;
    }

}

