package com.yuhhu.app.adapters;

import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.yuhhu.app.R;
import com.yuhhu.app.fontsUtils.FontsText;
import com.yuhhu.app.model.ModelProduct;
import com.yuhhu.app.pays.InAppOdeme;
import com.yuhhu.app.utils.Settings;

import java.util.List;

public class A_F_Product extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<ModelProduct> products;

    public A_F_Product(List<ModelProduct> products) {
        this.products = products;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(Settings.activity).inflate(R.layout.f_product_item, parent, false);
        Typeface tf = Typeface.createFromAsset(Settings.activity.getAssets(), "fonts/Ubuntu-Regular.ttf");
        ViewGroup myMostParentLayout = (ViewGroup) view.findViewById(R.id.fontroot);
        FontsText.setFontToAllChilds(myMostParentLayout, tf);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        ViewHolder refHolder = (ViewHolder) holder;
        final ModelProduct row = products.get(position);

        refHolder.text.setText(row.getText());

        refHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InAppOdeme odeme = new InAppOdeme(products.get(position).getSku(), "");
                Settings.inappodeme = odeme;
            }
        });
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {

        TextView text;

        public ViewHolder(View itemView) {
            super(itemView);
            text = (TextView) itemView.findViewById(R.id.text_title);
        }
    }

}

