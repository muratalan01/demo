package com.yuhhu.app.adapters;


import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.yuhhu.app.R;
import com.yuhhu.app.fontsUtils.FontsText;
import com.yuhhu.app.fragments.F_Photos;
import com.yuhhu.app.model.ModelGalery;
import com.yuhhu.app.utils.ScreenSize;
import com.yuhhu.app.utils.Settings;

import java.util.ArrayList;
import java.util.List;

public class A_Tab2Detay extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    ArrayList<ModelGalery> galeries;

    public A_Tab2Detay(ArrayList<ModelGalery> galeries) {
        this.galeries = galeries;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(Settings.activity).inflate(R.layout.f_tab2_detay_item, parent, false);
        Typeface tf = Typeface.createFromAsset(Settings.activity.getAssets(), "fonts/Ubuntu-Regular.ttf");
        ViewGroup myMostParentLayout = (ViewGroup) view.findViewById(R.id.fontroot);
        FontsText.setFontToAllChilds(myMostParentLayout, tf);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final ViewHolder refHolder = (ViewHolder) holder;
        final ModelGalery row = galeries.get(position);

        int[] sizes = ScreenSize.getScreenResolution(Settings.activity);
        int width = sizes[0] / 3;
        int height = sizes[1] / 4;

        refHolder.image.getLayoutParams().width = width;
        refHolder.image.getLayoutParams().height = height;

        refHolder.progressBar.setVisibility(View.VISIBLE);
        Glide.with(Settings.activity)
                .load(row.getPhoto().getUrl())
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        refHolder.progressBar.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        refHolder.progressBar.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(refHolder.image);

        refHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                F_Photos f = new F_Photos();

                f.galeries = (ArrayList<ModelGalery>) galeries.clone();
                f.position = position;

                Settings.fragmentActivity.getSupportFragmentManager().beginTransaction().replace(R.id.container_extra, f).addToBackStack("").commit();
            }
        });
    }

    @Override
    public int getItemCount() {
        return galeries.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        ProgressBar progressBar;
        public ViewHolder(View itemView) {
            super(itemView);
            image = (ImageView)itemView.findViewById(R.id.image);
            progressBar = (ProgressBar)itemView.findViewById(R.id.progressBar);
        }
    }

}
