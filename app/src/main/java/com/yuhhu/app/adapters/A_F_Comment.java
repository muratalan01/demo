package com.yuhhu.app.adapters;

import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.yuhhu.app.R;
import com.yuhhu.app.fontsUtils.FontsText;
import com.yuhhu.app.fragments.F_Tab2Detay;
import com.yuhhu.app.main.MainActivity;
import com.yuhhu.app.model.ModelComment;
import com.yuhhu.app.utils.Settings;

import java.util.List;

public class A_F_Comment extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<ModelComment> comments;

    public A_F_Comment(List<ModelComment> comments) {
        this.comments = comments;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(Settings.activity).inflate(R.layout.f_comment_item, parent, false);
        Typeface tf = Typeface.createFromAsset(Settings.activity.getAssets(), "fonts/Ubuntu-Regular.ttf");
        ViewGroup myMostParentLayout = (ViewGroup) view.findViewById(R.id.fontroot);
        FontsText.setFontToAllChilds(myMostParentLayout, tf);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final ViewHolder refHolder = (ViewHolder) holder;
        final ModelComment row = comments.get(position);

        Glide.with(Settings.activity).load(row.getUser().getUrl())
                .asBitmap()
                .placeholder(R.drawable.user_placeholder_cycle)
                .into(new BitmapImageViewTarget(refHolder.imageUser) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(Settings.activity.getResources(), resource);
                        circularBitmapDrawable.setCircular(true);
                        refHolder.imageUser.setImageDrawable(circularBitmapDrawable);
                    }
                });

        refHolder.textName.setText(row.getUser().getName());
        refHolder.textTime.setText(row.getTime());
        refHolder.textComment.setText(row.getText());

        refHolder.r_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                F_Tab2Detay f = new F_Tab2Detay();
                f.user = String.valueOf(row.getUser().getId());

                MainActivity.main.onBackPressed();
                MainActivity.main.barController.addFragment(f);
            }
        });
    }

    @Override
    public int getItemCount() {
        return comments.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imageUser;
        TextView textName, textTime, textComment;

        RelativeLayout r_user;

        public ViewHolder(View itemView) {
            super(itemView);
            imageUser = (ImageView)itemView.findViewById(R.id.image_user);

            textName = (TextView)itemView.findViewById(R.id.text_name);
            textTime = (TextView)itemView.findViewById(R.id.text_time);
            textComment = (TextView)itemView.findViewById(R.id.text_comment);

            r_user = (RelativeLayout)itemView.findViewById(R.id.r_user);
        }
    }

}

