package com.yuhhu.app.adapters;


import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.yuhhu.app.R;
import com.yuhhu.app.fontsUtils.FontsText;
import com.yuhhu.app.fragments.F_MesajDetay;
import com.yuhhu.app.model.ModelMessagesLast;
import com.yuhhu.app.utils.Settings;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class A_Tab3 extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<ModelMessagesLast> messageLists;
    String date;

    public A_Tab3(List<ModelMessagesLast> messageLists) {
        this.messageLists = messageLists;
        date = getDateTime();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(Settings.activity).inflate(R.layout.fragment_messages_item, parent, false);
        Typeface tf = Typeface.createFromAsset(Settings.activity.getAssets(), "fonts/Ubuntu-Regular.ttf");
        ViewGroup myMostParentLayout = (ViewGroup) view.findViewById(R.id.fontroot);
        FontsText.setFontToAllChilds(myMostParentLayout, tf);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final ViewHolder refHolder = (ViewHolder) holder;
        final ModelMessagesLast row = messageLists.get(position);

        Glide.with(Settings.activity).load(row.getPicture())
                .asBitmap()
                .placeholder(R.drawable.user_placeholder_cycle)
                .into(new BitmapImageViewTarget(refHolder.imageWho) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(Settings.activity.getResources(), resource);
                        circularBitmapDrawable.setCircular(true);
                        refHolder.imageWho.setImageDrawable(circularBitmapDrawable);
                    }
                });

        if (!row.isUnread()) {
            refHolder.textName.setTypeface(null, Typeface.NORMAL);
        } else {
            refHolder.textName.setTypeface(null, Typeface.BOLD);
        }

        refHolder.textName.setText(row.getName());
        refHolder.textMessage.setText(row.getMessage());
        refHolder.textTime.setText(getTime(row.getTime()));

        refHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                F_MesajDetay f = new F_MesajDetay();
                f.id = row.getId();
                f.name = row.getName();
                f.photo = row.getPicture();

                Settings.fragmentActivity.getSupportFragmentManager().beginTransaction().replace(R.id.container_extra, f).addToBackStack("").commit();
            }
        });
    }

    @Override
    public int getItemCount() {
        return messageLists.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imageWho;
        TextView textName, textMessage, textTime;

        public ViewHolder(View itemView) {
            super(itemView);
            imageWho = (ImageView) itemView.findViewById(R.id.image_who);

            textName = (TextView) itemView.findViewById(R.id.text_user_name);
            textMessage = (TextView) itemView.findViewById(R.id.text_message);
            textTime = (TextView) itemView.findViewById(R.id.text_time);
        }
    }

    private String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

    private String getTime(Integer ms) {
        Calendar today=Calendar.getInstance();
        today=clearTimes(today);

        if(ms * 1000L >today.getTimeInMillis()) {
            Date date = new Date(ms * 1000L);
            SimpleDateFormat dateformat = new SimpleDateFormat("HH:mm");
            return dateformat.format(date);
        } else {
            Date date = new Date(ms * 1000L);
            SimpleDateFormat dateformat = new SimpleDateFormat("dd.MM.yyyy");
            return dateformat.format(date);
        }
    }

    private  Calendar clearTimes(Calendar c) {
        c.set(Calendar.HOUR_OF_DAY,0);
        c.set(Calendar.MINUTE,0);
        c.set(Calendar.SECOND,0);
        c.set(Calendar.MILLISECOND,0);

        return c;
    }

}

