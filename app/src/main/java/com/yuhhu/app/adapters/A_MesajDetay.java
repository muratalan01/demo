package com.yuhhu.app.adapters;


import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.yuhhu.app.R;
import com.yuhhu.app.fontsUtils.FontsText;
import com.yuhhu.app.model.ModelMessage;
import com.yuhhu.app.utils.Settings;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class A_MesajDetay extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final int VIEW_TYPE_LEFT = 0;
    private final int VIEW_TYPE_RIGHT = 1;

    List<ModelMessage.MessageList> messageLists;
    String picture, name;
    String date;

    @Override
    public int getItemViewType(int position) {
        int type;
        if (messageLists.get(position).getType().equals("out")) {
            type = VIEW_TYPE_RIGHT;
        } else {
            type = VIEW_TYPE_LEFT;
        }
        return type;
    }

    public A_MesajDetay(List<ModelMessage.MessageList> messageLists, String picture, String name) {
        this.messageLists = messageLists;
        this.picture = picture;
        this.name = name;
        date = getDateTime();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == 0) {
            View view = LayoutInflater.from(Settings.activity).inflate(R.layout.fragment_messages_detay_item_left, parent, false);
            Typeface tf = Typeface.createFromAsset(Settings.activity.getAssets(), "fonts/Ubuntu-Regular.ttf");
            ViewGroup myMostParentLayout = (ViewGroup) view.findViewById(R.id.fontroot);
            FontsText.setFontToAllChilds(myMostParentLayout, tf);

            return new ViewHolderLeft(view);
        } else if (viewType == 1) {
            View view = LayoutInflater.from(Settings.activity).inflate(R.layout.fragment_messages_detay_item_right, parent, false);
            Typeface tf = Typeface.createFromAsset(Settings.activity.getAssets(), "fonts/Ubuntu-Regular.ttf");
            ViewGroup myMostParentLayout = (ViewGroup) view.findViewById(R.id.fontroot);
            FontsText.setFontToAllChilds(myMostParentLayout, tf);

            return new ViewHolderRight(view);
        }

        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolderLeft) {
            final ViewHolderLeft refHolder = (ViewHolderLeft) holder;
            final ModelMessage.MessageList row = messageLists.get(position);

            Glide.with(Settings.activity).load(picture)
                    .asBitmap()
                    .placeholder(R.drawable.user_placeholder_cycle)
                    .into(new BitmapImageViewTarget(refHolder.imageWho) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(Settings.activity.getResources(), resource);
                            circularBitmapDrawable.setCircular(true);
                            refHolder.imageWho.setImageDrawable(circularBitmapDrawable);
                        }
                    });

            refHolder.textMesaj.setText(row.getMessage());
            refHolder.textTime.setText(getTime(row.getTime()));

        } else if (holder instanceof ViewHolderRight) {
            final ViewHolderRight refHolder = (ViewHolderRight) holder;
            final ModelMessage.MessageList row = messageLists.get(position);

            Glide.with(Settings.activity).load(Settings.user.getPhoto())
                    .asBitmap()
                    .placeholder(R.drawable.user_placeholder_cycle)
                    .into(new BitmapImageViewTarget(refHolder.imageWho) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(Settings.activity.getResources(), resource);
                            circularBitmapDrawable.setCircular(true);
                            refHolder.imageWho.setImageDrawable(circularBitmapDrawable);
                        }
                    });

            refHolder.textMesaj.setText(row.getMessage());
            refHolder.textTime.setText(getTime(row.getTime()));
        }
    }

    @Override
    public int getItemCount() {
        return messageLists.size();
    }

    private class ViewHolderLeft extends RecyclerView.ViewHolder {
        TextView textMesaj, textTime;
        ImageView imageWho;

        public ViewHolderLeft(View itemView) {
            super(itemView);
            imageWho = (ImageView) itemView.findViewById(R.id.image_who);

            textMesaj = (TextView) itemView.findViewById(R.id.text_mesaj);
            textTime = (TextView) itemView.findViewById(R.id.text_time);
        }
    }

    private class ViewHolderRight extends RecyclerView.ViewHolder {
        TextView textMesaj, textTime;
        ImageView imageWho;

        public ViewHolderRight(View itemView) {
            super(itemView);
            imageWho = (ImageView) itemView.findViewById(R.id.image_who);

            textMesaj = (TextView) itemView.findViewById(R.id.text_mesaj);
            textTime = (TextView) itemView.findViewById(R.id.text_time);
        }
    }

    private String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

    private String getTime(Integer ms) {
        Calendar today=Calendar.getInstance();
        today=clearTimes(today);

        if(ms * 1000L >today.getTimeInMillis()) {
            Date date = new Date(ms * 1000L);
            SimpleDateFormat dateformat = new SimpleDateFormat("HH:mm");
            return dateformat.format(date);
        } else {
            Date date = new Date(ms * 1000L);
            SimpleDateFormat dateformat = new SimpleDateFormat("dd.MM.yyyy");
            return dateformat.format(date);
        }
    }

    private  Calendar clearTimes(Calendar c) {
        c.set(Calendar.HOUR_OF_DAY,0);
        c.set(Calendar.MINUTE,0);
        c.set(Calendar.SECOND,0);
        c.set(Calendar.MILLISECOND,0);

        return c;
    }

}
