package com.yuhhu.app.utils_image;

import java.io.File;

public class ModelFile {
    String key;
    File file;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }
}
