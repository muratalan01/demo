package com.yuhhu.app.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.yuhhu.app.model.ModelMessage;
import com.yuhhu.app.model.ModelMessagesLast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class DataBaseHelper extends android.database.sqlite.SQLiteOpenHelper {

    // Database Name
    public static String DATABASE_NAME = "yuhhu";
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Table Names
    private static final String TABLE_USER = "user";
    private static final String TABLE_MESSAGE = "message";

    // user Table - column names
    private static final String KEY_USER_ID = "id";
    private static final String KEY_USER_NAME = "name";
    private static final String KEY_USER_PICTURE = "picture";

    // message Table - column names
    private static final String KEY_MESSAGE_ID = "id";
    private static final String KEY_MESSAGE_TYPE = "type";
    private static final String KEY_MESSAGE_USER = "user";
    private static final String KEY_MESSAGE_TIME = "time";
    private static final String KEY_MESSAGE_MESSAGE = "message";
    private static final String KEY_MESSAGE_UNREAD = "unread";

    // user Table - column names
    private static final String CREATE_TABLE_USERS = "CREATE TABLE " + TABLE_USER
            + "("
            + KEY_USER_ID + " INTEGER PRIMARY KEY  NOT NULL,"
            + KEY_USER_NAME + " VARCHAR,"
            + KEY_USER_PICTURE + " VARCHAR"
            + ")";

    // message Table - column names
    private static final String CREATE_TABLE_MESSAGES = "CREATE TABLE " + TABLE_MESSAGE
            + "("
            + KEY_MESSAGE_ID + " INTEGER PRIMARY KEY  NOT NULL,"
            + KEY_MESSAGE_TYPE + " VARCHAR,"
            + KEY_MESSAGE_USER + " INTEGER,"
            + KEY_MESSAGE_TIME + " INTEGER,"
            + KEY_MESSAGE_MESSAGE + " TEXT,"
            + KEY_MESSAGE_UNREAD + " BOOL"
            + ")";

    public DataBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // creating required tables
        db.execSQL(CREATE_TABLE_USERS);
        db.execSQL(CREATE_TABLE_MESSAGES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // on upgrade drop older tables
        db.execSQL("DROP TABLE IF EXISTS " + CREATE_TABLE_USERS);
        db.execSQL("DROP TABLE IF EXISTS " + CREATE_TABLE_MESSAGES);
        // create new tables
        onCreate(db);
    }

    //---------------- "USERS" table methods ----------------//

    public void addUser(ModelMessage.User user) {
        if (checkUser(user.getId())) {
            updateUser(user);
            return; // user varsa update ediyoruz
        }

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_USER_ID, user.getId());
        values.put(KEY_USER_NAME, user.getName());
        values.put(KEY_USER_PICTURE, user.getPhoto());

        // insert row
        db.insert(TABLE_USER, null, values);
    }

    private void updateUser(ModelMessage.User  user) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_USER_ID, user.getId());
        values.put(KEY_USER_NAME, user.getName());
        values.put(KEY_USER_PICTURE, user.getPhoto());

        // updating row
        db.update(TABLE_USER, values, KEY_USER_ID + " = ?", new String[]{String.valueOf(user.getId())});
    }

    public List<ModelMessage.User > getAllUsers() {
        List<ModelMessage.User > users = new ArrayList<>();

        String query = "SELECT  * FROM " + TABLE_USER;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(query, null);

        if (c.moveToFirst()) {
            do {
                ModelMessage.User  user = new ModelMessage.User();

                user.setId(c.getInt(0));
                user.setName(c.getString(1));
                user.setPhoto(c.getString(2));

                users.add(user);
            } while (c.moveToNext());
        }

        return users;
    }

    public boolean checkUser(int userId) {
        String query = "SELECT " + KEY_USER_ID + " FROM " + TABLE_USER + " WHERE " + KEY_USER_ID + "='" + userId + "'";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(query, null);
        if (c.moveToFirst()) {
            return true;
        } else
            return false;
    }


    //---------------- "messages" table methods ----------------//

    public void addMessage(ModelMessage.MessageList message) {
        if (checkMesaj(message.getId())) {
            updateMessage(message);
            return; // mesaj varsa işlemi sonlandırıyoruz
        }

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_MESSAGE_ID, message.getId());
        values.put(KEY_MESSAGE_TYPE, message.getType());
        values.put(KEY_MESSAGE_USER, message.getUser());
        values.put(KEY_MESSAGE_TIME, message.getTime());
        values.put(KEY_MESSAGE_MESSAGE, message.getMessage());
        values.put(KEY_MESSAGE_UNREAD, true);

        // insert row
        long id = db.insert(TABLE_MESSAGE, null, values);
    }

    private void updateMessage(ModelMessage.MessageList message) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_MESSAGE_ID, message.getId());
        values.put(KEY_MESSAGE_TYPE, message.getType());
        values.put(KEY_MESSAGE_USER, message.getUser());
        values.put(KEY_MESSAGE_TIME, message.getTime());
        values.put(KEY_MESSAGE_MESSAGE, message.getMessage());
        values.put(KEY_MESSAGE_UNREAD, true);

        // updating row
        db.update(TABLE_MESSAGE, values, KEY_MESSAGE_ID + " = ?", new String[]{String.valueOf(message.getId())});
    }

    public List<ModelMessage.MessageList> getAllMessages(int userId) {
        List<ModelMessage.MessageList> messages = new ArrayList<>();

        String query = "SELECT  * FROM " + TABLE_MESSAGE + " WHERE " + KEY_MESSAGE_USER + "='" + userId + "' ORDER BY " + KEY_MESSAGE_ID + " ASC";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(query, null);

        if (c.moveToFirst()) {
            do {
                ModelMessage.MessageList message = new ModelMessage.MessageList();
                message.setId(c.getInt(0));
                message.setType(c.getString(1));
                message.setUser(c.getInt(2));
                message.setTime(c.getInt(3));
                message.setMessage(c.getString(4));
                message.setUnread(c.getInt(5) > 0);

                messages.add(message);
            } while (c.moveToNext());
        }

        setAllMessageRead(userId);

        return messages;
    }

    private void setAllMessageRead(int rowId){
        SQLiteDatabase db = this.getReadableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_MESSAGE_UNREAD, false);

        db.update(TABLE_MESSAGE, values, KEY_MESSAGE_USER + "= ?", new String[] {String.valueOf(rowId)});
    }

    public int getUnreadMessageCount(){
        SQLiteDatabase db = this.getReadableDatabase();

        String query = "SELECT  * FROM " + TABLE_MESSAGE + " WHERE " + KEY_MESSAGE_UNREAD + "='1'";
        Cursor cursor = db.rawQuery(query, null);
        int cnt = cursor.getCount();
        cursor.close();
        return cnt;
    }

    public boolean checkMesaj(int mesajId) {
        String query = "SELECT " + KEY_MESSAGE_ID + " FROM " + TABLE_MESSAGE + " WHERE " + KEY_MESSAGE_ID + "='" + mesajId + "'";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(query, null);
        if (c.moveToFirst()) {
            return true;
        } else
            return false;
    }

    public void deleteMessageOne(int messageID) {
        SQLiteDatabase db = this.getReadableDatabase();
        db.delete(TABLE_MESSAGE, KEY_MESSAGE_ID + "=" + messageID, null);
    }


    public List<ModelMessagesLast> getMessagesLast() {
        List<ModelMessagesLast> messagesLast = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        String queryMaxTime = "SELECT *, max(" + KEY_MESSAGE_ID + ") FROM " + TABLE_MESSAGE + " GROUP BY " + KEY_MESSAGE_USER + " ORDER BY max(" + KEY_MESSAGE_ID + ") DESC";
        Cursor cmaxUser = db.rawQuery(queryMaxTime, null);

        if (cmaxUser.moveToFirst()) {
            do {
                String queryUser = "SELECT * FROM " + TABLE_USER + " WHERE " + KEY_USER_ID + " = '" + cmaxUser.getInt(2) + "'";
                Cursor c = db.rawQuery(queryUser, null);
                c.moveToFirst();

                ModelMessagesLast messageLast = new ModelMessagesLast();
                messageLast.setId(c.getInt(0));
                messageLast.setName(c.getString(1));
                messageLast.setPicture(c.getString(2));

                String querySonMesaj = "SELECT * FROM " + TABLE_MESSAGE + " WHERE " + KEY_MESSAGE_ID + "=(SELECT MAX(" + KEY_MESSAGE_ID + ") FROM " + TABLE_MESSAGE + " WHERE " + KEY_MESSAGE_USER + "='" + c.getInt(0) + "')";
                Cursor cson = db.rawQuery(querySonMesaj, null);
                if (cson.moveToFirst()) {
                    messageLast.setType(cson.getInt(1) > 0);
                    messageLast.setTime(cson.getInt(3));
                    messageLast.setMessage(cson.getString(4));
                    messageLast.setUnread(cson.getInt(5) > 0);
                }

                messagesLast.add(messageLast);
            } while (cmaxUser.moveToNext());
        }

        return messagesLast;
    }

    public void deleteMessage(int userId) {
        SQLiteDatabase db = this.getReadableDatabase();
        db.delete(TABLE_USER, KEY_USER_ID + "=" + userId, null);
        db.delete(TABLE_MESSAGE, KEY_MESSAGE_USER + "=" + userId, null);
    }

    // closing database
    public void closeDB() {
        SQLiteDatabase db = this.getReadableDatabase();
        if (db != null && db.isOpen())
            db.close();
    }

    public static String getDate(long milliSeconds) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        int d = getDaysAgo(calendar.getTime());
        SimpleDateFormat formatter;
        if (d == 0) {
            formatter = new SimpleDateFormat("hh:mm");
            return formatter.format(calendar.getTime());
        } else {
            formatter = new SimpleDateFormat("dd MM yyyy");
            return formatter.format(calendar.getTime());
        }
    }

    private static int getDaysAgo(Date date) {
        long days = (new Date().getTime() - date.getTime()) / 86400000;

        if (days == 0) return 0;
        else return 1;
    }
}
