package com.yuhhu.app.model;

public class ModelGalery {

    int id;
    Photo photo;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Photo getPhoto() {
        return photo;
    }

    public void setPhoto(Photo photo) {
        this.photo = photo;
    }

    public static class Photo {
        String url;
        int en;
        int boy;

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public int getEn() {
            return en;
        }

        public void setEn(int en) {
            this.en = en;
        }

        public int getBoy() {
            return boy;
        }

        public void setBoy(int boy) {
            this.boy = boy;
        }
    }

}
