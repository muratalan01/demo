package com.yuhhu.app.utils;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import com.yuhhu.app.R;
import com.yuhhu.app.fontsUtils.FontsText;

import java.util.List;

public class MyDialogBottom extends Dialog {

    RecyclerView r_list;
    List<String> list;
    String title, message, cancel;
    OnActionButtonListener listener;

    public MyDialogBottom(Context context, int themeResId, List<String> list, String title, String message, String cancel, OnActionButtonListener listener) {
        super(context, themeResId);
        this.list = list;
        this.title = title;
        this.message = message;
        this.cancel = cancel;
        this.listener = listener;
    }

    @Override
    protected void onStart() {
        super.onStart();
        WindowManager.LayoutParams wlp = getWindow().getAttributes();
        wlp.gravity = Gravity.BOTTOM;
        getWindow().setAttributes(wlp);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.f_dialog_botton);
        Typeface tf = Typeface.createFromAsset(Settings.activity.getAssets(), "fonts/Ubuntu-Regular.ttf");
        ViewGroup myMostParentLayout = (ViewGroup) findViewById(R.id.fontroot);
        FontsText.setFontToAllChilds(myMostParentLayout, tf);

        TextView text_title = (TextView) findViewById(R.id.text_dialog_title);
        text_title.setText(title);
        TextView text_message = (TextView) findViewById(R.id.text_dialog);
        text_message.setText(message);

        if (title.equals("")) {
            text_title.setVisibility(View.GONE);
        }
        if (message.equals("")) {
            text_message.setVisibility(View.GONE);
        }

        TextView text_cancel = (TextView) findViewById(R.id.text_cancel);
        text_cancel.setText(cancel);
        text_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        r_list = (RecyclerView) findViewById(R.id.r_list);
        r_list.setHasFixedSize(true);
        r_list.setLayoutManager(new LinearLayoutManager(getContext()));

        r_list.setAdapter(new AdapterDialog(list));
    }

    class AdapterDialog extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        List<String> list;

        public AdapterDialog(List<String> list) {
            this.list = list;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(Settings.activity).inflate(R.layout.f_dialog_item, parent, false);
            Typeface tf = Typeface.createFromAsset(Settings.activity.getAssets(), "fonts/Ubuntu-Regular.ttf");
            ViewGroup myMostParentLayout = (ViewGroup) findViewById(R.id.fontroot);
            FontsText.setFontToAllChilds(myMostParentLayout, tf);

            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
            ViewHolder refHolder = (ViewHolder) holder;
            String row = list.get(position);

            refHolder.text_btn.setText(row);
            refHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onClickList(position);
                    dismiss();
                }
            });

            if (position == list.size() - 1)
                refHolder.line.setVisibility(View.INVISIBLE);
            else
                refHolder.line.setVisibility(View.VISIBLE);

        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        private class ViewHolder extends RecyclerView.ViewHolder {
            TextView text_btn;
            View line;

            public ViewHolder(View itemView) {
                super(itemView);
                text_btn = (TextView) itemView.findViewById(R.id.text_btn);
                line = (View) itemView.findViewById(R.id.v1);
            }
        }

    }

    public interface OnActionButtonListener {
        void onClickList(int position);
    }


    // Dialogun kullanımı
    /*
     List<String> list = new ArrayList<>(Arrays.asList("list item text","list item text"));
                MyDialogBottom dialog = new MyDialogBottom(Settings.activity, R.style.MyDialogBottom, list, "title", "message", "cancel", new MyDialogBottom.OnActionButtonListener() {
                    @Override
                    public void onClickList(int position) {
                        switch (position){
                            case 0:

                                break;
                            case 1:

                                break;
                            case 2:

                                break;
                        }
                    }
                });
                dialog.show();
     */

}
