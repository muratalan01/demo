package com.yuhhu.app.utils;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.yuhhu.app.R;
import com.yuhhu.app.fontsUtils.FontsText;

import java.util.Calendar;
import java.util.List;

public class MyDialogCenter extends Dialog {

    String oval, title, message;
    List<String> list;
    OnActionButtonListener listener;
    LinearLayout l_buttons_root;
    EditText edit_email;
    boolean isEdit = false;

    public MyDialogCenter(Context context, int themeResId, String oval, String title, String message, List<String> list, OnActionButtonListener listener) {
        super(context, themeResId);
        this.oval = oval;
        this.title = title;
        this.message = message;
        this.list = list;
        this.listener = listener;
    }

    public MyDialogCenter(Context context, int themeResId, String oval, String title, String message, boolean isEdit, List<String> list, OnActionButtonListener listener) {
        super(context, themeResId);
        this.oval = oval;
        this.title = title;
        this.message = message;
        this.list = list;
        this.listener = listener;
        this.isEdit = isEdit;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.f_dialog_center);
        Typeface tf = Typeface.createFromAsset(Settings.activity.getAssets(), "fonts/Ubuntu-Regular.ttf");
        ViewGroup myMostParentLayout = (ViewGroup)findViewById(R.id.fontroot);
        FontsText.setFontToAllChilds(myMostParentLayout, tf);

        setCancelable(false);

        TextView text_oval = (TextView) findViewById(R.id.text_oval);
        text_oval.setText(oval);

        TextView text_title = (TextView) findViewById(R.id.text_dialog_title);
        text_title.setText(title);
        TextView text_message = (TextView) findViewById(R.id.text_dialog);
        text_message.setText(message);

        if (title.equals(""))
            text_title.setVisibility(View.GONE);
        if (message.equals(""))
            text_message.setVisibility(View.GONE);

        edit_email = (EditText) findViewById(R.id.edit_email);
        if (isEdit)
            edit_email.setVisibility(View.VISIBLE);

        l_buttons_root = (LinearLayout) findViewById(R.id.l_buttons_root);

        for (int i = 0; i < list.size(); i++) {
            addButtonText(list.get(i), i);
        }
    }

    long stop_click = 0;

    private void addButtonText(String text, final int position) {
        LinearLayout row = new LinearLayout(getContext());
        row.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        row.setOrientation(LinearLayout.VERTICAL);

        row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                long tt = Calendar.getInstance().getTimeInMillis();
                if ((tt - stop_click) < 1000) return;
                stop_click = Calendar.getInstance().getTimeInMillis();

                if (isEdit)
                    listener.onClickListWithEdit(position, edit_email.getText().toString());
                else
                    listener.onClickList(position);

                dismiss();
            }
        });

        TextView view = new TextView(getContext());
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(0, getPixelValue(5), 0, 0);
        view.setLayoutParams(params);

        view.setText(text);
        view.setTextSize(15);
        view.setTextColor(Color.parseColor("#ffffff"));
        view.setAllCaps(false);
        view.setGravity(Gravity.CENTER);
        view.setPadding(0, getPixelValue(10), 0, getPixelValue(10));
        view.setBackgroundResource(R.drawable.back_dialog_btn);
        row.addView(view);

        l_buttons_root.addView(row);
    }

    private int getPixelValue(int dimenId) {
        float scale = getContext().getResources().getDisplayMetrics().density;
        int pixels = (int) (dimenId * scale);
        return pixels;
    }

    public interface OnActionButtonListener {
        void onClickList(int position);

        void onClickListWithEdit(int position, String text);
    }

    // Dialogun kullanımı
   /* List<String> list = new ArrayList<>(Arrays.asList("Şifremi sıfırla", "Vazgeç")); // butonlar
    MyDialogCenter dialog = new MyDialogCenter(Settings.activity, R.style.MyDialogCenter, "!", "title", "message", true, list, new MyDialogCenter.OnActionButtonListener() {
        @Override
        public void onClickList(int position) {

        }

        @Override
        public void onClickListWithEdit(int position, String text) {
            switch (position){
                case 0:
                    forgetPassword(text);
                    break;
                case 1:

                    break;
            }
        }
    });
    dialog.show();*/

}

