package com.yuhhu.app.utils;

import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;


public class KeyBoardClose {

    public static void close() {
        InputMethodManager inputManager = (InputMethodManager) Settings.activity.getSystemService(Context.INPUT_METHOD_SERVICE);

        if (inputManager.isActive()) {
            View view = Settings.activity.getCurrentFocus();
            //If no view currently has focus, create a new one, just so we can grab a window token from it
            if (view == null) {
                view = new View(Settings.activity);
            }
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

}
