package com.yuhhu.app.utils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.SwitchCompat;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.yuhhu.app.R;

import java.util.Calendar;

public class FormClass {

    Activity activity;
    LinearLayout root;

    int form_height = 47, form_left = 15, form_right = 15, form_header_top = 30, form_header_bottom = 2, form_text_size = 13, form_line_height = 1;
    int form_condition_top_bottom = 8;

    public FormClass(Activity activity, LinearLayout root) {
        this.activity = activity;
        this.root = root;
    }

    private int getPixelValue(int dimenId) {
        float scale = activity.getResources().getDisplayMetrics().density;
        int pixels = (int) (dimenId * scale);
        return pixels;
    }

    public void addLine(){
        LinearLayout row = new LinearLayout(activity);
        row.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        row.setOrientation(LinearLayout.VERTICAL);
        row.setBackgroundColor(Color.parseColor("#ffffff"));

        View lineBottom = new View(activity);
        lineBottom.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, getPixelValue(form_line_height)));
        lineBottom.setBackgroundColor(Color.parseColor("#c8c7cc"));
        row.addView(lineBottom);

        root.addView(row);
    }

    public void addHeader(String text, boolean line) {
        LinearLayout row = new LinearLayout(activity);
        row.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        row.setOrientation(LinearLayout.VERTICAL);
        row.setBackgroundColor(Color.parseColor("#eee9e9"));

        TextView view = new TextView(activity);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(getPixelValue(form_left), getPixelValue(form_header_top), getPixelValue(form_right), getPixelValue(form_header_bottom));
        view.setLayoutParams(params);
        if (text.equals(""))
            view.setTextSize(0);
        else
            view.setTextSize(form_text_size);
        view.setTextColor(Color.parseColor("#2274d8"));
        view.setText(text);
        row.addView(view);

        if (line) {
            View lineBottom = new View(activity);
            lineBottom.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, getPixelValue(form_line_height)));
            lineBottom.setBackgroundColor(Color.parseColor("#c8c7cc"));
            row.addView(lineBottom);
        }

        root.addView(row);
    }

    public void addText(String text) {
        LinearLayout row = new LinearLayout(activity);
        row.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        row.setOrientation(LinearLayout.VERTICAL);

        TextView view = new TextView(activity);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(getPixelValue(form_left), 0, getPixelValue(form_right), 0);
        view.setLayoutParams(params);
        view.setTextSize(12);
        view.setTextColor(Color.parseColor("#8f8f92"));
        view.setText(text);
        row.addView(view);

        root.addView(row);
    }

    public void addFormTextNoClick(String text_desc, String color_desc, String text, String color_text, boolean line_buttom) {
        LinearLayout row = new LinearLayout(activity);
        row.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        row.setBackgroundColor(Color.parseColor("#ffffff"));
        row.setOrientation(LinearLayout.VERTICAL);

        RelativeLayout r_row = new RelativeLayout(activity);
        r_row.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
        r_row.setBackgroundColor(Color.parseColor("#ffffff"));

        TextView textView_desc = new TextView(activity);

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, getPixelValue(form_height));
        params.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);

        textView_desc.setLayoutParams(params);
        textView_desc.setPadding(getPixelValue(form_left), 0, getPixelValue(form_right), 0);
        textView_desc.setText(text_desc);
        textView_desc.setTextSize(form_text_size);
        textView_desc.setTextColor(Color.parseColor(color_desc));
        textView_desc.setAllCaps(false);
        textView_desc.setBackgroundColor(Color.parseColor("#ffffff"));
        textView_desc.setGravity(Gravity.LEFT | Gravity.CENTER);
        textView_desc.setTypeface(Typeface.DEFAULT);
        r_row.addView(textView_desc);

        TextView view = new TextView(activity);

        RelativeLayout.LayoutParams params1 = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, getPixelValue(form_height));
        params1.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);

        view.setLayoutParams(params1);
        view.setPadding(getPixelValue(form_left), 0, getPixelValue(form_right), 0);
        view.setText(text);
        view.setTextSize(form_text_size);
        view.setTextColor(Color.parseColor(color_text));
        view.setAllCaps(false);
        view.setBackgroundColor(Color.parseColor("#ffffff"));
        view.setGravity(Gravity.RIGHT | Gravity.CENTER);
        view.setTypeface(Typeface.DEFAULT);
        r_row.addView(view);

        row.addView(r_row);

        if (line_buttom) {
            View lineBottom = new View(activity);
            RelativeLayout.LayoutParams paramsline = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, getPixelValue(form_line_height));
            paramsline.setMargins(getPixelValue(form_left), 0, 0, 0);
            lineBottom.setLayoutParams(paramsline);
            lineBottom.setBackgroundColor(Color.parseColor("#c8c7cc"));
            row.addView(lineBottom);
        }

        root.addView(row);
    }

    public void addFormEdit(String text_desc, String color_desc, String text, String color_text, String edit_hint, boolean line_buttom, final OnEditTextListener edit_listener) {
        LinearLayout row = new LinearLayout(activity);
        row.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        row.setBackgroundColor(Color.parseColor("#ffffff"));
        row.setOrientation(LinearLayout.VERTICAL);

        RelativeLayout r_row = new RelativeLayout(activity);
        r_row.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
        r_row.setBackgroundColor(Color.parseColor("#ffffff"));

        TextView textView_desc = new TextView(activity);

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, getPixelValue(form_height));
        params.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);

        textView_desc.setLayoutParams(params);
        textView_desc.setPadding(getPixelValue(form_left), 0, getPixelValue(form_right), 0);
        textView_desc.setText(text_desc);
        textView_desc.setTextSize(form_text_size);
        textView_desc.setTextColor(Color.parseColor(color_desc));
        textView_desc.setAllCaps(false);
        textView_desc.setBackgroundColor(Color.parseColor("#ffffff"));
        textView_desc.setGravity(Gravity.LEFT | Gravity.CENTER);
        textView_desc.setTypeface(Typeface.DEFAULT);
        r_row.addView(textView_desc);

        final EditText view = new EditText(activity);

        RelativeLayout.LayoutParams params1 = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, getPixelValue(form_height));
        params1.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);

        view.setLayoutParams(params1);
        view.setPadding(getPixelValue(form_left), 0, getPixelValue(form_right), 0);
        view.setText(text);
        view.setTextSize(form_text_size);
        view.setTextColor(Color.parseColor(color_text));
        view.setAllCaps(false);
        view.setBackgroundColor(Color.parseColor("#ffffff"));
        view.setGravity(Gravity.RIGHT | Gravity.CENTER);
        view.setTypeface(Typeface.DEFAULT);
        view.setHint(edit_hint);
        view.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
        r_row.addView(view);

        view.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                edit_listener.changedText(view.getText().toString());
            }
        });

        row.addView(r_row);

        row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                view.requestFocus();
                InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
            }
        });

        if (line_buttom) {
            View lineBottom = new View(activity);
            RelativeLayout.LayoutParams paramsline = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, getPixelValue(form_line_height));
            paramsline.setMargins(getPixelValue(form_left), 0, 0, 0);
            lineBottom.setLayoutParams(paramsline);
            lineBottom.setBackgroundColor(Color.parseColor("#c8c7cc"));
            row.addView(lineBottom);
        }

        root.addView(row);
    }


    public void addFormEditMulti(String title , String color_title, String text, String color_text, String edit_hint, boolean line_buttom, final OnEditTextListener edit_listener) {
        LinearLayout row = new LinearLayout(activity);
        row.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        row.setBackgroundColor(Color.parseColor("#ffffff"));
        row.setOrientation(LinearLayout.VERTICAL);

        TextView text_t = new TextView(activity);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(getPixelValue(form_left), getPixelValue(form_left), getPixelValue(form_right), 0);
        text_t.setLayoutParams(params);
        text_t.setTextSize(14);
        text_t.setTextColor(Color.parseColor(color_title));
        text_t.setText(title);
        row.addView(text_t);

        final EditText view = new EditText(activity);
        view.setPadding(getPixelValue(form_left), getPixelValue(form_left) , getPixelValue(form_right), getPixelValue(form_left));
        view.setText(text);
        view.setTextSize(form_text_size);
        view.setTextColor(Color.parseColor(color_text));
        view.setBackgroundColor(Color.parseColor("#ffffff"));
        view.setGravity(Gravity.LEFT | Gravity.TOP);
        view.setHint(edit_hint);
        view.setInputType(InputType.TYPE_CLASS_TEXT|InputType.TYPE_TEXT_FLAG_MULTI_LINE | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
        view.setMaxLines(7);
        view.setLines(7);
        view.setFilters(new InputFilter[] { new InputFilter.LengthFilter(300) });
        row.addView(view);

        view.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                edit_listener.changedText(view.getText().toString());
            }
        });

        row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                view.requestFocus();
                InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
            }
        });

        if (line_buttom) {
            View lineBottom = new View(activity);
            RelativeLayout.LayoutParams paramsline = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, getPixelValue(form_line_height));
            paramsline.setMargins(getPixelValue(form_left), 0, 0, 0);
            lineBottom.setLayoutParams(paramsline);
            lineBottom.setBackgroundColor(Color.parseColor("#c8c7cc"));
            row.addView(lineBottom);
        }

        root.addView(row);
    }

    public void addFormEdit(String text_desc, String color_desc, String text, String color_text, String edit_hint, int input_type, boolean line_buttom, final OnEditTextListener edit_listener) {
        LinearLayout row = new LinearLayout(activity);
        row.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        row.setBackgroundColor(Color.parseColor("#ffffff"));
        row.setOrientation(LinearLayout.VERTICAL);

        RelativeLayout r_row = new RelativeLayout(activity);
        r_row.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
        r_row.setBackgroundColor(Color.parseColor("#ffffff"));

        TextView textView_desc = new TextView(activity);

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, getPixelValue(form_height));
        params.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);

        textView_desc.setLayoutParams(params);
        textView_desc.setPadding(getPixelValue(form_left), 0, getPixelValue(form_right), 0);
        textView_desc.setText(text_desc);
        textView_desc.setTextSize(form_text_size);
        textView_desc.setTextColor(Color.parseColor(color_desc));
        textView_desc.setAllCaps(false);
        textView_desc.setBackgroundColor(Color.parseColor("#ffffff"));
        textView_desc.setGravity(Gravity.LEFT | Gravity.CENTER);
        textView_desc.setTypeface(Typeface.DEFAULT);
        r_row.addView(textView_desc);

        final EditText view = new EditText(activity);

        RelativeLayout.LayoutParams params1 = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, getPixelValue(form_height));
        params1.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);

        view.setLayoutParams(params1);
        view.setPadding(getPixelValue(form_left), 0, getPixelValue(form_right), 0);
        view.setText(text);
        view.setTextSize(form_text_size);
        view.setTextColor(Color.parseColor(color_text));
        view.setAllCaps(false);
        view.setBackgroundColor(Color.parseColor("#ffffff"));
        view.setGravity(Gravity.RIGHT | Gravity.CENTER);
        view.setTypeface(Typeface.DEFAULT);
        view.setHint(edit_hint);
        view.setInputType(input_type);
        r_row.addView(view);

        view.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                edit_listener.changedText(view.getText().toString());
            }
        });

        row.addView(r_row);

        row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                view.requestFocus();
                InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
            }
        });

        if (line_buttom) {
            View lineBottom = new View(activity);
            RelativeLayout.LayoutParams paramsline = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, getPixelValue(form_line_height));
            paramsline.setMargins(getPixelValue(form_left), 0, 0, 0);
            lineBottom.setLayoutParams(paramsline);
            lineBottom.setBackgroundColor(Color.parseColor("#c8c7cc"));
            row.addView(lineBottom);
        }

        root.addView(row);
    }

    public void addFormButton(final String text, String color_text, boolean line_buttom, final OnActionButtonListener listener) {
        LinearLayout row = new LinearLayout(activity);
        row.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        row.setBackgroundColor(Color.parseColor("#ffffff"));
        row.setOrientation(LinearLayout.VERTICAL);

        row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                long tt = Calendar.getInstance().getTimeInMillis();
                if ((tt - stop_click) < 1000) return;
                stop_click = Calendar.getInstance().getTimeInMillis();
                listener.onClick();
            }
        });

        TextView view = new TextView(activity);
        RelativeLayout.LayoutParams params1 = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, getPixelValue(form_height));
        view.setLayoutParams(params1);
        view.setPadding(getPixelValue(form_left), 0, getPixelValue(form_right), 0);
        view.setText(text);
        view.setTextSize(form_text_size);
        view.setTextColor(Color.parseColor(color_text));
        view.setAllCaps(false);
        view.setBackgroundColor(Color.parseColor("#ffffff"));
        view.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.arrow, 0);
        view.setGravity(Gravity.CENTER);
        view.setTypeface(Typeface.DEFAULT);
        row.addView(view);

        if (line_buttom) {
            View lineBottom = new View(activity);
            RelativeLayout.LayoutParams paramsline = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, getPixelValue(form_line_height));
            paramsline.setMargins(getPixelValue(form_left), 0, 0, 0);
            lineBottom.setLayoutParams(paramsline);
            lineBottom.setBackgroundColor(Color.parseColor("#c8c7cc"));
            row.addView(lineBottom);
        }

        root.addView(row);
    }

    public void addFormButton(String text_desc, String color_desc, final String text, String color_text, boolean line_buttom, final OnActionButtonListener listener) {
        LinearLayout row = new LinearLayout(activity);
        row.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        row.setBackgroundColor(Color.parseColor("#ffffff"));
        row.setOrientation(LinearLayout.VERTICAL);

        RelativeLayout r_row = new RelativeLayout(activity);
        r_row.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
        r_row.setBackgroundColor(Color.parseColor("#ffffff"));

        r_row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                long tt = Calendar.getInstance().getTimeInMillis();
                if ((tt - stop_click) < 1000) return;
                stop_click = Calendar.getInstance().getTimeInMillis();
                listener.onClick();
            }
        });

        TextView textView_desc = new TextView(activity);

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, getPixelValue(form_height));
        params.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);

        textView_desc.setLayoutParams(params);
        textView_desc.setPadding(getPixelValue(form_left), 0, getPixelValue(form_right), 0);
        textView_desc.setText(text_desc);
        textView_desc.setTextSize(form_text_size);
        textView_desc.setTextColor(Color.parseColor(color_desc));
        textView_desc.setAllCaps(false);
        textView_desc.setBackgroundColor(Color.parseColor("#ffffff"));
        textView_desc.setGravity(Gravity.LEFT | Gravity.CENTER);
        textView_desc.setTypeface(Typeface.DEFAULT);
        r_row.addView(textView_desc);

        TextView view = new TextView(activity);

        RelativeLayout.LayoutParams params1 = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, getPixelValue(form_height));
        params1.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);

        view.setLayoutParams(params1);
        view.setPadding(getPixelValue(form_left), 0, getPixelValue(form_right), 0);
        view.setText(text);
        view.setTextSize(form_text_size);
        view.setTextColor(Color.parseColor(color_text));
        view.setAllCaps(false);
        view.setBackgroundColor(Color.parseColor("#ffffff"));
        view.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.arrow, 0);
        view.setGravity(Gravity.RIGHT | Gravity.CENTER);
        view.setTypeface(Typeface.DEFAULT);
        r_row.addView(view);

        row.addView(r_row);

        if (line_buttom) {
            View lineBottom = new View(activity);
            RelativeLayout.LayoutParams paramsline = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, getPixelValue(form_line_height));
            paramsline.setMargins(getPixelValue(form_left), 0, 0, 0);
            lineBottom.setLayoutParams(paramsline);
            lineBottom.setBackgroundColor(Color.parseColor("#c8c7cc"));
            row.addView(lineBottom);
        }

        root.addView(row);
    }

    public void addFormButton(String text_desc, String color_desc, final String text, String color_text, boolean line_buttom, final String title, final String[] list, final OnButtonClickListener listener) {
        RelativeLayout row = new RelativeLayout(activity);
        row.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
        row.setBackgroundColor(Color.parseColor("#ffffff"));

        row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectDialogItem(title,list, text, listener);
            }
        });

        TextView textView_desc = new TextView(activity);

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, getPixelValue(form_height));
        params.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);

        textView_desc.setLayoutParams(params);
        textView_desc.setPadding(getPixelValue(form_left), 0, getPixelValue(form_right), 0);
        textView_desc.setText(text_desc);
        textView_desc.setTextSize(form_text_size);
        textView_desc.setTextColor(Color.parseColor(color_desc));
        textView_desc.setAllCaps(false);
        textView_desc.setBackgroundColor(Color.parseColor("#ffffff"));
        textView_desc.setGravity(Gravity.LEFT | Gravity.CENTER);
        textView_desc.setTypeface(Typeface.DEFAULT);
        row.addView(textView_desc);

        TextView view = new TextView(activity);

        RelativeLayout.LayoutParams params1 = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, getPixelValue(form_height));
        params1.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);

        view.setLayoutParams(params1);
        view.setPadding(getPixelValue(form_left), 0, getPixelValue(form_right), 0);
        view.setText(text);
        view.setTextSize(form_text_size);
        view.setTextColor(Color.parseColor(color_text));
        view.setAllCaps(false);
        view.setBackgroundColor(Color.parseColor("#ffffff"));
        view.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.arrow, 0);
        view.setGravity(Gravity.RIGHT | Gravity.CENTER);
        view.setTypeface(Typeface.DEFAULT);
        row.addView(view);

        if (line_buttom) {
            View lineBottom = new View(activity);
            RelativeLayout.LayoutParams paramsline = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, getPixelValue(form_line_height));
            paramsline.addRule(RelativeLayout.BELOW, textView_desc.getId());
            paramsline.setMargins(getPixelValue(form_left), 0, 0, 0);
            lineBottom.setLayoutParams(paramsline);
            lineBottom.setBackgroundColor(Color.parseColor("#c8c7cc"));
            row.addView(lineBottom);
        }

        root.addView(row);
    }

    private void selectDialogItem(String title, final String[] values, String search_title, final OnButtonClickListener listener) {
        this.listener = listener;
        AlertDialog.Builder myDialog = new AlertDialog.Builder(Settings.activity);
        myDialog.setTitle(title);

        myDialog.setSingleChoiceItems(values, getSelectPosition(values, search_title), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                listener.onClick(values[item]);
                dialog.dismiss();
            }});

        myDialog.setNegativeButton("Kapat", null);

        myDialog.show();
    }
    private int getSelectPosition(String[] list, String text_search){
        int s = 0;
        for (int i =0; i<list.length; i++){
            if (list[i].equals(text_search))
                s = i;
        }

        return s;
    }

    public void addFormActionButton(String text, String colorCode, final OnActionButtonListener listener) {
        LinearLayout row = new LinearLayout(activity);
        row.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        row.setOrientation(LinearLayout.VERTICAL);
        row.setBackgroundColor(Color.parseColor(colorCode));

        row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                long tt = Calendar.getInstance().getTimeInMillis();
                if ((tt - stop_click) < 1000) return;
                stop_click = Calendar.getInstance().getTimeInMillis();

                listener.onClick();
            }
        });

        TextView view = new TextView(activity);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, getPixelValue(form_height));
        view.setLayoutParams(params);
        view.setPadding(getPixelValue(form_left), 0, getPixelValue(form_right), 0);
        view.setText(text);
        view.setTextSize(form_text_size);
        view.setTextColor(Color.parseColor("#ffffff"));
        view.setAllCaps(false);
        view.setGravity(Gravity.CENTER);
        view.setTypeface(Typeface.DEFAULT);
        row.addView(view);

        root.addView(row);
    }

    String checked = "false";
    public void addButtonKosul(String text, boolean tick, boolean line_buttom, final OnSwichChangeListener listener) {
        LinearLayout row = new LinearLayout(activity);
        row.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        row.setBackgroundColor(Color.parseColor("#ffffff"));
        row.setOrientation(LinearLayout.VERTICAL);

        RelativeLayout r_row = new RelativeLayout(activity);
        r_row.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
        r_row.setBackgroundColor(Color.parseColor("#ffffff"));

        TextView textView_desc = new TextView(activity);

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, getPixelValue(form_height));
        params.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);

        textView_desc.setLayoutParams(params);
        textView_desc.setPadding(getPixelValue(form_left), 0, getPixelValue(form_right), 0);
        textView_desc.setText(text);
        textView_desc.setTextSize(form_text_size);
        textView_desc.setTextColor(Color.parseColor("#000000"));
        textView_desc.setAllCaps(false);
        textView_desc.setGravity(Gravity.LEFT | Gravity.CENTER);
        textView_desc.setTypeface(Typeface.DEFAULT);
        r_row.addView(textView_desc);

        SwitchCompat swich = new SwitchCompat(activity);
        RelativeLayout.LayoutParams params1 = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, getPixelValue(form_height));
        params1.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
        swich.setLayoutParams(params1);
        swich.setTextOn("");
        swich.setTextOff("");

        r_row.addView(swich);

        if (tick) {
            swich.setChecked(true);
        } else {
            swich.setChecked(false);
        }

        swich.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                listener.isChecked(isChecked);
            }
        });

        row.addView(r_row);

        if (line_buttom) {
            View lineBottom = new View(activity);
            RelativeLayout.LayoutParams paramsline = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, getPixelValue(form_line_height));
            paramsline.setMargins(getPixelValue(form_left), 0, 0, 0);
            lineBottom.setLayoutParams(paramsline);
            lineBottom.setBackgroundColor(Color.parseColor("#c8c7cc"));
            row.addView(lineBottom);
        }

        row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                long tt = Calendar.getInstance().getTimeInMillis();
                if ((tt - stop_click) < 1000) return;
                stop_click = Calendar.getInstance().getTimeInMillis();
                listener.onClick();
            }
        });

        root.addView(row);
    }

    public void addButtonKosulVip(String text, boolean tick, boolean isVip, boolean line_buttom, final OnSwichChangeListener listener) {
        LinearLayout row = new LinearLayout(activity);
        row.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        row.setBackgroundColor(Color.parseColor("#ffffff"));
        row.setOrientation(LinearLayout.VERTICAL);

        RelativeLayout r_row = new RelativeLayout(activity);
        r_row.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
        r_row.setBackgroundColor(Color.parseColor("#ffffff"));

        TextView textView_desc = new TextView(activity);

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, getPixelValue(form_height));
        params.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);

        textView_desc.setLayoutParams(params);
        textView_desc.setPadding(getPixelValue(form_left), 0, getPixelValue(form_right), 0);
        textView_desc.setText(text);
        textView_desc.setTextSize(form_text_size);
        textView_desc.setTextColor(Color.parseColor("#000000"));
        textView_desc.setAllCaps(false);
        textView_desc.setGravity(Gravity.LEFT | Gravity.CENTER);
        textView_desc.setTypeface(Typeface.DEFAULT);
        r_row.addView(textView_desc);

        SwitchCompat swich = new SwitchCompat(activity);
        RelativeLayout.LayoutParams params1 = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, getPixelValue(form_height));
        params1.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
        swich.setLayoutParams(params1);
        swich.setTextOn("");
        swich.setTextOff("");

        r_row.addView(swich);

        if (!isVip)

        if (tick) {
            swich.setChecked(true);
        } else {
            swich.setChecked(false);
        }

        swich.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                listener.isChecked(isChecked);
            }
        });

        row.addView(r_row);

        if (line_buttom) {
            View lineBottom = new View(activity);
            RelativeLayout.LayoutParams paramsline = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, getPixelValue(form_line_height));
            paramsline.setMargins(getPixelValue(form_left), 0, 0, 0);
            lineBottom.setLayoutParams(paramsline);
            lineBottom.setBackgroundColor(Color.parseColor("#c8c7cc"));
            row.addView(lineBottom);
        }

        row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                long tt = Calendar.getInstance().getTimeInMillis();
                if ((tt - stop_click) < 1000) return;
                stop_click = Calendar.getInstance().getTimeInMillis();
                listener.onClick();
            }
        });

        root.addView(row);
    }

    public interface OnEditTextListener {
        void changedText(String text);
    }

    OnButtonClickListener listener;
    public interface OnButtonClickListener {
        void onClick(String value);
    }

    long stop_click = 0;
    public interface OnActionButtonListener {
        void onClick();
    }

    public interface OnSwichChangeListener {
        void onClick();
        void isChecked(boolean isChecked);
    }


    //ornek form kullanımı seçimli
    /*
      String[] values1 = { "Erkek", "Kadın" };
        formClass.addFormButton("Cinsiyet", "#000000", user.getGender(), "#919196", false, "Cinsiyet", values1, new FormClass.OnButtonClickListener() {
            @Override
            public void onClick(String value) {
                user.setGender(value);
                reloadForms();
            }
        });

          formClass.addFormTextNoClick("E-posta Adresi", "#000000", user.getEmail(), "#919196",false);

        formClass.addHeader("", false);

        formClass.addFormEdit("Ad Soyad / Rumuz", "#000000", user.getName(), "#919196", "Zorunlu", true, new FormClass.OnEditTextListener() {
            @Override
            public void changedText(String text) {
                user.setName(text);
            }
        });

         formClass.addFormActionButton("Değişiklikleri Kaydet", "#5dafdf", new FormClass.OnActionButtonListener() {
            @Override
            public void onClick() {
                setUserValues();
            }
        });


          form.addFormButton("Destek", "#000000", "", "#000000", true, new FormClass.OnActionButtonListener() {
            @Override
            public void onClick() {
                Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                sendIntent.setType("plain/text");
                sendIntent.setData(Uri.parse("destek@terapistapp.com"));
                sendIntent.setClassName("com.google.android.gm", "com.google.android.gm.ComposeActivityGmail");
                sendIntent.putExtra(Intent.EXTRA_EMAIL, new String[] { "destek@terapistapp.com" });
                sendIntent.putExtra(Intent.EXTRA_SUBJECT, "Destek Talebi");
                sendIntent.putExtra(Intent.EXTRA_TEXT, "Destek Mesaj: \n\n Lütfen buradan sonrasını olduğu gibi bırakınız. \n Takip No: " + Settings.uuid + "\n");
                startActivity(sendIntent);
            }
        });


      public void reloadForms(){
        root.removeAllViews();
        addForms();
    }
     */

}
