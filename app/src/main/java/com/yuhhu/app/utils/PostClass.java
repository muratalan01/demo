package com.yuhhu.app.utils;

import android.os.AsyncTask;
import android.widget.Toast;

import com.yuhhu.app.R;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import javax.net.ssl.HttpsURLConnection;

public class PostClass {

    public interface OnDataLoaded {
        void onDataLoad(Boolean success, String response);
    }

    public static String encodeString(String value) {
        try {
            return URLEncoder.encode(value, "utf-8");
        } catch(Exception e) {
            return "";
        }
    }

    public static void PostData(final String url, final String postdata, final OnDataLoaded listener) {
        if (NetworkUtil.getConnectivityStatus()==0){
            Toast.makeText(Settings.activity, Settings.dilBilgisi("connection_error", Settings.activity.getResources().getString(R.string.connection_error)) , Toast.LENGTH_SHORT).show();
            return;
        }

        class LoadJson extends AsyncTask<Void, Void, String> {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected String doInBackground(Void... arg0) {

                String result = "";

                try {
                    String data = "&packet=" + PostClass.encodeString(Settings.packet)
                            + "&device=" + PostClass.encodeString(Settings.device)
                            + "&version=" + Settings.version
                            + "&language=" + PostClass.encodeString(Settings.language)
                            + "&country=" + PostClass.encodeString(Settings.country)
                            + "&uuid=" + PostClass.encodeString(Settings.uuid)
                            + "&level=" + PostClass.encodeString(Settings.level)
                            + "&timezone=" + PostClass.encodeString(Settings.timezone)
                            + "&resolution=" + PostClass.encodeString(Settings.resolution)
                            + "&did=" + PostClass.encodeString(Settings.did)
                            + "&uid=" + PostClass.encodeString(Settings.uid);

                    if (postdata!=null)
                        data = data + postdata;

                    Log.i("POST_REQUEST", Settings.api_url + url + data);
                    byte[] postDataBytes = data.toString().getBytes("UTF-8");

                    URL uri = new URL(Settings.api_url + url);

                    if(uri.getProtocol().equals("http")) {

                        // Http Bağlantısı
                        HttpURLConnection conn;

                        conn = (HttpURLConnection) uri.openConnection();
                        conn.setRequestMethod("POST");
                        conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
                        conn.setDoOutput(true);
                        conn.getOutputStream().write(postDataBytes);

                        if (conn.getResponseCode() != 200) {
                            listener.onDataLoad(false, null);
                        }

                        BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
                        String output;
                        while ((output = br.readLine()) != null) result += output;
                        conn.disconnect();

                    } else {

                        // Https Ssl Bağlantısı
                        HttpsURLConnection conn;

                        conn = (HttpsURLConnection) uri.openConnection();
                        conn.setRequestMethod("POST");
                        conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
                        conn.setDoOutput(true);
                        conn.getOutputStream().write(postDataBytes);

                        if (conn.getResponseCode() != 200) {
                            listener.onDataLoad(false, null);
                        }

                        BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
                        String output;
                        while ((output = br.readLine()) != null) result += output;
                        conn.disconnect();

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    listener.onDataLoad(false, null);
                    return null;
                }

                Log.i("POST_RESPONSE", result);
                listener.onDataLoad(true, result);
                return null;

            }
            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);
            }
        }
        (new LoadJson()).execute();
    }

}

