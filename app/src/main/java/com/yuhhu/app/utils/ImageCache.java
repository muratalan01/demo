package com.yuhhu.app.utils;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class ImageCache {

    public interface ImageLoad {
        void onImageLoaded(Bitmap bitmap);
        void onImageFailed();
    }

    static String resim_yolu = "cache_dir";

    private static String md5(final String s) {
        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();
            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2) h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static void load(Activity activity, String resimurl, final ImageLoad imageLoadLİstener) {

        class ImageDownloaderTask extends AsyncTask<String, Void, Bitmap> {

            private String resimid = null;
            private Activity act = null;

            public ImageDownloaderTask(Activity _activity, String _resimid) {
                this.act = _activity;
                this.resimid = _resimid;
            }

            @Override
            protected Bitmap doInBackground(String... params) {
                Bitmap resim = downloadBitmap(params[0]);
                if (resim != null){
                    resim_kaydet(this.act, this.resimid, resim);
                }
                return resim;
            }

            @Override
            protected void onPostExecute(Bitmap bitmap) {
                super.onPostExecute(bitmap);
                if (bitmap != null){
                    imageLoadLİstener.onImageLoaded(bitmap);
                }else {
                    imageLoadLİstener.onImageFailed();
                }
            }

            private Bitmap downloadBitmap(String url) {
                if (url==null) return null;
                HttpURLConnection urlConnection = null;
                try {
                    URL uri = new URL(url);
                    urlConnection = (HttpURLConnection) uri.openConnection();
                    int statusCode = urlConnection.getResponseCode();
                    if (statusCode != 200) {
                        return null;
                    }

                    InputStream inputStream = urlConnection.getInputStream();
                    if (inputStream != null) {
                        Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                        return bitmap;
                    }
                } catch (Exception e) {
                    if (urlConnection != null) {
                        urlConnection.disconnect();
                    }
                } finally {
                    if (urlConnection != null) {
                        urlConnection.disconnect();
                    }
                }
                return null;
            }
        }

        Bitmap g = resim_yukle(activity, resimurl);
        if (g != null) {
            imageLoadLİstener.onImageLoaded(g);
        } else {
            (new ImageDownloaderTask(activity, md5(resimurl))).execute(resimurl);
        }
    }

    private static String datayolu(Activity activity) {
        try {
            String datadir = activity.getPackageManager().getPackageInfo(activity.getPackageName(), 0).applicationInfo.dataDir;
            return datadir;
        } catch (Exception e) {
        }

        return "/";
    }

    private static boolean checkfile(Activity activity, String resimid) {
        try {
            File file = new File(datayolu(activity) + "/" + resim_yolu + "/" + resimid);
            if (file.exists()) return true;
            else return false;
        } catch (Exception e) {
            return false;
        }
    }

    private static Bitmap resim_yukle(Activity activity, String resimurl) {
        String resimid = md5(resimurl);

        if (resimurl.equals("null")) return null;

        if (checkfile(activity, resimid) == false) return null;

        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            Bitmap bitmap = BitmapFactory.decodeFile(datayolu(activity) + "/" + resim_yolu + "/" + resimid, options);
            return bitmap;

        } catch (Exception e) {
            return null;
        }

    }

    private static void resim_kaydet(Activity activity, String resimid, Bitmap resim) {
        if (resimid.equals("null")) return;

        try {
            File f = new File(datayolu(activity) + "/" + resim_yolu + "/");
            if (!f.exists()) f.mkdirs();
        } catch (Exception e) {
        }

        File file = new File(datayolu(activity) + "/" + resim_yolu + "/" + resimid);
        FileOutputStream out = null;

        try {
            out = new FileOutputStream(file);
            resim.compress(Bitmap.CompressFormat.PNG, 100, out);
        } catch (Exception e) {
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (Exception e) {
            }
        }
    }

    public static void hepsini_sil(Activity activity) {
        try {
            File dir = new File(datayolu(activity) + "/" + resim_yolu);
            if (dir.isDirectory()) {
                String[] children = dir.list();
                for (int i = 0; i < children.length; i++) {
                    new File(dir, children[i]).delete();
                }
            }
        } catch (Exception e) {
        }
    }

}

    //ImageCache class'ının kullanımı
        /*
        ImageCache.load(Settings.activity, url, new ImageCache.ImageLoad() {
        @Override
        public void onImageLoaded(final Bitmap bitmap) {
                image_view.setImageBitmap
                }

        @Override
        public void onImageFailed() {
        }
        });*/