package com.yuhhu.app.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.FragmentActivity;
import android.view.WindowManager;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.yuhhu.app.R;
import com.yuhhu.app.main.WebViewActivity;
import com.yuhhu.app.model.ModelCity;
import com.yuhhu.app.model.ModelUser;
import com.yuhhu.app.pays.InAppOdeme;
import com.yuhhu.app.push.RegisterApp;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;


public class Settings {

    public static final String inapp_base64 = "";
    public static InAppOdeme inappodeme = null;

    public static String api_url = "http://woowlove.com/woow/v1.0/api.php?i=";

    public static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    public static GoogleCloudMessaging gcm;
    public static String push_account_id = "";

    public static String device = "android";
    public static String packet = "";
    public static String version = "";
    public static String uuid = ""; // cihaz-unique-idsi
    public static String language = ""; // ülkesi (tr, en gibi)
    public static String country = "";
    public static String level = "";
    public static String timezone = "";
    public static String resolution = "";
    public static String did = ""; // service tarafından id oluşturulup atanacak
    public static String uid = "";

    public static Activity activity;
    public static FragmentActivity fragmentActivity;

    private static long son_islem_suresi = 0;

    public static String banner = "";
    public static String inters = "";
    public static int rate = 0;
    public static boolean premium = false;

    public static ModelUser user = new ModelUser();
    public static ArrayList<ModelCity> cities = new ArrayList<>();

    public static int filterMinYas = 18;
    public static int filterMaxYas = 80;
    public static String filterCinsiyet = "female";
    public static String lat = "";
    public static String lon = "";

    public static void cihazBilgileri(Activity activity) {
        Settings.packet = activity.getPackageName();
        Settings.language = Locale.getDefault().getLanguage();
        Settings.country = Locale.getDefault().getCountry();
        Settings.uuid = android.provider.Settings.Secure.getString(activity.getApplicationContext().getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);

        PackageInfo pInfo = null;
        try {
            pInfo = activity.getPackageManager().getPackageInfo(activity.getPackageName(), 0);
            Settings.version = pInfo.versionName;
            Log.i("versiyon", Settings.version);
        } catch (PackageManager.NameNotFoundException e) {
        }

        Settings.level = String.valueOf(Build.VERSION.SDK_INT);
        Settings.timezone = Calendar.getInstance().getTimeZone().getDisplayName(false, TimeZone.SHORT);
        Settings.resolution = ScreenSize.getScreenResolution(activity)[0] + "x" + ScreenSize.getScreenResolution(activity)[1];
    }

    public static void device_register() {
        long tt = Calendar.getInstance().getTimeInMillis();
        if ((tt - Settings.son_islem_suresi) < 10000) return;

        Settings.son_islem_suresi = Calendar.getInstance().getTimeInMillis();

        PostClass.PostData("device/register", null, new PostClass.OnDataLoaded() {
            @Override
            public void onDataLoad(Boolean success, String response) {
                if (success == true) {
                    try {
                        JSONObject obj = new JSONObject(response);
                        if (obj.has("data")) {
                            JSONObject data = obj.getJSONObject("data");

                            if (data.has("did")) {
                                SharedPreferenceClass.saveValue("did", data.getString("did"));
                                Settings.did = data.getString("did");
                            }
                            if (data.has("banner")){
                                SharedPreferenceClass.saveValue("banner", data.getString("banner"));
                                Settings.banner = data.getString("banner");
                            }
                            if (data.has("inters")){
                                SharedPreferenceClass.saveValue("inters", data.getString("inters"));
                                Settings.inters = data.getString("inters");
                            }
                            if (data.has("rate")){
                                SharedPreferenceClass.saveValueInt("rate", data.getInt("rate"));
                                Settings.rate = data.getInt("rate");
                            }
                            if (data.has("premium")){
                                SharedPreferenceClass.saveValueBoolean("premium", data.getBoolean("premium"));
                                Settings.premium = data.getBoolean("premium");
                            }

                            if (data.has("message")) {
                                if (data.has("appurl")) {
                                    JSONObject c = data.getJSONObject("message");
                                    alertGoster(c.getString("text"), c.getString("button"), data.getString("appurl"), null);
                                } else if (data.has("weburl")) {
                                    JSONObject c = data.getJSONObject("message");
                                    alertGoster(c.getString("text"), c.getString("button"), null, data.getString("weburl"));
                                } else {
                                    JSONObject c = data.getJSONObject("message");
                                    alertGoster(c.getString("text"), c.getString("button"), null, null);
                                }

                                return;
                            }

                            if (data.has("appurl")) {
                                String appurl = data.getString("appurl");

                                Intent intent = new Intent(activity, WebViewActivity.class);
                                intent.putExtra("url", appurl);
                                Settings.activity.startActivity(intent);
                            }

                            if (data.has("weburl")) {
                                final String weburl = data.getString("weburl");
                                Settings.activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(weburl));
                                        Settings.activity.startActivity(browserIntent);
                                    }
                                });
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                }
            }
        });
    }

    public static void device_islem(String islem) {
        if (islem.equals("onResume")) {
            Settings.activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Settings.device_register();
                    } catch (Exception e) {
                    }
                }
            });
        } else if (islem.equals("onPause")) {
            Settings.son_islem_suresi = Calendar.getInstance().getTimeInMillis();
        }
    }

    private static void alertGoster(final String msg, final String msgbuton, final String appurl, final String weburl) {
        Settings.activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(Settings.activity);
                builder.setMessage(msg)
                        .setPositiveButton(msgbuton, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                if (appurl != null) {
                                    Intent intent = new Intent(Settings.activity, WebViewActivity.class);
                                    intent.putExtra("url", appurl);
                                    Settings.activity.startActivity(intent);
                                } else if (weburl != null) {
                                    Settings.activity.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Log.i("weburlonly", weburl);
                                            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(weburl));
                                            Settings.activity.startActivity(browserIntent);
                                        }
                                    });
                                }
                                dialog.dismiss();
                            }
                        });
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });
    }

    // Push Notification Fonksiyonları
    public static void push_register() {
        if (checkPlayServices()) {
            new RegisterApp(activity.getApplicationContext(), gcm, push_account_id).execute();
        }
    }

    public static void push_token_alindi(String token) {
        if (token == null) return;
        String postdata = "&token=" + PostClass.encodeString(token);
        PostClass.PostData("device/token", postdata, new PostClass.OnDataLoaded() {
            @Override
            public void onDataLoad(Boolean success, String response) {
                if (success == true) {
                    JSONObject obj = null;
                    try {
                        obj = new JSONObject(response);
                        if (obj.has("data")) {
                            JSONObject data = obj.getJSONObject("data");
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private static boolean checkPlayServices() {
        int resultCode = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(Settings.activity);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GoogleApiAvailability.getInstance().isUserResolvableError(resultCode)) {
                GoogleApiAvailability.getInstance().getErrorDialog(activity, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            }
            return false;
        }

        return true;
    }

    public static String dilBilgisi(String metin, String gercek) {
        String donus = SharedPreferenceClass.getValue(metin, "");
        if (donus.equals("")) return gercek;
        return donus;
    }

    //Telefonun permission izinleri kullanıcıdan istenmiş fakat reddetmiş ise alert gösterilir ve gerekli izinleri vermesi sağlanır
    public static void alertIzin(final String msj) {
        Settings.activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(Settings.activity);
                builder.setMessage(msj)
                        .setPositiveButton(Settings.dilBilgisi("ok", Settings.activity.getResources().getString(R.string.ok)), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                try {
                                    Intent intent = new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                    intent.setData(Uri.parse("package:" + Settings.activity.getPackageName()));
                                    Settings.activity.startActivity(intent);
                                } catch (ActivityNotFoundException e) {
                                    //Open the generic Apps page:
                                    Intent intent = new Intent(android.provider.Settings.ACTION_MANAGE_APPLICATIONS_SETTINGS);
                                    Settings.activity.startActivity(intent);
                                }

                                dialog.dismiss();
                            }
                        });
                android.support.v7.app.AlertDialog dialog = builder.create();
                dialog.show();
            }
        });
    }

    public static void showError(final String text, final String btn){
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                List<String> list = new ArrayList<>(Arrays.asList(btn)); // butonlar
                MyDialogCenter dialog = new MyDialogCenter(Settings.activity, R.style.MyDialogCenter, "!", "", text, list, new MyDialogCenter.OnActionButtonListener() {
                    @Override
                    public void onClickList(int position) {

                    }

                    @Override
                    public void onClickListWithEdit(int position, String text) {

                    }
                });
                dialog.show();
            }
        });

    }

    public static void barChange(final String color) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                    activity.getWindow().setStatusBarColor(Color.parseColor(color));
                }
            }
        });
    }

    public static void barDefault() {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                    activity.getWindow().setStatusBarColor(activity.getResources().getColor(R.color.colorPrimaryDark));
                }
            }
        });
    }
}
