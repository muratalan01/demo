package com.yuhhu.app.utils;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

public class SharedPreferenceClass {

    public static void saveValue(String key, String text) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(Settings.activity);
        Editor editor = prefs.edit();
        editor.putString(key, text);
        editor.commit();
    }

    public static String getValue(String key, String defaultValue) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(Settings.activity);
        String value = prefs.getString(key, defaultValue);
        return value;
    }

    public static void saveValueInt(String key, int value) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(Settings.activity);
        Editor editor = prefs.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public static int getValueInt(String key, int value){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(Settings.activity);
        int value_ = prefs.getInt(key, value);
        return value_;
    }

    public static void saveValueBoolean(String key, boolean value){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(Settings.activity);
        Editor editor = prefs.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public static boolean getValueBoolean(String key, boolean value){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(Settings.activity);
        boolean value_ = prefs.getBoolean(key, value);
        return value_;
    }

}
