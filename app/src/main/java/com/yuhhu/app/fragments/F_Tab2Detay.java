package com.yuhhu.app.fragments;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.gson.Gson;
import com.yuhhu.app.R;
import com.yuhhu.app.adapters.A_TabPosts;
import com.yuhhu.app.adapters.A_Tab2Detay;
import com.yuhhu.app.fontsUtils.FontsText;
import com.yuhhu.app.main.MainActivity;
import com.yuhhu.app.model.ModelGalery;
import com.yuhhu.app.model.ModelPost;
import com.yuhhu.app.model.ModelProfile;
import com.yuhhu.app.utils.MyDialogBottom;
import com.yuhhu.app.utils.PostClass;
import com.yuhhu.app.utils.ScreenSize;
import com.yuhhu.app.utils.Settings;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class F_Tab2Detay extends Fragment {

    public static F_Tab2Detay f;
    public String user;

    View visible_view;

    A_TabPosts a_tabPosts;
    RecyclerView r_listPost;

    A_Tab2Detay a_tab1Detay;
    RecyclerView r_list;

    TextView text_bar;

    ProgressBar progressBar;
    ImageView image;
    FloatingActionButton imageLike;
    TextView textName, textCity, textLastSeen, textAboutTitle, textAbout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (visible_view != null) return visible_view;

        final View rootView = inflater.inflate(R.layout.f_tab2_detay, container, false);
        Typeface tf = Typeface.createFromAsset(Settings.activity.getAssets(), "fonts/Ubuntu-Regular.ttf");
        ViewGroup myMostParentLayout = (ViewGroup) rootView.findViewById(R.id.fontroot);
        FontsText.setFontToAllChilds(myMostParentLayout, tf);

        f = this;

        final LinearLayout btnLeft = (LinearLayout) rootView.findViewById(R.id.btn_left);
        btnLeft.setVisibility(View.VISIBLE);
        btnLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.main.onBackPressed();
            }
        });

        final LinearLayout btnRight = (LinearLayout) rootView.findViewById(R.id.btn_right);

        if (user.equals(String.valueOf(Settings.user.getId())))
            btnRight.setVisibility(View.GONE);
        else
            btnRight.setVisibility(View.VISIBLE);

        btnRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final List<String> list = new ArrayList<>();
                for (ModelProfile.Report row : profile.getReport()) {
                    list.add(row.getText());
                }

                MyDialogBottom dialog = new MyDialogBottom(Settings.activity, R.style.MyDialogBottom, list, "", Settings.dilBilgisi("why_report", getString(R.string.why_report)), Settings.dilBilgisi("cancel", getString(R.string.cancel)), new MyDialogBottom.OnActionButtonListener() {
                    @Override
                    public void onClickList(int position) {
                        report(position, list.get(position));
                    }
                });
                dialog.show();
            }
        });

        progressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
        image = (ImageView) rootView.findViewById(R.id.image);

        text_bar = (TextView)rootView.findViewById(R.id.text_bar);
        Typeface tf_bar = Typeface.createFromAsset(Settings.activity.getAssets(), "fonts/Ubuntu-Bold.ttf");
        text_bar.setTypeface(tf_bar);

        AppBarLayout appBarLayout = (AppBarLayout)rootView.findViewById(R.id.app_bar);

        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {

                if (Math.abs(verticalOffset)-appBarLayout.getTotalScrollRange() == 0) {
                    //  Collapsed
                    text_bar.setVisibility(View.VISIBLE);
                    textName.setVisibility(View.INVISIBLE);
                }else {
                    //Expanded
                    text_bar.setVisibility(View.INVISIBLE);
                    textName.setVisibility(View.VISIBLE);
                }
            }
        });

        imageLike = (FloatingActionButton) rootView.findViewById(R.id.image_like);

        int[] sizes = ScreenSize.getScreenResolution(Settings.activity);
        int width = sizes[0];
        int height = sizes[1] * 3 / 5;

        image.getLayoutParams().width = width;
        image.getLayoutParams().height = height;

        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                F_MediaDetay f = new F_MediaDetay();
                f.photo = profile.getPhoto().getUrl();

                Settings.fragmentActivity.getSupportFragmentManager().beginTransaction().replace(R.id.container_extra, f).addToBackStack("").commit();
            }
        });

        imageLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!profile.is_like()) {
                    profile.setIs_like(true);
                    like(Integer.parseInt(user));
                    imageLike.setImageResource(R.drawable.icon_heart);
                } else {
                    profile.setIs_like(false);
                    unLike(Integer.parseInt(user));
                    imageLike.setImageResource(R.drawable.icon_heart_empty);
                }

                F_Tab1.f.isRefreshFollow = true;
            }
        });

        textName = (TextView) rootView.findViewById(R.id.text_name);
        textCity = (TextView) rootView.findViewById(R.id.text_city);
        textLastSeen = (TextView) rootView.findViewById(R.id.text_last_seen);
        textAboutTitle = (TextView) rootView.findViewById(R.id.text_about_title);
        textAbout = (TextView) rootView.findViewById(R.id.text_about);

        ImageView imageMessage = (ImageView) rootView.findViewById(R.id.image_message);
        imageMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                F_MesajDetay f = new F_MesajDetay();
                f.id = profile.getId();
                f.name = profile.getName();
                f.photo = profile.getPhoto().getUrl();

                Settings.fragmentActivity.getSupportFragmentManager().beginTransaction().replace(R.id.container_extra, f).addToBackStack("").commit();
            }
        });

        if (user.equals(String.valueOf(Settings.user.getId())))
            imageMessage.setVisibility(View.GONE);

        r_listPost = (RecyclerView) rootView.findViewById(R.id.r_list_post);
        r_listPost.setHasFixedSize(true);
        r_listPost.setLayoutManager(new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL));

        r_list = (RecyclerView) rootView.findViewById(R.id.r_list);
        r_list.setHasFixedSize(true);
        r_list.setLayoutManager(new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL));

        LinearLayout buttons_layout = (LinearLayout) rootView.findViewById(R.id.buttons_layout);
        buttons_layout.setVisibility(View.VISIBLE);

        final Button btn_all = (Button) rootView.findViewById(R.id.btn_all);
        final Button btn_galery = (Button) rootView.findViewById(R.id.btn_follow);

        btn_all.setText(Settings.dilBilgisi("title_share_post", getString(R.string.title_share_post)));
        btn_galery.setText(Settings.dilBilgisi("title_galery", getString(R.string.title_galery)));


        View.OnClickListener secili = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int id = v.getId();
                switch (id) {
                    case R.id.btn_all:
                        r_listPost.setVisibility(View.VISIBLE);
                        r_list.setVisibility(View.GONE);

                        btn_all.setSelected(true);
                        btn_galery.setSelected(false);

                        break;
                    case R.id.btn_follow:
                        r_listPost.setVisibility(View.GONE);
                        r_list.setVisibility(View.VISIBLE);

                        btn_all.setSelected(false);
                        btn_galery.setSelected(true);

                        break;
                }
            }
        };

        btn_all.setOnClickListener(secili);
        btn_galery.setOnClickListener(secili);

        btn_all.setSelected(true);

        getUser();
        getDataPost();

        visible_view = rootView;
        return rootView;
    }

    ModelProfile profile;

    public void getUser() {
        MainActivity.main.showProgress();
        profile = new ModelProfile();

        String postdata = "&user=" + PostClass.encodeString(String.valueOf(user));

        PostClass.PostData("profile/detail", postdata, new PostClass.OnDataLoaded() {
            @Override
            public void onDataLoad(Boolean success, String response) {
                MainActivity.main.closeProgress();
                if (success == true) {
                    JSONObject obj = null;
                    try {
                        obj = new JSONObject(response);
                        if (obj.has("data")) {
                            JSONObject data = obj.getJSONObject("data");

                            Gson gson = new Gson();

                            profile = gson.fromJson(data.toString(), ModelProfile.class);

                            Settings.activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    setUserInformation();
                                    getData();
                                }
                            });

                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    List<ModelPost> posts;
    String next = "";

    private void getDataPost() {
        if (posts == null) {
            posts = new ArrayList<>();
            MainActivity.main.showProgress();
        }

        String postdata = "&user=" + PostClass.encodeString(user)
                + "&next=" + PostClass.encodeString(next);

        PostClass.PostData("profile/profile_posts", postdata, new PostClass.OnDataLoaded() {
            @Override
            public void onDataLoad(Boolean success, String response) {
                MainActivity.main.closeProgress();
                if (success == true) {
                    JSONObject obj = null;
                    try {
                        obj = new JSONObject(response);
                        if (obj.has("data")) {
                            JSONObject data = obj.getJSONObject("data");

                            if (data.has("next"))
                                next = data.getString("next");

                            if (data.has("list")) {
                                JSONArray list = data.getJSONArray("list");

                                Gson gson = new Gson();

                                for (int i = 0; i < list.length(); i++) {
                                    JSONObject c = list.getJSONObject(i);
                                    posts.add(gson.fromJson(c.toString(), ModelPost.class));
                                }

                                Settings.activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (a_tabPosts == null)
                                            setUpAdapterFollow();
                                        else {
                                            if (next.equals(""))
                                                a_tabPosts.setMoreDataAvailable(false);
                                            a_tabPosts.notifyDataChanged();
                                        }

                                    }
                                });

                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void setUpAdapterFollow() {
        a_tabPosts = new A_TabPosts(posts, 3);
        r_listPost.setAdapter(a_tabPosts);

        a_tabPosts.setLoadMoreListener(new A_TabPosts.OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                getDataPost();
            }
        });
    }

    private void refreshFollow() {
        if (a_tabPosts != null) {
            if (posts != null)
                posts.clear();

            a_tabPosts.setMoreDataAvailable(true);
            a_tabPosts.notifyDataChanged();
        }

        next = "";
        getDataPost();
    }

    public void refreshCommentCount(int postPosition, int count) {
        posts.get(postPosition).setComment_count(count);
        a_tabPosts.notifyDataSetChanged();
    }

    ArrayList<ModelGalery> galeries;

    private void getData() {
        galeries = new ArrayList<>();

        String postdata = "&id=" + PostClass.encodeString(user);

        PostClass.PostData("profile/get_galery", postdata, new PostClass.OnDataLoaded() {
            @Override
            public void onDataLoad(Boolean success, String response) {
                if (success == true) {
                    JSONObject obj = null;
                    try {
                        obj = new JSONObject(response);
                        if (obj.has("data")) {
                            JSONObject data = obj.getJSONObject("data");

                            if (data.has("list")) {
                                JSONArray list = data.getJSONArray("list");

                                Gson gson = new Gson();

                                for (int i = 0; i < list.length(); i++) {
                                    JSONObject c = list.getJSONObject(i);
                                    galeries.add(gson.fromJson(c.toString(), ModelGalery.class));
                                }

                                Settings.activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        a_tab1Detay = new A_Tab2Detay(galeries);
                                        r_list.setAdapter(a_tab1Detay);
                                    }
                                });

                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }


    private void setUserInformation() {
        progressBar.setVisibility(View.VISIBLE);
        Glide.with(Settings.activity)
                .load(profile.getPhoto().getUrl())
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        progressBar.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        progressBar.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(image);

        if (profile.is_like())
            imageLike.setImageResource(R.drawable.icon_heart);
        else
            imageLike.setImageResource(R.drawable.icon_heart_empty);

        if (user.equals(String.valueOf(Settings.user.getId())))
            imageLike.setVisibility(View.GONE);
        else
            imageLike.setVisibility(View.VISIBLE);

        textName.setText(profile.getName() + ", " + profile.getAge());
        text_bar.setText(profile.getName());

        textCity.setText(profile.getCity());

        textLastSeen.setText(Settings.dilBilgisi("last_seen", getString(R.string.last_seen)) + " - " + profile.getLast_seen());

        if (profile.getAbout().equals(""))
            textAboutTitle.setVisibility(View.GONE);
        else
            textAboutTitle.setText(profile.getName() + " " + Settings.dilBilgisi("about_title", getString(R.string.about_title)));

        if (profile.getAbout().equals(""))
            textAbout.setVisibility(View.GONE);
        else
            textAbout.setText(profile.getAbout());
    }

    private void report(int report_id, String report_text) {
        String postdata = "&user=" + PostClass.encodeString(user)
                + "&report_id=" + PostClass.encodeString(String.valueOf(report_id))
                + "&report_text=" + PostClass.encodeString(report_text);
        PostClass.PostData("profile/report", postdata, new PostClass.OnDataLoaded() {
            @Override
            public void onDataLoad(Boolean success, String response) {
                if (success == true) {
                    JSONObject obj = null;
                    try {
                        obj = new JSONObject(response);

                        if (obj.has("result")) {
                            if (obj.getString("result").equals("ok")) {
                                Settings.activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(Settings.activity, Settings.dilBilgisi("reported", getString(R.string.reported)), Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }

                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void like(int id){
        String postdata = "&user=" + PostClass.encodeString(String.valueOf(id));
        PostClass.PostData("profile/like", postdata, new PostClass.OnDataLoaded() {
            @Override
            public void onDataLoad(Boolean success, String response) {
                if (success == true) {
                    JSONObject obj = null;
                    try {
                        obj = new JSONObject(response);
                        if (obj.has("data")) {
                            JSONObject data = obj.getJSONObject("data");
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void unLike(int id){
        String postdata = "&user=" + PostClass.encodeString(String.valueOf(id));
        PostClass.PostData("profile/unLike", postdata, new PostClass.OnDataLoaded() {
            @Override
            public void onDataLoad(Boolean success, String response) {
                if (success == true) {
                    JSONObject obj = null;
                    try {
                        obj = new JSONObject(response);
                        if (obj.has("data")) {
                            JSONObject data = obj.getJSONObject("data");
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

}
