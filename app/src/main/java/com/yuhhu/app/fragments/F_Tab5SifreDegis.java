package com.yuhhu.app.fragments;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.yuhhu.app.R;
import com.yuhhu.app.fontsUtils.FontsText;
import com.yuhhu.app.main.MainActivity;
import com.yuhhu.app.utils.FormClass;
import com.yuhhu.app.utils.PostClass;
import com.yuhhu.app.utils.Settings;

import org.json.JSONObject;

public class F_Tab5SifreDegis extends Fragment {

    View visible_view;

    LinearLayout root;
    FormClass formClass;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (visible_view !=null) return visible_view;

        View rootView = inflater.inflate(R.layout.layout_forms, container, false);
        Typeface tf = Typeface.createFromAsset(Settings.activity.getAssets(), "fonts/Ubuntu-Regular.ttf");
        ViewGroup myMostParentLayout = (ViewGroup) rootView.findViewById(R.id.fontroot);
        FontsText.setFontToAllChilds(myMostParentLayout, tf);

        final LinearLayout btnLeft = (LinearLayout) rootView.findViewById(R.id.btn_left);
        btnLeft.setVisibility(View.VISIBLE);
        btnLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.main.onBackPressed();
            }
        });

        TextView text_bar = (TextView)rootView.findViewById(R.id.text_bar);
        Typeface tf_bar = Typeface.createFromAsset(Settings.activity.getAssets(), "fonts/Ubuntu-Bold.ttf");
        text_bar.setTypeface(tf_bar);
        text_bar.setVisibility(View.VISIBLE);
        text_bar.setText(Settings.dilBilgisi("change_pass_title", getString(R.string.change_pass_title)));

        root = (LinearLayout) rootView.findViewById(R.id.root);
        formClass = new FormClass(Settings.activity, root);

        addForms();

        visible_view = rootView;
        return rootView;
    }

    String pass, new_pass, new_pass2;
    private void addForms(){
        root.removeAllViews();

        formClass.addHeader("", false);
        formClass.addLine();

        formClass.addFormEdit(Settings.dilBilgisi("password", getString(R.string.password)), "#000000", "", "#000000", "", InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD, true, new FormClass.OnEditTextListener() {
            @Override
            public void changedText(String text) {
                pass = text;
            }
        });

        formClass.addFormEdit(Settings.dilBilgisi("new_password", getString(R.string.new_password)), "#000000", "", "#000000", "", InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD, true, new FormClass.OnEditTextListener() {
            @Override
            public void changedText(String text) {
                new_pass = text;
            }
        });

        formClass.addFormEdit(Settings.dilBilgisi("new_password_again", getString(R.string.new_password_again)), "#000000", "", "#000000", "", InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD, false, new FormClass.OnEditTextListener() {
            @Override
            public void changedText(String text) {
                new_pass2 = text;
            }
        });

        formClass.addLine();
        formClass.addHeader("", false);
        formClass.addLine();

        formClass.addFormActionButton(Settings.dilBilgisi("btn_degis", getString(R.string.btn_degis)), "#4695e0", new FormClass.OnActionButtonListener() {
            @Override
            public void onClick() {
                change();
            }
        });

        formClass.addHeader("", false);

    }

    private void change(){
        String postdata = "&pass=" + PostClass.encodeString(pass)
                + "&new_pass=" + PostClass.encodeString(new_pass)
                + "&new_pass2=" + PostClass.encodeString(new_pass2);

        PostClass.PostData("user/password", postdata, new PostClass.OnDataLoaded() {
            @Override
            public void onDataLoad(Boolean success, String response) {
                if (success == true) {
                    JSONObject obj = null;
                    try {
                        obj = new JSONObject(response);
                        if (obj.has("result")){
                            if (obj.getString("result").equals("ok")){
                                Settings.activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        MainActivity.main.onBackPressed();
                                    }
                                });
                            }

                            if (obj.getString("result").equals("error")){
                                if (obj.has("message")) {
                                    JSONObject message = obj.getJSONObject("message");
                                    final String text = message.getString("text");
                                    final String btn = message.getString("button");

                                    Settings.activity.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Settings.showError(text,btn);
                                        }
                                    });
                                }
                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

}

