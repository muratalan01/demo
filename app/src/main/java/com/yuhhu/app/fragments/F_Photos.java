package com.yuhhu.app.fragments;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.yuhhu.app.R;
import com.yuhhu.app.adapters.A_Photos;
import com.yuhhu.app.fontsUtils.FontsText;
import com.yuhhu.app.main.MainActivity;
import com.yuhhu.app.model.ModelGalery;
import com.yuhhu.app.utils.Settings;

import java.util.ArrayList;
import java.util.List;

public class F_Photos extends Fragment {

    public ArrayList<ModelGalery> galeries;
    public int position;
    public boolean canDelete = false;

    A_Photos a_photos;
    RecyclerView r_list;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Settings.barChange("#dd000000");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.f_photos, container, false);
        Typeface tf = Typeface.createFromAsset(Settings.activity.getAssets(), "fonts/Ubuntu-Regular.ttf");
        ViewGroup myMostParentLayout = (ViewGroup) rootView.findViewById(R.id.fontroot);
        FontsText.setFontToAllChilds(myMostParentLayout, tf);

       LinearLayout btn_right = (LinearLayout)rootView.findViewById(R.id.btn_right);
        btn_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.main.onBackPressed();
            }
        });

        r_list = (RecyclerView) rootView.findViewById(R.id.r_list);
        r_list.setHasFixedSize(true);
        SnapHelper snapHelper = new PagerSnapHelper();
        r_list.setLayoutManager(new LinearLayoutManager(Settings.activity, LinearLayoutManager.HORIZONTAL, false));
        snapHelper.attachToRecyclerView(r_list);

        a_photos = new A_Photos(galeries, canDelete);
        r_list.setAdapter(a_photos);
        r_list.scrollToPosition(position);

        return rootView;
    }


    @Override
    public void onPause() {
        super.onPause();
        Settings.barDefault();
    }

}
