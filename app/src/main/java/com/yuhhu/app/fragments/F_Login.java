package com.yuhhu.app.fragments;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.yuhhu.app.R;
import com.yuhhu.app.database.DataBaseHelper;
import com.yuhhu.app.fontsUtils.FontsText;
import com.yuhhu.app.main.MainActivity;
import com.yuhhu.app.utils.KeyBoardClose;
import com.yuhhu.app.utils.MyDialogCenter;
import com.yuhhu.app.utils.PostClass;
import com.yuhhu.app.utils.Settings;
import com.yuhhu.app.utils.SharedPreferenceClass;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class F_Login extends Fragment implements View.OnClickListener{

    EditText editEmail, editPass;
    TextView textOr;
    Button btnGiris, btnSifreYenile, btnKayit, btnFacebook;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.f_login, container, false);
        Typeface tf = Typeface.createFromAsset(Settings.activity.getAssets(), "fonts/Ubuntu-Regular.ttf");
        ViewGroup myMostParentLayout = (ViewGroup) rootView.findViewById(R.id.fontroot);
        FontsText.setFontToAllChilds(myMostParentLayout, tf);

        editEmail = (EditText)rootView.findViewById(R.id.edit_email);
        editPass = (EditText)rootView.findViewById(R.id.edit_pass);

        textOr = (TextView)rootView.findViewById(R.id.text_or);

        btnGiris = (Button)rootView.findViewById(R.id.btn_giris);
        btnGiris.setOnClickListener(this);
        btnSifreYenile = (Button)rootView.findViewById(R.id.btn_sifre_yenile);
        btnSifreYenile.setOnClickListener(this);
        btnKayit = (Button)rootView.findViewById(R.id.btn_kayit);
        btnKayit.setOnClickListener(this);
        btnFacebook = (Button)rootView.findViewById(R.id.btn_facebook);
        btnFacebook.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_giris:
                giris();
                break;
            case R.id.btn_sifre_yenile:
                List<String> list = new ArrayList<>(Arrays.asList("Şifremi sıfırla", "Vazgeç")); // butonlar
                MyDialogCenter dialog = new MyDialogCenter(Settings.activity, R.style.MyDialogCenter, "!", "", "", true, list, new MyDialogCenter.OnActionButtonListener() {
                    @Override
                    public void onClickList(int position) {

                    }

                    @Override
                    public void onClickListWithEdit(int position, String text) {
                        switch (position){
                            case 0:
                                forgetPassword(text);
                                break;
                            case 1:

                                break;
                        }
                    }
                });
                dialog.show();
                break;
            case R.id.btn_kayit:
                Settings.fragmentActivity.getSupportFragmentManager().beginTransaction().replace(R.id.container_extra, new F_SignUp()).commit();
                break;
            case R.id.btn_facebook:

                break;
        }
    }

    private void giris(){
        MainActivity.main.showProgress();
        KeyBoardClose.close();
        String postdata = "&email=" + PostClass.encodeString(editEmail.getText().toString())
                + "&pass=" + PostClass.encodeString(editPass.getText().toString());

        PostClass.PostData("user/login", postdata, new PostClass.OnDataLoaded() {
            @Override
            public void onDataLoad(Boolean success, String response) {
                MainActivity.main.closeProgress();
                if (success == true) {
                    JSONObject obj = null;
                    try {
                        obj = new JSONObject(response);
                        if (obj.has("data")) {
                            JSONObject data = obj.getJSONObject("data");
                            SharedPreferenceClass.saveValue("uid", data.getString("uid"));
                            Settings.uid = data.getString("uid");
                            DataBaseHelper.DATABASE_NAME = Settings.uid;
                            MainActivity.main.getMessages();

                            Settings.activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    MainActivity.main.init();
                                    getFragmentManager().beginTransaction().remove(getFragmentManager().findFragmentById(R.id.container_extra)).commit();
                                }
                            });
                        }

                        if (obj.has("result")){
                            if (obj.getString("result").equals("error")){
                                if (obj.has("message")) {
                                    JSONObject message = obj.getJSONObject("message");
                                    final String text = message.getString("text");
                                    final String btn = message.getString("button");

                                    Settings.activity.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Settings.showError(text,btn);
                                        }
                                    });
                                }
                            }
                        }

                    } catch (Exception e) {
                    }
                }
            }
        });
    }

    private void forgetPassword(String email){
        MainActivity.main.showProgress();
        String postdata = "&email=" + PostClass.encodeString(email);

        PostClass.PostData("user/forget", postdata, new PostClass.OnDataLoaded() {
            @Override
            public void onDataLoad(Boolean success, String response) {
                MainActivity.main.closeProgress();
                if (success == true) {
                    JSONObject obj = null;
                    try {
                        obj = new JSONObject(response);
                        if (obj.has("message")) {
                            JSONObject message = obj.getJSONObject("message");
                            final String text = message.getString("text");
                            final String btn = message.getString("button");

                            Settings.activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Settings.showError(text,btn);
                                }
                            });
                        }

                    } catch (Exception e) {
                    }
                }
            }
        });
    }

}

