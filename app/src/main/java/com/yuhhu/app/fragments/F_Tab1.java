package com.yuhhu.app.fragments;


import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.baoyz.widget.PullRefreshLayout;
import com.baoyz.widget.SmartisanDrawable;
import com.google.gson.Gson;
import com.yuhhu.app.R;
import com.yuhhu.app.adapters.A_TabPosts;
import com.yuhhu.app.fontsUtils.FontsText;
import com.yuhhu.app.main.MainActivity;
import com.yuhhu.app.model.ModelPost;
import com.yuhhu.app.utils.PostClass;
import com.yuhhu.app.utils.Settings;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class F_Tab1 extends Fragment {

    public static F_Tab1 f;
    View visible_view;

    public boolean isRefreshFollow = false;

    A_TabPosts a_tabPosts;
    A_TabPosts a_tabPsotsFollow;
    RecyclerView r_listAll, r_listFollow;
    private PullRefreshLayout pullRefAll, pullRefFollow;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (visible_view != null) return visible_view;

        View rootView = inflater.inflate(R.layout.f_tab1, container, false);
        Typeface tf = Typeface.createFromAsset(Settings.activity.getAssets(), "fonts/Ubuntu-Regular.ttf");
        ViewGroup myMostParentLayout = (ViewGroup) rootView.findViewById(R.id.fontroot);
        FontsText.setFontToAllChilds(myMostParentLayout, tf);

        f = this;

/*        TextView text_bar = (TextView)rootView.findViewById(R.id.text_bar);
        Typeface tf_bar = Typeface.createFromAsset(Settings.activity.getAssets(), "fonts/Ubuntu-Bold.ttf");
        text_bar.setTypeface(tf_bar);
        text_bar.setVisibility(View.VISIBLE);
        text_bar.setText(getResources().getString(R.string.title_1));*/

        ImageView imageRight = (ImageView) rootView.findViewById(R.id.image_right);
        imageRight.setImageResource(R.drawable.share_post);
        final LinearLayout btnRight = (LinearLayout) rootView.findViewById(R.id.btn_right);
        btnRight.setVisibility(View.VISIBLE);
        btnRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Settings.fragmentActivity.getSupportFragmentManager().beginTransaction().replace(R.id.container_extra, new F_SharePost()).addToBackStack("").commit();
            }
        });

        r_listAll = (RecyclerView) rootView.findViewById(R.id.r_listAll);
        r_listAll.setHasFixedSize(true);
        r_listAll.setLayoutManager(new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL));

        pullRefAll = (PullRefreshLayout) rootView.findViewById(R.id.sr_all);
        pullRefreshAll();

        r_listFollow = (RecyclerView) rootView.findViewById(R.id.r_listFollow);
        r_listFollow.setHasFixedSize(true);
        r_listFollow.setLayoutManager(new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL));

        pullRefFollow = (PullRefreshLayout) rootView.findViewById(R.id.sr_follow);
        pullRefreshFollow();

        LinearLayout buttons_layout = (LinearLayout) rootView.findViewById(R.id.buttons_layout);
        buttons_layout.setVisibility(View.VISIBLE);

        final Button btn_all = (Button) rootView.findViewById(R.id.btn_all);
        final Button btn_follow = (Button) rootView.findViewById(R.id.btn_follow);

        btn_all.setText(Settings.dilBilgisi("title_all", getString(R.string.title_all)));
        btn_follow.setText(Settings.dilBilgisi("title_follow", getString(R.string.title_follow)));

        View.OnClickListener secili = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int id = v.getId();
                switch (id) {
                    case R.id.btn_all:
                        pullRefAll.setVisibility(View.VISIBLE);
                        pullRefFollow.setVisibility(View.GONE);

                        btn_all.setSelected(true);
                        btn_follow.setSelected(false);

                        break;
                    case R.id.btn_follow:
                        pullRefAll.setVisibility(View.GONE);
                        pullRefFollow.setVisibility(View.VISIBLE);

                        btn_all.setSelected(false);
                        btn_follow.setSelected(true);

                        if (a_tabPsotsFollow == null){
                            getDataFollow();
                        } else {
                            if (isRefreshFollow) {
                                isRefreshFollow = false;
                                refreshFollow();
                            }
                        }

                        break;
                }
            }
        };

        btn_all.setOnClickListener(secili);
        btn_follow.setOnClickListener(secili);

        btn_all.setSelected(true);

        getData();

        visible_view = rootView;
        return rootView;
    }

    public void pullRefreshAll() {
        pullRefAll.setRefreshStyle(PullRefreshLayout.STYLE_RING);
        pullRefAll.setColorSchemeColors(Color.parseColor("#0087b4"));
        pullRefAll.setRefreshDrawable(new SmartisanDrawable(getActivity(), pullRefAll));

        pullRefAll.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // start refresh
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (a_tabPosts != null) {
                            if (posts != null)
                                posts.clear();

                            a_tabPosts.setMoreDataAvailable(true);
                            a_tabPosts.notifyDataChanged();
                        }

                        next = "";
                        getData();
                        // refresh complete
                        pullRefAll.setRefreshing(false);
                    }
                }, 16);
            }
        });
    }

    public void refreshList() {
        if (a_tabPosts != null) {
            if (posts != null)
                posts.clear();

            next = "";
            a_tabPosts.notifyDataSetChanged();
        }

        getData();
    }

    public void refreshCommentCount(int postPosition, int count) {
        posts.get(postPosition).setComment_count(count);
        a_tabPosts.notifyDataSetChanged();
    }

    List<ModelPost> posts;
    String next = "";

    private void getData() {
        if (posts == null) {
            posts = new ArrayList<>();
            MainActivity.main.showProgress();
        }

        String postdata = "&next=" + PostClass.encodeString(next);

        PostClass.PostData("profile/posts", postdata, new PostClass.OnDataLoaded() {
            @Override
            public void onDataLoad(Boolean success, String response) {
                MainActivity.main.closeProgress();
                if (success == true) {
                    JSONObject obj = null;
                    try {
                        obj = new JSONObject(response);
                        if (obj.has("data")) {
                            JSONObject data = obj.getJSONObject("data");

                            if (data.has("next"))
                                next = data.getString("next");

                            if (data.has("list")) {
                                JSONArray list = data.getJSONArray("list");

                                Gson gson = new Gson();

                                for (int i = 0; i < list.length(); i++) {
                                    JSONObject c = list.getJSONObject(i);
                                    posts.add(gson.fromJson(c.toString(), ModelPost.class));
                                }

                                Settings.activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (a_tabPosts == null)
                                            setUpAdapter();
                                        else {
                                            if (next.equals(""))
                                                a_tabPosts.setMoreDataAvailable(false);
                                            a_tabPosts.notifyDataChanged();
                                        }

                                    }
                                });

                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void setUpAdapter() {
        a_tabPosts = new A_TabPosts(posts, 1);
        r_listAll.setAdapter(a_tabPosts);

        a_tabPosts.setLoadMoreListener(new A_TabPosts.OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                getData();
            }
        });
    }

    List<ModelPost> postsFollow;
    String next_follow = "";

    private void getDataFollow() {
        if (postsFollow == null) {
            postsFollow = new ArrayList<>();
            MainActivity.main.showProgress();
        }

        String postdata = "&next=" + PostClass.encodeString(next_follow);

        PostClass.PostData("profile/follow_posts", postdata, new PostClass.OnDataLoaded() {
            @Override
            public void onDataLoad(Boolean success, String response) {
                MainActivity.main.closeProgress();
                if (success == true) {
                    JSONObject obj = null;
                    try {
                        obj = new JSONObject(response);
                        if (obj.has("data")) {
                            JSONObject data = obj.getJSONObject("data");

                            if (data.has("next"))
                                next_follow = data.getString("next");

                            if (data.has("list")) {
                                JSONArray list = data.getJSONArray("list");

                                Gson gson = new Gson();

                                for (int i = 0; i < list.length(); i++) {
                                    JSONObject c = list.getJSONObject(i);
                                    postsFollow.add(gson.fromJson(c.toString(), ModelPost.class));
                                }

                                Settings.activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (a_tabPsotsFollow == null)
                                            setUpAdapterFollow();
                                        else {
                                            if (next_follow.equals(""))
                                                a_tabPsotsFollow.setMoreDataAvailable(false);
                                            a_tabPsotsFollow.notifyDataChanged();
                                        }

                                    }
                                });

                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void setUpAdapterFollow() {
        a_tabPsotsFollow = new A_TabPosts(postsFollow, 2);
        r_listFollow.setAdapter(a_tabPsotsFollow);

        a_tabPsotsFollow.setLoadMoreListener(new A_TabPosts.OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                getDataFollow();
            }
        });
    }

    public void pullRefreshFollow() {
        pullRefFollow.setRefreshStyle(PullRefreshLayout.STYLE_RING);
        pullRefFollow.setColorSchemeColors(Color.parseColor("#0087b4"));
        pullRefFollow.setRefreshDrawable(new SmartisanDrawable(getActivity(), pullRefFollow));

        pullRefFollow.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // start refresh
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (a_tabPsotsFollow != null) {
                            if (postsFollow != null)
                                postsFollow.clear();

                            a_tabPsotsFollow.setMoreDataAvailable(true);
                            a_tabPsotsFollow.notifyDataChanged();
                        }

                        next_follow = "";
                        getDataFollow();
                        // refresh complete
                        pullRefFollow.setRefreshing(false);
                    }
                }, 16);
            }
        });
    }

    public void refreshFollow(){
        if (a_tabPsotsFollow != null) {
            if (postsFollow != null)
                postsFollow.clear();

            a_tabPsotsFollow.setMoreDataAvailable(true);
            a_tabPsotsFollow.notifyDataChanged();
        }

        next_follow = "";
        getDataFollow();
    }

    public void refreshCommentCountFollow(int postPosition, int count) {
        postsFollow.get(postPosition).setComment_count(count);
        a_tabPsotsFollow.notifyDataSetChanged();
    }

}
