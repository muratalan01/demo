package com.yuhhu.app.fragments;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.yuhhu.app.R;
import com.yuhhu.app.fontsUtils.FontsText;
import com.yuhhu.app.main.MainActivity;
import com.yuhhu.app.model.ModelUser;
import com.yuhhu.app.utils.FormClass;
import com.yuhhu.app.utils.PostClass;
import com.yuhhu.app.utils.Settings;
import com.yuhhu.app.utils.SharedPreferenceClass;

import org.json.JSONObject;


public class F_Tab5 extends Fragment {

        public static F_Tab5 f;
        View visible_view;

        LinearLayout root;
        FormClass formClass;

        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            if (visible_view != null) return visible_view;

            View rootView = inflater.inflate(R.layout.layout_forms, container, false);
            Typeface tf = Typeface.createFromAsset(Settings.activity.getAssets(), "fonts/Ubuntu-Regular.ttf");
            ViewGroup myMostParentLayout = (ViewGroup) rootView.findViewById(R.id.fontroot);
            FontsText.setFontToAllChilds(myMostParentLayout, tf);

            f = this;

            TextView text_bar = (TextView) rootView.findViewById(R.id.text_bar);
            Typeface tf_bar = Typeface.createFromAsset(Settings.activity.getAssets(), "fonts/Ubuntu-Bold.ttf");
            text_bar.setTypeface(tf_bar);
            text_bar.setVisibility(View.VISIBLE);
            text_bar.setText(Settings.dilBilgisi("title_5", getString(R.string.title_5)));

            root = (LinearLayout) rootView.findViewById(R.id.root);
            formClass = new FormClass(Settings.activity, root);

            addForms();

            return rootView;
        }

    private void addForms() {
        root.removeAllViews();

        formClass.addFormButton(Settings.dilBilgisi("information_hesap", getString(R.string.information_hesap)), "#000000", "", "#000000", true, new FormClass.OnActionButtonListener() {
            @Override
            public void onClick() {
                MainActivity.main.barController.addFragment(new F_Tab5HesapBilgileri());
            }
        });

        formClass.addFormButton(Settings.dilBilgisi("information_profil", getString(R.string.information_profil)), "#000000", "", "#000000", true, new FormClass.OnActionButtonListener() {
            @Override
            public void onClick() {
                MainActivity.main.barController.addFragment(new F_Tab5Profil());
            }
        });

        formClass.addFormButton(Settings.dilBilgisi("change_password", getString(R.string.change_password)), "#000000", "", "#000000", true, new FormClass.OnActionButtonListener() {
            @Override
            public void onClick() {
                MainActivity.main.barController.addFragment(new F_Tab5SifreDegis());
            }
        });

        formClass.addFormButton(Settings.dilBilgisi("logout", getString(R.string.logout)), "#000000", "", "#000000", false, new FormClass.OnActionButtonListener() {
            @Override
            public void onClick() {
                Settings.uid = "";
                Settings.user = new ModelUser();
                SharedPreferenceClass.saveValue("uid", "");
                logout();
            }
        });

        formClass.addLine();
        formClass.addHeader("", false);
        formClass.addLine();

        formClass.addFormButton(Settings.dilBilgisi("help", getString(R.string.help)), "#000000", "", "#000000", true, new FormClass.OnActionButtonListener() {
            @Override
            public void onClick() {
                F_WebView f = new F_WebView();
                f.page = "help";

                Settings.fragmentActivity.getSupportFragmentManager().beginTransaction().replace(R.id.container_extra, f).addToBackStack("").commit();
            }
        });

        formClass.addFormButton(Settings.dilBilgisi("privacy", getString(R.string.privacy)), "#000000", "", "#000000", true, new FormClass.OnActionButtonListener() {
            @Override
            public void onClick() {
                F_WebView f = new F_WebView();
                f.page = "privacy";

                Settings.fragmentActivity.getSupportFragmentManager().beginTransaction().replace(R.id.container_extra, f).addToBackStack("").commit();
            }
        });

        formClass.addFormButton(Settings.dilBilgisi("rules", getString(R.string.rules)), "#000000", "", "#000000", false, new FormClass.OnActionButtonListener() {
            @Override
            public void onClick() {
                F_WebView f = new F_WebView();
                f.page = "rules";

                Settings.fragmentActivity.getSupportFragmentManager().beginTransaction().replace(R.id.container_extra, f).addToBackStack("").commit();
            }
        });

        formClass.addLine();
        formClass.addHeader("", false);
    }

    private void logout() {
        MainActivity.main.showProgress();
        PostClass.PostData("user/logout", null, new PostClass.OnDataLoaded() {
            @Override
            public void onDataLoad(Boolean success, String response) {
                MainActivity.main.closeProgress();
                if (success == true) {
                    JSONObject obj = null;
                    try {
                        obj = new JSONObject(response);
                        if (obj.has("data")) {
                            JSONObject data = obj.getJSONObject("data");
                        }

                        Settings.activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Settings.fragmentActivity.getSupportFragmentManager().beginTransaction().replace(R.id.container_extra, new F_Login()).commit();
                            }
                        });

                    } catch (Exception e) {
                    }
                }
            }
        });
    }

}