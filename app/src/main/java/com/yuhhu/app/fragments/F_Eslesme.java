package com.yuhhu.app.fragments;


import android.graphics.Matrix;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.BounceInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.yuhhu.app.R;
import com.yuhhu.app.fontsUtils.FontsText;
import com.yuhhu.app.main.MainActivity;
import com.yuhhu.app.utils.Settings;

public class F_Eslesme extends Fragment implements View.OnTouchListener{

    View visible_view;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (visible_view !=null) return visible_view;

        View rootView = inflater.inflate(R.layout.f_eslesme, container, false);
        Typeface tf = Typeface.createFromAsset(Settings.activity.getAssets(), "fonts/Ubuntu-Regular.ttf");
        ViewGroup myMostParentLayout = (ViewGroup) rootView.findViewById(R.id.fontroot);
        FontsText.setFontToAllChilds(myMostParentLayout, tf);

        final LinearLayout btnLeft = (LinearLayout) rootView.findViewById(R.id.btn_left);
        btnLeft.setVisibility(View.VISIBLE);
        btnLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.main.onBackPressed();
            }
        });

        TextView text_bar = (TextView)rootView.findViewById(R.id.text_bar);
        Typeface tf_bar = Typeface.createFromAsset(Settings.activity.getAssets(), "fonts/Ubuntu-Bold.ttf");
        text_bar.setTypeface(tf_bar);
        text_bar.setVisibility(View.VISIBLE);
        text_bar.setText(getResources().getString(R.string.title_2));

        // frgament i sürükleme
        image = (ImageView) rootView.findViewById(R.id.image);
        image.setOnTouchListener(this);

        visible_view = rootView;
        return rootView;
    }


    private ImageView image;
    public FrameLayout frameLayout;
    private int _xDelta;
    RelativeLayout.LayoutParams layoutParams;
    int birim = 0;
    float angle = 0;
    float lastX;

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        final int X = (int) event.getRawX();

        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                lastX = X;

                break;
            case MotionEvent.ACTION_UP:

                break;
            case MotionEvent.ACTION_MOVE:
                if (Math.abs(X - lastX) > 1) {
                    if ( X - lastX < 0){
                        birim += Math.abs(X - lastX);
                        angle -= 0.5;
                    }else {
                        birim -= Math.abs(X - lastX);
                        angle += 0.5;
                    }

                    lastX = X;
                }

           /*   layoutParams = (RelativeLayout.LayoutParams) v.getLayoutParams();
                layoutParams.leftMargin = -birim;
                layoutParams.rightMargin = birim;
                v.setLayoutParams(layoutParams);*/

                Matrix matrix = new Matrix();
                image.setScaleType(ImageView.ScaleType.MATRIX);   //required
                matrix.postRotate(angle, image.getDrawable().getBounds().width(), image.getDrawable().getBounds().height());
                image.setImageMatrix(matrix);

                break;
        }

        image.invalidate();
        return true;
    }

    public Animation getAnimation(int xfrom, int xto, int time) {
        Animation animation = new TranslateAnimation(xfrom, xto, 0, 0);
        animation.setInterpolator(new BounceInterpolator());
        animation.setDuration(time);
        animation.setFillAfter(true);

        return animation;
    }


}

