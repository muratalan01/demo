package com.yuhhu.app.fragments;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.gson.Gson;
import com.yuhhu.app.R;
import com.yuhhu.app.adapters.A_TabPosts;
import com.yuhhu.app.adapters.A_Tab5ProfilGalery;
import com.yuhhu.app.fontsUtils.FontsText;
import com.yuhhu.app.main.MainActivity;
import com.yuhhu.app.model.ModelGalery;
import com.yuhhu.app.model.ModelPost;
import com.yuhhu.app.model.ModelUser;
import com.yuhhu.app.utils.MyDialogBottom;
import com.yuhhu.app.utils.PostClass;
import com.yuhhu.app.utils.ScreenSize;
import com.yuhhu.app.utils.Settings;
import com.yuhhu.app.utils_image.ModelField;
import com.yuhhu.app.utils_image.ModelFile;
import com.yuhhu.app.utils_image.MultipartUtility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class F_Tab5Profil extends Fragment {

    public static F_Tab5Profil f;
    View visible_view;

    A_TabPosts a_tabPosts;
    RecyclerView r_listPost;

    A_Tab5ProfilGalery a_tab4Profil;
    RecyclerView r_list;

    ProgressBar progressBar;
    ImageView image;
    TextView textEmty;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (visible_view != null) return visible_view;

        View rootView = inflater.inflate(R.layout.f_tab5_profil, container, false);
        Typeface tf = Typeface.createFromAsset(Settings.activity.getAssets(), "fonts/Ubuntu-Regular.ttf");
        ViewGroup myMostParentLayout = (ViewGroup) rootView.findViewById(R.id.fontroot);
        FontsText.setFontToAllChilds(myMostParentLayout, tf);

        f = this;

        final LinearLayout btnLeft = (LinearLayout) rootView.findViewById(R.id.btn_left);
        btnLeft.setVisibility(View.VISIBLE);
        btnLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.main.onBackPressed();
            }
        });

        final LinearLayout btnRight = (LinearLayout) rootView.findViewById(R.id.btn_right);
        btnRight.setVisibility(View.VISIBLE);
        btnRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final List<String> list = new ArrayList<>(Arrays.asList(Settings.dilBilgisi("take_foto", getString(R.string.take_foto)), Settings.dilBilgisi("take_album", getString(R.string.take_album))));
                MyDialogBottom dialog = new MyDialogBottom(Settings.activity, R.style.MyDialogBottom, list, "", Settings.dilBilgisi("chose", getString(R.string.chose)), Settings.dilBilgisi("cancel", getString(R.string.cancel)), new MyDialogBottom.OnActionButtonListener() {
                    @Override
                    public void onClickList(int position) {
                        switch (position) {
                            case 0:
                                MainActivity.main.take(new MainActivity.TakeListener() {
                                    @Override
                                    public void photoPath(String path) {
                                        sendImageProfile(path);
                                    }
                                });
                                break;
                            case 1:
                                MainActivity.main.takeFromGlarey(new MainActivity.TakeListener() {
                                    @Override
                                    public void photoPath(String path) {
                                        sendImageProfile(path);
                                    }
                                });
                                break;
                        }
                    }
                });
                dialog.show();
            }
        });

        image = (ImageView) rootView.findViewById(R.id.image);
        progressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);

        int[] sizes = ScreenSize.getScreenResolution(Settings.activity);
        int width = sizes[0];
        int height = sizes[1]*3/5;

        image.getLayoutParams().width = width;
        image.getLayoutParams().height = height;

        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Settings.user.getPhoto().getId() == 0) return;
                F_MediaDetay f = new F_MediaDetay();
                f.photo =Settings.user.getPhoto().getUrl();
                f.id = Settings.user.getPhoto().getId();

                Settings.fragmentActivity.getSupportFragmentManager().beginTransaction().replace(R.id.container_extra, f).addToBackStack("").commit();
            }
        });

        progressBar.setVisibility(View.VISIBLE);
        Glide.with(Settings.activity)
                .load(Settings.user.getPhoto().getUrl())
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        progressBar.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        progressBar.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(image);

        TextView textName = (TextView) rootView.findViewById(R.id.text_name);
        textName.setText(Settings.user.getName() + ", " + Settings.user.getAge());

        TextView textCity = (TextView) rootView.findViewById(R.id.text_city);
        textCity.setText(Settings.user.getCity());

        TextView textAboutTitle = (TextView) rootView.findViewById(R.id.text_about_title);
        if (Settings.user.getAbout().equals(""))
            textAboutTitle.setVisibility(View.GONE);
        else
            textAboutTitle.setText(Settings.user.getName() + " " + Settings.dilBilgisi("about_title", getString(R.string.about_title)));

        TextView textAbout = (TextView) rootView.findViewById(R.id.text_about);
        if (Settings.user.getAbout().equals(""))
            textAbout.setVisibility(View.GONE);
        else
            textAbout.setText(Settings.user.getAbout());

        final ImageView btnAddGalery = (ImageView) rootView.findViewById(R.id.btn_add);
        btnAddGalery.setVisibility(View.GONE);
        btnAddGalery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final List<String> list = new ArrayList<>(Arrays.asList(Settings.dilBilgisi("take_foto", getString(R.string.take_foto)), Settings.dilBilgisi("take_album", getString(R.string.take_album))));
                MyDialogBottom dialog = new MyDialogBottom(Settings.activity, R.style.MyDialogBottom, list, "", Settings.dilBilgisi("chose", getString(R.string.chose)), Settings.dilBilgisi("cancel", getString(R.string.cancel)), new MyDialogBottom.OnActionButtonListener() {
                    @Override
                    public void onClickList(int position) {
                        switch (position) {
                            case 0:
                                MainActivity.main.take(new MainActivity.TakeListener() {
                                    @Override
                                    public void photoPath(String path) {
                                        sendImageGlary(path);
                                    }
                                });
                                break;
                            case 1:
                                MainActivity.main.takeFromGlarey(new MainActivity.TakeListener() {
                                    @Override
                                    public void photoPath(String path) {
                                        sendImageGlary(path);
                                    }
                                });
                                break;
                        }
                    }
                });
                dialog.show();
            }
        });

        r_listPost = (RecyclerView) rootView.findViewById(R.id.r_list_post);
        r_listPost.setHasFixedSize(true);
        r_listPost.setLayoutManager(new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL));

        textEmty = (TextView) rootView.findViewById(R.id.text_emty);
        textEmty.setText(Settings.dilBilgisi("text_emty_galery", getString(R.string.text_emty_galery)));

        r_list = (RecyclerView) rootView.findViewById(R.id.r_list);
        r_list.setHasFixedSize(true);
        r_list.setLayoutManager(new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL));

        LinearLayout buttons_layout = (LinearLayout) rootView.findViewById(R.id.buttons_layout);
        buttons_layout.setVisibility(View.VISIBLE);

        final Button btn_all = (Button) rootView.findViewById(R.id.btn_all);
        final Button btn_galery = (Button) rootView.findViewById(R.id.btn_follow);

        btn_all.setText(Settings.dilBilgisi("title_share_post", getString(R.string.title_share_post)));
        btn_galery.setText(Settings.dilBilgisi("title_galery", getString(R.string.title_galery)));

        View.OnClickListener secili = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int id = v.getId();
                switch (id) {
                    case R.id.btn_all:
                        r_listPost.setVisibility(View.VISIBLE);
                        r_list.setVisibility(View.GONE);
                        btnAddGalery.setVisibility(View.GONE);

                        textEmty.setVisibility(View.GONE);

                        btn_all.setSelected(true);
                        btn_galery.setSelected(false);

                        break;
                    case R.id.btn_follow:
                        r_listPost.setVisibility(View.GONE);
                        r_list.setVisibility(View.VISIBLE);
                        btnAddGalery.setVisibility(View.VISIBLE);

                        if (galeries == null) {
                            getData();
                        }else {
                            if (galeries.size() < 1)
                                textEmty.setVisibility(View.VISIBLE);
                            else
                                textEmty.setVisibility(View.GONE);
                        }

                        btn_all.setSelected(false);
                        btn_galery.setSelected(true);

                        break;
                }
            }
        };

        btn_all.setOnClickListener(secili);
        btn_galery.setOnClickListener(secili);

        btn_all.setSelected(true);

        getDataPost();

        visible_view = rootView;
        return rootView;
    }

    public void setPhotoAvatar(){
        progressBar.setVisibility(View.VISIBLE);
        Glide.with(Settings.activity)
                .load(Settings.user.getPhoto().getUrl())
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        progressBar.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        progressBar.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(image);
    }

    List<ModelPost> posts;
    String next = "";

    private void getDataPost() {
        if (posts == null) {
            posts = new ArrayList<>();
            MainActivity.main.showProgress();
        }

        String postdata = "&next=" + PostClass.encodeString(next);

        PostClass.PostData("user/posts", postdata, new PostClass.OnDataLoaded() {
            @Override
            public void onDataLoad(Boolean success, String response) {
                MainActivity.main.closeProgress();
                if (success == true) {
                    JSONObject obj = null;
                    try {
                        obj = new JSONObject(response);
                        if (obj.has("data")) {
                            JSONObject data = obj.getJSONObject("data");

                            if (data.has("next"))
                                next = data.getString("next");

                            if (data.has("list")) {
                                JSONArray list = data.getJSONArray("list");

                                Gson gson = new Gson();

                                for (int i = 0; i < list.length(); i++) {
                                    JSONObject c = list.getJSONObject(i);
                                    posts.add(gson.fromJson(c.toString(), ModelPost.class));
                                }

                                Settings.activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (a_tabPosts == null)
                                            setUpAdapterFollow();
                                        else {
                                            if (next.equals(""))
                                                a_tabPosts.setMoreDataAvailable(false);
                                            a_tabPosts.notifyDataChanged();
                                        }

                                    }
                                });

                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void setUpAdapterFollow() {
        a_tabPosts = new A_TabPosts(posts, 4);
        r_listPost.setAdapter(a_tabPosts);

        a_tabPosts.setLoadMoreListener(new A_TabPosts.OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                getDataPost();
            }
        });
    }

    private void refreshFollow(){
        if (a_tabPosts != null) {
            if (posts != null)
                posts.clear();

            a_tabPosts.setMoreDataAvailable(true);
            a_tabPosts.notifyDataChanged();
        }

        next = "";
        getDataPost();
    }

    public void refreshCommentCount(int postPosition, int count) {
        posts.get(postPosition).setComment_count(count);
        a_tabPosts.notifyDataSetChanged();
    }

    ArrayList<ModelGalery> galeries;

    public void getData() {
        if (galeries == null)
            galeries = new ArrayList<>();
        else
            galeries.clear();

        PostClass.PostData("user/get_galery", null, new PostClass.OnDataLoaded() {
            @Override
            public void onDataLoad(Boolean success, String response) {
                if (success == true) {
                    JSONObject obj = null;
                    try {
                        obj = new JSONObject(response);
                        if (obj.has("data")) {
                            JSONObject data = obj.getJSONObject("data");

                            if (data.has("list")) {
                                JSONArray list = data.getJSONArray("list");

                                Gson gson = new Gson();

                                for (int i = 0; i < list.length(); i++) {
                                    JSONObject c = list.getJSONObject(i);
                                    galeries.add(gson.fromJson(c.toString(), ModelGalery.class));
                                }

                                Settings.activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (a_tab4Profil == null){
                                            a_tab4Profil = new A_Tab5ProfilGalery(galeries);
                                            r_list.setAdapter(a_tab4Profil);
                                        }else
                                            a_tab4Profil.notifyDataSetChanged();

                                    }
                                });

                            }
                        }

                        Settings.activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (galeries.size() < 1) {
                                    textEmty.setVisibility(View.VISIBLE);
                                    if (a_tab4Profil !=null)
                                        a_tab4Profil.notifyDataSetChanged();
                                }else
                                    textEmty.setVisibility(View.GONE);
                            }
                        });

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void sendImageProfile(String path_file) {
        MainActivity.main.showProgress();

        List<ModelField> fields = new ArrayList<>();
        ModelField field = new ModelField();
        field.setKey("uid");
        field.setField(Settings.uid);
        fields.add(field);

        List<ModelFile> files = new ArrayList<>();
        ModelFile file = new ModelFile();
        file.setKey("photo");
        file.setFile(new File(path_file));
        files.add(file);

        MultipartUtility.postFile(Settings.api_url + "user/add_photo", fields, files, new MultipartUtility.PostFileListener() {
            @Override
            public void response(String response) {
                System.out.println("response: " + response);

                MainActivity.main.closeProgress();
                JSONObject obj = null;
                try {
                    obj = new JSONObject(response);
                    if (obj.has("data")) {
                        JSONObject data = obj.getJSONObject("data");

                        Gson gson = new Gson();
                        ModelUser user = gson.fromJson(data.toString(), ModelUser.class);
                        Settings.user = user;

                        Settings.activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                progressBar.setVisibility(View.VISIBLE);
                                Glide.with(Settings.activity)
                                        .load(Settings.user.getPhoto().getUrl())
                                        .listener(new RequestListener<String, GlideDrawable>() {
                                            @Override
                                            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                                progressBar.setVisibility(View.GONE);
                                                return false;
                                            }

                                            @Override
                                            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                                progressBar.setVisibility(View.GONE);
                                                return false;
                                            }
                                        })
                                        .into(image);
                            }
                        });
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void upload(int total, int per) {

            }

            @Override
            public void uploadfinish() {
            }


        });
    }

    private void sendImageGlary(String path_file) {

        MainActivity.main.showProgress();

        List<ModelField> fields = new ArrayList<>();
        ModelField field = new ModelField();
        field.setKey("uid");
        field.setField(Settings.uid);
        fields.add(field);

        List<ModelFile> files = new ArrayList<>();
        ModelFile file = new ModelFile();
        file.setKey("photo");
        file.setFile(new File(path_file));
        files.add(file);

        MultipartUtility.postFile(Settings.api_url + "user/add_galery", fields, files, new MultipartUtility.PostFileListener() {
            @Override
            public void response(String response) {
                galeries.clear();
                MainActivity.main.closeProgress();
                JSONObject obj = null;
                try {
                    obj = new JSONObject(response);
                    if (obj.has("data")) {
                        JSONObject data = obj.getJSONObject("data");

                        if (data.has("list")) {
                            JSONArray list = data.getJSONArray("list");

                            Gson gson = new Gson();

                            for (int i = 0; i < list.length(); i++) {
                                JSONObject c = list.getJSONObject(i);
                                galeries.add(gson.fromJson(c.toString(), ModelGalery.class));
                            }

                            Settings.activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (a_tab4Profil == null) {
                                        a_tab4Profil = new A_Tab5ProfilGalery(galeries);
                                        r_list.setAdapter(a_tab4Profil);
                                    } else
                                        a_tab4Profil.notifyDataSetChanged();

                                }
                            });

                        }
                    }

                    Settings.activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (galeries.size() < 1)
                                textEmty.setVisibility(View.VISIBLE);
                            else
                                textEmty.setVisibility(View.GONE);
                        }
                    });

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void upload(int total, int per) {

            }

            @Override
            public void uploadfinish() {
                System.out.println("yüklendi*****.....................");
            }


        });
    }

}
