package com.yuhhu.app.fragments;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.baoyz.widget.PullRefreshLayout;
import com.baoyz.widget.SmartisanDrawable;
import com.google.gson.Gson;
import com.yuhhu.app.R;
import com.yuhhu.app.adapters.A_Tab4;
import com.yuhhu.app.fontsUtils.FontsText;
import com.yuhhu.app.main.MainActivity;
import com.yuhhu.app.model.ModelNotification;
import com.yuhhu.app.utils.PostClass;
import com.yuhhu.app.utils.Settings;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class F_Tab4 extends Fragment {

    public static F_Tab4 f;
    View visible_view;

    A_Tab4 a_tab3;
    RecyclerView r_list;
    private PullRefreshLayout pullRef;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (visible_view != null) return visible_view;

        View rootView = inflater.inflate(R.layout.f_r_list_with_pull, container, false);
        Typeface tf = Typeface.createFromAsset(Settings.activity.getAssets(), "fonts/Ubuntu-Regular.ttf");
        ViewGroup myMostParentLayout = (ViewGroup) rootView.findViewById(R.id.fontroot);
        FontsText.setFontToAllChilds(myMostParentLayout, tf);

        f = this;

        TextView text_bar = (TextView) rootView.findViewById(R.id.text_bar);
        Typeface tf_bar = Typeface.createFromAsset(Settings.activity.getAssets(), "fonts/Ubuntu-Bold.ttf");
        text_bar.setTypeface(tf_bar);
        text_bar.setVisibility(View.VISIBLE);
        text_bar.setText(Settings.dilBilgisi("title_4", getString(R.string.title_4)));

        r_list = (RecyclerView) rootView.findViewById(R.id.r_list);
        r_list.setHasFixedSize(true);
        r_list.setLayoutManager(new LinearLayoutManager(getActivity()));

        pullRef = (PullRefreshLayout) rootView.findViewById(R.id.swipeRefreshLayout);
        pullRefresh();

        getData();

        visible_view = rootView;
        return rootView;
    }

    public void pullRefresh() {
        pullRef.setRefreshStyle(PullRefreshLayout.STYLE_RING);
        pullRef.setColorSchemeColors(Color.parseColor("#0087b4"));
        pullRef.setRefreshDrawable(new SmartisanDrawable(getActivity(), pullRef));

        pullRef.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // start refresh
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (a_tab3 != null) {
                            if (notifications != null)
                                notifications.clear();

                            a_tab3.setMoreDataAvailable(true);
                            a_tab3.notifyDataChanged();
                        }

                        next = "";
                        getData();
                        // refresh complete
                        pullRef.setRefreshing(false);
                    }
                }, 16);
            }
        });
    }

    List<ModelNotification> notifications;
    String next = "";

    private void getData() {
        if (notifications == null) {
            notifications = new ArrayList<>();
            MainActivity.main.showProgress();
        }

        String postdata = "&next=" + PostClass.encodeString(next);

        PostClass.PostData("user/notifications", postdata, new PostClass.OnDataLoaded() {
            @Override
            public void onDataLoad(Boolean success, String response) {
                MainActivity.main.closeProgress();
                if (success == true) {
                    JSONObject obj = null;
                    try {
                        obj = new JSONObject(response);
                        if (obj.has("data")) {
                            JSONObject data = obj.getJSONObject("data");

                            if (data.has("next"))
                                next = data.getString("next");


                            if (data.has("list")) {
                                JSONArray list = data.getJSONArray("list");

                                Gson gson = new Gson();

                                for (int i = 0; i < list.length(); i++) {
                                    JSONObject c = list.getJSONObject(i);
                                    notifications.add(gson.fromJson(c.toString(), ModelNotification.class));
                                }

                                Settings.activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (a_tab3 == null)
                                            setUpAdapter();
                                        else {
                                            if (next.equals(""))
                                                a_tab3.setMoreDataAvailable(false);
                                            a_tab3.notifyDataChanged();
                                        }
                                    }
                                });

                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void setUpAdapter() {
        a_tab3 = new A_Tab4(notifications);
        r_list.setAdapter(a_tab3);

        a_tab3.setLoadMoreListener(new A_Tab4.OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                getData();
            }
        });
    }

}
