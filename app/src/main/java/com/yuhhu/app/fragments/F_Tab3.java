package com.yuhhu.app.fragments;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.baoyz.widget.PullRefreshLayout;
import com.baoyz.widget.SmartisanDrawable;
import com.yuhhu.app.R;
import com.yuhhu.app.adapters.A_Tab3;
import com.yuhhu.app.database.DataBaseHelper;
import com.yuhhu.app.fontsUtils.FontsText;
import com.yuhhu.app.model.ModelMessagesLast;
import com.yuhhu.app.utils.Settings;

import java.util.List;

public class F_Tab3 extends Fragment {

    public static F_Tab3 f;
    View visible_view;

    A_Tab3 a_tab2;
    RecyclerView r_list;
    private PullRefreshLayout pullRef;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (visible_view !=null) return visible_view;

        View rootView = inflater.inflate(R.layout.f_r_list_with_pull, container, false);
        Typeface tf = Typeface.createFromAsset(Settings.activity.getAssets(), "fonts/Ubuntu-Regular.ttf");
        ViewGroup myMostParentLayout = (ViewGroup) rootView.findViewById(R.id.fontroot);
        FontsText.setFontToAllChilds(myMostParentLayout, tf);

        f = this;

        TextView text_bar = (TextView)rootView.findViewById(R.id.text_bar);
        Typeface tf_bar = Typeface.createFromAsset(Settings.activity.getAssets(), "fonts/Ubuntu-Bold.ttf");
        text_bar.setTypeface(tf_bar);
        text_bar.setVisibility(View.VISIBLE);
        text_bar.setText(Settings.dilBilgisi("title_3", getString(R.string.title_3)));

        r_list = (RecyclerView) rootView.findViewById(R.id.r_list);
        r_list.setHasFixedSize(true);
        r_list.setLayoutManager(new LinearLayoutManager(getActivity()));

        pullRef = (PullRefreshLayout) rootView.findViewById(R.id.swipeRefreshLayout);
        pullRefresh();

        sonMesajlar();

        visible_view = rootView;
        return rootView;
    }

    public void pullRefresh() {
        pullRef.setRefreshStyle(PullRefreshLayout.STYLE_RING);
        pullRef.setColorSchemeColors(Color.parseColor("#0087b4"));
        pullRef.setRefreshDrawable(new SmartisanDrawable(getActivity(), pullRef));

        pullRef.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // start refresh
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        sonMesajlar();
                        // refresh complete
                        pullRef.setRefreshing(false);
                    }
                }, 16);
            }
        });
    }

    public void sonMesajlar() {
        Settings.activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                DataBaseHelper db = new DataBaseHelper(Settings.activity);
                List<ModelMessagesLast> sonMesajlar = db.getMessagesLast();

                int count = db.getUnreadMessageCount();
                //FragmentBase.f_base.bildirimSayilari(count);
                db.closeDB();

                a_tab2 = new A_Tab3(sonMesajlar);
                r_list.setAdapter(a_tab2);
            }
        });
    }
}
