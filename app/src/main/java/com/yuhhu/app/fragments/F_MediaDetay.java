package com.yuhhu.app.fragments;

import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;
import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;
import com.google.gson.Gson;
import com.yuhhu.app.R;
import com.yuhhu.app.fontsUtils.FontsText;
import com.yuhhu.app.main.MainActivity;
import com.yuhhu.app.model.ModelUser;
import com.yuhhu.app.utils.MyDialogCenter;
import com.yuhhu.app.utils.PostClass;
import com.yuhhu.app.utils.ScreenSize;
import com.yuhhu.app.utils.Settings;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class F_MediaDetay extends Fragment {

    public String photo, comment = "";
    public int id = 0;
    View visible_view;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Settings.barChange("#000000");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (visible_view != null) return visible_view;

        View rootView = inflater.inflate(R.layout.f_media_detay, container, false);
        Typeface tf = Typeface.createFromAsset(Settings.activity.getAssets(), "fonts/Ubuntu-Regular.ttf");
        ViewGroup myMostParentLayout = (ViewGroup)rootView.findViewById(R.id.fontroot);
        FontsText.setFontToAllChilds(myMostParentLayout, tf);

        LinearLayout btn_left = (LinearLayout)rootView.findViewById(R.id.btn_left);
        btn_left.setVisibility(View.VISIBLE);
        btn_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.main.onBackPressed();
            }
        });

        final ProgressBar progressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);

        final SubsamplingScaleImageView imageMedia = (SubsamplingScaleImageView) rootView.findViewById(R.id.image_media);

        progressBar.setVisibility(View.VISIBLE);
        Glide.with(Settings.activity)
                .load(photo)
                .asBitmap()
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap bitmap, GlideAnimation anim) {
                        int bitmapWidth = bitmap.getWidth();
                        int bitmapHeight = bitmap.getHeight();

                        int[] sizes = ScreenSize.getScreenResolution(Settings.activity);
                        int width = sizes[0];

                        int newWidth = width;
                        int newHeight = (int) Math.floor((double) bitmapHeight * ((double) newWidth / (double) bitmapWidth));

                        if (newHeight > sizes[1])
                            newHeight = sizes[1];

                        imageMedia.getLayoutParams().width = newWidth;
                        imageMedia.getLayoutParams().height = newHeight;

                        imageMedia.setMaxScale(8);
                        imageMedia.setImage(ImageSource.bitmap(bitmap));
                        progressBar.setVisibility(View.GONE);
                    }
                });

        TextView textAciklama = (TextView) rootView.findViewById(R.id.text_aciklama);
        textAciklama.setText(comment);

        ImageView image_delete = (ImageView) rootView.findViewById(R.id.image_delete);
        if (id>0){
            image_delete.setVisibility(View.VISIBLE);
            image_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    List<String> list = new ArrayList<>(Arrays.asList(Settings.dilBilgisi("btn_delete", Settings.activity.getString(R.string.btn_delete)), Settings.dilBilgisi("btn_give_up", Settings.activity.getString(R.string.btn_give_up)))); //butonlar
                    MyDialogCenter dialog = new MyDialogCenter(Settings.activity, R.style.MyDialogCenter, "!", "", Settings.dilBilgisi("delete", Settings.activity.getString(R.string.delete)), list, new MyDialogCenter.OnActionButtonListener() {
                        @Override
                        public void onClickList(int pos) {
                            switch (pos) {
                                case 0:
                                    sil(id);
                                    break;
                                case 1:

                                    break;
                            }
                        }

                        @Override
                        public void onClickListWithEdit(int position, String text) {

                        }
                    });
                    dialog.show();
                }
            });
        }

        visible_view = rootView;
        return rootView;
    }

    private void sil(int id) {
        MainActivity.main.showProgress();
        String postdata = "&photo_id=" + PostClass.encodeString(String.valueOf(id));
        PostClass.PostData("user/delete_photo", postdata, new PostClass.OnDataLoaded() {
            @Override
            public void onDataLoad(Boolean success, String response) {
                MainActivity.main.closeProgress();
                if (success == true) {
                    JSONObject obj = null;
                    try {
                        obj = new JSONObject(response);
                        if (obj.has("data")) {
                            JSONObject data = obj.getJSONObject("data");

                            Gson gson = new Gson();
                            ModelUser user = gson.fromJson(data.toString(), ModelUser.class);
                            Settings.user = user;

                            Settings.activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    F_Tab5Profil.f.setPhotoAvatar();
                                    MainActivity.main.onBackPressed();
                                }
                            });
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
        Settings.barDefault();
    }
}