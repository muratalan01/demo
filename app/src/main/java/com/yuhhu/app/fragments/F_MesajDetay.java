package com.yuhhu.app.fragments;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.yuhhu.app.R;
import com.yuhhu.app.adapters.A_MesajDetay;
import com.yuhhu.app.database.DataBaseHelper;
import com.yuhhu.app.fontsUtils.FontsText;
import com.yuhhu.app.main.MainActivity;
import com.yuhhu.app.model.ModelMessage;
import com.yuhhu.app.utils.KeyBoardClose;
import com.yuhhu.app.utils.PostClass;
import com.yuhhu.app.utils.Settings;

import java.util.ArrayList;
import java.util.List;

public class F_MesajDetay extends Fragment {

    A_MesajDetay a_mesajDetay;
    RecyclerView r_list;

    public int id;
    public String name;
    public String photo;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_messages_detay, container, false);
        Typeface tf = Typeface.createFromAsset(Settings.activity.getAssets(), "fonts/Ubuntu-Regular.ttf");
        ViewGroup myMostParentLayout = (ViewGroup) rootView.findViewById(R.id.fontroot);
        FontsText.setFontToAllChilds(myMostParentLayout, tf);

        LinearLayout btn_left = (LinearLayout) rootView.findViewById(R.id.btn_left);
        btn_left.setVisibility(View.VISIBLE);
        btn_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                KeyBoardClose.close();
                Settings.fragmentActivity.getSupportFragmentManager().popBackStack();
            }
        });

        TextView text_bar = (TextView) rootView.findViewById(R.id.text_bar);
        Typeface tf_bar = Typeface.createFromAsset(Settings.activity.getAssets(), "fonts/Ubuntu-Bold.ttf");
        text_bar.setTypeface(tf_bar);
        text_bar.setVisibility(View.VISIBLE);
        text_bar.setText(name);

        r_list = (RecyclerView) rootView.findViewById(R.id.r_list);
        r_list.setHasFixedSize(true);
        r_list.setLayoutManager(new LinearLayoutManager(getActivity()));

        getMessagesDb();

        final EditText edit_message = (EditText) rootView.findViewById(R.id.edit_detay_mesaj);
        TextView text_btn_gonder = (TextView) rootView.findViewById(R.id.text_btn_gonder);
        text_btn_gonder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String postdata = "&user=" + PostClass.encodeString(String.valueOf(id))
                        + "&message=" + PostClass.encodeString(edit_message.getText().toString());
                MainActivity.main.sendMessage(postdata);
                edit_message.setText("");
            }
        });

        return rootView;
    }

    List<ModelMessage.MessageList> messages;

    public void getMessagesDb() {
        Settings.activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (messages == null)
                    messages = new ArrayList<>();
                else
                    messages.clear();

                DataBaseHelper db = new DataBaseHelper(Settings.activity);
                messages.addAll(db.getAllMessages(id));

                db.closeDB();

                if (a_mesajDetay == null) {
                    a_mesajDetay = new A_MesajDetay(messages, photo, name);
                    r_list.setAdapter(a_mesajDetay);
                } else {
                    a_mesajDetay.notifyDataSetChanged();
                }

                r_list.scrollToPosition(messages.size() - 1);

                if (F_Tab3.f != null)
                    F_Tab3.f.sonMesajlar();
            }
        });
    }

    public Integer getUser() {
        return id;
    }

}