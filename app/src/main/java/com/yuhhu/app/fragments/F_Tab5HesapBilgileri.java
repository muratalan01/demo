package com.yuhhu.app.fragments;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.yuhhu.app.R;
import com.yuhhu.app.fontsUtils.FontsText;
import com.yuhhu.app.main.MainActivity;
import com.yuhhu.app.model.ModelCity;
import com.yuhhu.app.model.ModelUser;
import com.yuhhu.app.utils.FormClass;
import com.yuhhu.app.utils.MyDialogBottom;
import com.yuhhu.app.utils.PostClass;
import com.yuhhu.app.utils.Settings;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class F_Tab5HesapBilgileri extends Fragment {

    View visible_view;

    LinearLayout root;
    FormClass formClass;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (visible_view !=null) return visible_view;

        View rootView = inflater.inflate(R.layout.layout_forms, container, false);
        Typeface tf = Typeface.createFromAsset(Settings.activity.getAssets(), "fonts/Ubuntu-Regular.ttf");
        ViewGroup myMostParentLayout = (ViewGroup) rootView.findViewById(R.id.fontroot);
        FontsText.setFontToAllChilds(myMostParentLayout, tf);

        final LinearLayout btnLeft = (LinearLayout) rootView.findViewById(R.id.btn_left);
        btnLeft.setVisibility(View.VISIBLE);
        btnLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.main.onBackPressed();
            }
        });

        TextView text_bar = (TextView)rootView.findViewById(R.id.text_bar);
        Typeface tf_bar = Typeface.createFromAsset(Settings.activity.getAssets(), "fonts/Ubuntu-Bold.ttf");
        text_bar.setTypeface(tf_bar);
        text_bar.setVisibility(View.VISIBLE);
        text_bar.setText(Settings.dilBilgisi("information_hesap", getString(R.string.information_hesap)));

        root = (LinearLayout) rootView.findViewById(R.id.root);
        formClass = new FormClass(Settings.activity, root);

        addForms();

        visible_view = rootView;
        return rootView;
    }

    String email = Settings.user.getMail(), name = Settings.user.getName(), gender =  Settings.user.getGender(), birth = Settings.user.getBirthday(),
            city = Settings.user.getCity(), about = Settings.user.getAbout();

    boolean isGhost = Settings.user.isGhost(), isNotificationMessage = Settings.user.getNotification().isMessage(), isNotificationGame = Settings.user.getNotification().isGame(),
            isNotificationVizitor = Settings.user.getNotification().isVisitor(), isNotificationLike = Settings.user.getNotification().isLike();

    private void addForms() {
        root.removeAllViews();

        gender = gender.equals("") ? "seçiminiz" : gender.equals("male") ? "Erkek" : "Kadın" ;
        birth = birth.equals("") ? "seçiminiz" : birth;

        formClass.addFormTextNoClick(Settings.dilBilgisi("email", getResources().getString(R.string.email)), "#000000", email, "#919196", true);

        formClass.addFormEdit(Settings.dilBilgisi("name", getResources().getString(R.string.name)), "#000000", name, "#919196", "", true, new FormClass.OnEditTextListener() {
            @Override
            public void changedText(String text) {
                name = text;
            }
        });

        formClass.addFormButton(Settings.dilBilgisi("gender", getResources().getString(R.string.gender)), "#000000", gender, "#000000", true, new FormClass.OnActionButtonListener() {
            @Override
            public void onClick() {
                final List<String> list = new ArrayList<>(Arrays.asList(Settings.dilBilgisi("male",getString(R.string.male)),Settings.dilBilgisi("female",getString(R.string.female))));
                MyDialogBottom dialog = new MyDialogBottom(Settings.activity, R.style.MyDialogBottom, list, "", Settings.dilBilgisi("chose",getString(R.string.chose)), Settings.dilBilgisi("cancel",getString(R.string.cancel)), new MyDialogBottom.OnActionButtonListener() {
                    @Override
                    public void onClickList(int position) {
                        gender = list.get(position);
                        addForms();
                    }
                });
                dialog.show();
            }
        });

        formClass.addFormButton(Settings.dilBilgisi("birthday", getResources().getString(R.string.birthday)), "#000000", birth, "#000000", true, new FormClass.OnActionButtonListener() {
            @Override
            public void onClick() {
                ArrayList<String> yillar = new ArrayList<>();
                for (int i = 1940; i < 2006; i++){
                    yillar.add(String.valueOf(i));
                }
                final List<String> list = new ArrayList<>(yillar);
                MyDialogBottom dialog = new MyDialogBottom(Settings.activity, R.style.MyDialogBottom, list, "", Settings.dilBilgisi("chose",getString(R.string.chose)), Settings.dilBilgisi("cancel",getString(R.string.cancel)), new MyDialogBottom.OnActionButtonListener() {
                    @Override
                    public void onClickList(int position) {
                        birth = list.get(position);
                        addForms();
                    }
                });
                dialog.show();
            }
        });

        formClass.addFormButton(Settings.dilBilgisi("city", getString(R.string.city)), "#000000", city, "#000000", true, new FormClass.OnActionButtonListener() {
            @Override
            public void onClick() {
                ArrayList<String> cities = new ArrayList<>();

                for (ModelCity row: Settings.cities) {
                    cities.add(row.getCity());
                }

                final List<String> list = new ArrayList<>(cities);
                MyDialogBottom dialog = new MyDialogBottom(Settings.activity, R.style.MyDialogBottom, list, "", Settings.dilBilgisi("chose",getString(R.string.chose)), Settings.dilBilgisi("cancel",getString(R.string.cancel)), new MyDialogBottom.OnActionButtonListener() {
                    @Override
                    public void onClickList(int position) {
                        city = list.get(position);
                        addForms();
                    }
                });
                dialog.show();
            }
        });

        formClass.addFormEditMulti(Settings.dilBilgisi("form_about", getString(R.string.form_about)), "#000000", about, "#999999", Settings.dilBilgisi("form_about_hint", getString(R.string.form_about_hint)), false, new FormClass.OnEditTextListener() {
            @Override
            public void changedText(String text) {
                about = text;
            }
        });

        formClass.addLine();
        formClass.addHeader("",true);

        formClass.addButtonKosul(Settings.dilBilgisi("ghost", getResources().getString(R.string.ghost)), isGhost, true, new FormClass.OnSwichChangeListener() {
            @Override
            public void onClick() {
            }

            @Override
            public void isChecked(boolean isChecked) {
                isGhost = isChecked;
            }
        });

        formClass.addButtonKosul(Settings.dilBilgisi("notif_message", getResources().getString(R.string.notif_message)), isNotificationMessage, true, new FormClass.OnSwichChangeListener() {
            @Override
            public void onClick() {
            }

            @Override
            public void isChecked(boolean isChecked) {
                isNotificationMessage = isChecked;
            }
        });

        formClass.addButtonKosulVip(Settings.dilBilgisi("notif_game", getResources().getString(R.string.notif_game)), isNotificationGame, Settings.premium, true, new FormClass.OnSwichChangeListener() {
            @Override
            public void onClick() {
            }

            @Override
            public void isChecked(boolean isChecked) {
                isNotificationGame = isChecked;
            }
        });

        formClass.addButtonKosul(Settings.dilBilgisi("notif_visitor", getResources().getString(R.string.notif_visitor)), isNotificationVizitor, true, new FormClass.OnSwichChangeListener() {
            @Override
            public void onClick() {
            }

            @Override
            public void isChecked(boolean isChecked) {
                isNotificationVizitor = isChecked;
            }
        });

        formClass.addButtonKosul(Settings.dilBilgisi("notif_like", getResources().getString(R.string.notif_like)), isNotificationLike, false, new FormClass.OnSwichChangeListener() {
            @Override
            public void onClick() {
            }

            @Override
            public void isChecked(boolean isChecked) {
                isNotificationLike = isChecked;
            }
        });

        formClass.addLine();
        formClass.addHeader("", false);

        formClass.addFormActionButton(Settings.dilBilgisi("btn_save", getResources().getString(R.string.btn_save)), "#4695e0", new FormClass.OnActionButtonListener() {
            @Override
            public void onClick() {
                saveUser();
            }
        });

        formClass.addHeader("", false);
    }

    private void saveUser(){
        MainActivity.main.showProgress();
        String cins = "";
        switch (gender){
            case "Erkek":
                cins = "male";
                break;
            case "Kadın":
                cins = "female";
                break;
        }

        String postdata = "&name=" + PostClass.encodeString(name)
                + "&gender=" + PostClass.encodeString(cins)
                + "&birthday=" + PostClass.encodeString(birth)
                + "&city=" + PostClass.encodeString(String.valueOf(getCityId(city)))
                + "&about=" + PostClass.encodeString(about)
                + "&ghost=" +PostClass.encodeString(String.valueOf(isGhost))
                + "&notif_message=" +PostClass.encodeString(String.valueOf(isNotificationMessage))
                + "&notif_game=" +PostClass.encodeString(String.valueOf(isNotificationGame))
                + "&notif_visitor=" +PostClass.encodeString(String.valueOf(isNotificationVizitor))
                + "&notif_like=" +PostClass.encodeString(String.valueOf(isNotificationLike));

        PostClass.PostData("user/save", postdata, new PostClass.OnDataLoaded() {
            @Override
            public void onDataLoad(Boolean success, String response) {
                MainActivity.main.closeProgress();
                if (success == true) {
                    JSONObject obj = null;
                    try {
                        obj = new JSONObject(response);
                        if (obj.has("data")) {
                            JSONObject data = obj.getJSONObject("data");

                            Gson gson = new Gson();
                            ModelUser user = gson.fromJson(data.toString(), ModelUser.class);
                            Settings.user = user;

                            Settings.activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(Settings.activity, Settings.dilBilgisi("saved_informations", getString(R.string.saved_informations)), Toast.LENGTH_SHORT).show();
                                    MainActivity.main.onBackPressed();
                                }
                            });
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private int getCityId(String city){
        int id = 0;
        for (ModelCity row: Settings.cities) {
            if (row.getCity().equals(city))
                id = row.getId();
        }

        return id;
    }

}

