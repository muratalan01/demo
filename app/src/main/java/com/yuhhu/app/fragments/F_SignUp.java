package com.yuhhu.app.fragments;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.yuhhu.app.R;
import com.yuhhu.app.database.DataBaseHelper;
import com.yuhhu.app.fontsUtils.FontsText;
import com.yuhhu.app.main.MainActivity;
import com.yuhhu.app.utils.FormClass;
import com.yuhhu.app.utils.KeyBoardClose;
import com.yuhhu.app.utils.MyDialogBottom;
import com.yuhhu.app.utils.PostClass;
import com.yuhhu.app.utils.Settings;
import com.yuhhu.app.utils.SharedPreferenceClass;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class F_SignUp extends Fragment {

    LinearLayout root;
    FormClass formClass;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.f_signup, container, false);
        Typeface tf = Typeface.createFromAsset(Settings.activity.getAssets(), "fonts/Ubuntu-Regular.ttf");
        ViewGroup myMostParentLayout = (ViewGroup) rootView.findViewById(R.id.fontroot);
        FontsText.setFontToAllChilds(myMostParentLayout, tf);

        final LinearLayout btnLeft = (LinearLayout) rootView.findViewById(R.id.btn_left);
        btnLeft.setVisibility(View.VISIBLE);
        btnLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Settings.fragmentActivity.getSupportFragmentManager().beginTransaction().replace(R.id.container_extra, new F_Login()).commit();
            }
        });

        TextView text_bar = (TextView) rootView.findViewById(R.id.text_bar);
        Typeface tf_bar = Typeface.createFromAsset(Settings.activity.getAssets(), "fonts/Ubuntu-Bold.ttf");
        text_bar.setTypeface(tf_bar);
        text_bar.setVisibility(View.VISIBLE);
        text_bar.setText("Yeni Üyelik");

        root = (LinearLayout) rootView.findViewById(R.id.root);
        formClass = new FormClass(Settings.activity, root);

        addForms();

        return rootView;
    }

    String email = "", pass = "", pass2 = "", name ="", lastname ="", gender = "", birth = "";
    boolean isCheckedUyelik = false, isCheckedGizlilik = false;

    private void addForms() {
        root.removeAllViews();
        gender = gender.equals("") ? "seçiminiz" : gender ;
        birth = birth.equals("") ? "seçiminiz" : birth;

        formClass.addFormEdit(Settings.dilBilgisi("email", getResources().getString(R.string.email)), "#000000", email, "#919196", "", InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS, true, new FormClass.OnEditTextListener() {
            @Override
            public void changedText(String text) {
                email = text;
            }
        });

        formClass.addFormEdit(Settings.dilBilgisi("password", getResources().getString(R.string.password)), "#000000", pass, "#919196", "", InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD, true, new FormClass.OnEditTextListener() {
            @Override
            public void changedText(String text) {
                pass = text;
            }
        });

        formClass.addFormEdit(Settings.dilBilgisi("password_again", getResources().getString(R.string.password_again)), "#000000", pass2, "#919196", "", InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD, true, new FormClass.OnEditTextListener() {
            @Override
            public void changedText(String text) {
                pass2 = text;
            }
        });

        formClass.addFormEdit(Settings.dilBilgisi("name", getResources().getString(R.string.name)), "#000000", name, "#919196", "", true, new FormClass.OnEditTextListener() {
            @Override
            public void changedText(String text) {
                name = text;
            }
        });

        formClass.addFormButton(Settings.dilBilgisi("gender", getResources().getString(R.string.gender)), "#000000", gender, "#000000", true, new FormClass.OnActionButtonListener() {
            @Override
            public void onClick() {
                final List<String> list = new ArrayList<>(Arrays.asList(Settings.dilBilgisi("male",getString(R.string.male)),Settings.dilBilgisi("female",getString(R.string.female))));
                MyDialogBottom dialog = new MyDialogBottom(Settings.activity, R.style.MyDialogBottom, list, "", Settings.dilBilgisi("chose",getString(R.string.chose)), Settings.dilBilgisi("cancel",getString(R.string.cancel)), new MyDialogBottom.OnActionButtonListener() {
                    @Override
                    public void onClickList(int position) {
                        gender = list.get(position);
                        addForms();
                    }
                });
                dialog.show();
            }
        });

        formClass.addFormButton(Settings.dilBilgisi("birthday", getResources().getString(R.string.birthday)), "#000000", birth, "#000000", false, new FormClass.OnActionButtonListener() {
            @Override
            public void onClick() {
                ArrayList<String> yillar = new ArrayList<String>();
                for (int i = 1940; i < 2006; i++){
                    yillar.add(String.valueOf(i));
                }
                final List<String> list = new ArrayList<>(yillar);
                MyDialogBottom dialog = new MyDialogBottom(Settings.activity, R.style.MyDialogBottom, list, "", Settings.dilBilgisi("chose",getString(R.string.chose)), Settings.dilBilgisi("cancel",getString(R.string.cancel)), new MyDialogBottom.OnActionButtonListener() {
                    @Override
                    public void onClickList(int position) {
                        birth = list.get(position);
                        addForms();
                    }
                });
                dialog.show();
            }
        });

        formClass.addLine();
        formClass.addHeader("", false);
        formClass.addLine();

        formClass.addButtonKosul(Settings.dilBilgisi("signup_aggreement_membership", getResources().getString(R.string.signup_aggreement_membership)), isCheckedUyelik, true, new FormClass.OnSwichChangeListener() {
            @Override
            public void onClick() {
                F_WebView f_webView = new F_WebView();
                f_webView.page = "rules";
                Settings.fragmentActivity.getSupportFragmentManager().beginTransaction().replace(R.id.container_extra, f_webView).addToBackStack(null).commit();
            }

            @Override
            public void isChecked(boolean isChecked) {
                isCheckedUyelik = isChecked;
            }
        });
        formClass.addButtonKosul(Settings.dilBilgisi("signup_aggreement_privacy", getResources().getString(R.string.signup_aggreement_privacy)), isCheckedGizlilik, false, new FormClass.OnSwichChangeListener() {
            @Override
            public void onClick() {
                F_WebView f_webView = new F_WebView();
                f_webView.page = "privacy";
                Settings.fragmentActivity.getSupportFragmentManager().beginTransaction().replace(R.id.container_extra, f_webView).addToBackStack(null).commit();
            }

            @Override
            public void isChecked(boolean isChecked) {
                isCheckedGizlilik = isChecked;
            }
        });

        formClass.addLine();
        formClass.addHeader("", false);

        formClass.addFormActionButton(Settings.dilBilgisi("signup", getResources().getString(R.string.signup)), "#4695e0", new FormClass.OnActionButtonListener() {
            @Override
            public void onClick() {
                if (!isCheckedUyelik || !isCheckedGizlilik) {
                    Settings.showError(Settings.dilBilgisi("signup_aggreement_not_accepted", getResources().getString(R.string.signup_aggreement_not_accepted)), Settings.dilBilgisi("ok", getResources().getString(R.string.ok)));
                }else {
                    signUp();
                }
            }
        });

        formClass.addHeader("", false);
    }

    private void signUp(){
        MainActivity.main.showProgress();
        KeyBoardClose.close();
        String cins = "";
        if (gender.equals("Erkek"))
            cins = "male";
        else if (gender.equals("Kadın"))
            cins = "female";

        String postdata = "&email=" + PostClass.encodeString(email)
                + "&pass=" + PostClass.encodeString(pass)
                + "&pass2=" + PostClass.encodeString(pass2)
                + "&name=" + PostClass.encodeString(name)
                + "&gender=" + PostClass.encodeString(cins)
                + "&birthday=" + PostClass.encodeString(birth);

        PostClass.PostData("user/signup", postdata, new PostClass.OnDataLoaded() {
            @Override
            public void onDataLoad(Boolean success, String response) {
                MainActivity.main.closeProgress();
                if (success == true) {
                    JSONObject obj = null;
                    try {
                        obj = new JSONObject(response);
                        if (obj.has("data")) {
                            JSONObject data = obj.getJSONObject("data");
                            SharedPreferenceClass.saveValue("uid", data.getString("uid"));
                            Settings.uid = data.getString("uid");
                            DataBaseHelper.DATABASE_NAME = Settings.uid;
                            MainActivity.main.getMessages();

                            Settings.activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    MainActivity.main.init();
                                    getFragmentManager().beginTransaction().remove(getFragmentManager().findFragmentById(R.id.container_extra)).commit();
                                }
                            });
                        }

                        if (obj.has("result")){
                            if (obj.getString("result").equals("error")){
                                if (obj.has("message")) {
                                    JSONObject message = obj.getJSONObject("message");
                                    final String text = message.getString("text");
                                    final String btn = message.getString("button");

                                    Settings.activity.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Settings.showError(text,btn);
                                        }
                                    });
                                }
                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

}