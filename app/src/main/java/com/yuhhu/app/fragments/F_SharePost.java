package com.yuhhu.app.fragments;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.yuhhu.app.R;
import com.yuhhu.app.fontsUtils.FontsText;
import com.yuhhu.app.main.MainActivity;
import com.yuhhu.app.utils.MyDialogBottom;
import com.yuhhu.app.utils.PostClass;
import com.yuhhu.app.utils.Settings;
import com.yuhhu.app.utils_image.ModelField;
import com.yuhhu.app.utils_image.ModelFile;
import com.yuhhu.app.utils_image.MultipartUtility;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class F_SharePost extends Fragment {

    ProgressBar progressBar;
    ImageView image;
    String pathPhoto = "";
    EditText editPost;
    TextView textPostLimit;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.f_sahere_post, container, false);
        Typeface tf = Typeface.createFromAsset(Settings.activity.getAssets(), "fonts/Ubuntu-Regular.ttf");
        ViewGroup myMostParentLayout = (ViewGroup) rootView.findViewById(R.id.fontroot);
        FontsText.setFontToAllChilds(myMostParentLayout, tf);

        final LinearLayout btnLeft = (LinearLayout) rootView.findViewById(R.id.btn_left);
        btnLeft.setVisibility(View.VISIBLE);
        btnLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.main.onBackPressed();
            }
        });

        TextView text_bar = (TextView) rootView.findViewById(R.id.text_bar);
        Typeface tf_bar = Typeface.createFromAsset(Settings.activity.getAssets(), "fonts/Ubuntu-Bold.ttf");
        text_bar.setTypeface(tf_bar);
        text_bar.setVisibility(View.VISIBLE);
        text_bar.setText(Settings.dilBilgisi("share_title", getString(R.string.share_title)));

        final LinearLayout btnRight = (LinearLayout) rootView.findViewById(R.id.btn_right);
        btnRight.setVisibility(View.VISIBLE);
        btnRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final List<String> list = new ArrayList<>(Arrays.asList(Settings.dilBilgisi("take_foto", getString(R.string.take_foto)), Settings.dilBilgisi("take_album", getString(R.string.take_album))));
                MyDialogBottom dialog = new MyDialogBottom(Settings.activity, R.style.MyDialogBottom, list, "", Settings.dilBilgisi("chose", getString(R.string.chose)), Settings.dilBilgisi("cancel", getString(R.string.cancel)), new MyDialogBottom.OnActionButtonListener() {
                    @Override
                    public void onClickList(int position) {
                        switch (position) {
                            case 0:
                                MainActivity.main.take(new MainActivity.TakeListener() {
                                    @Override
                                    public void photoPath(String path) {
                                        pathPhoto = path;

                                        Bitmap bitmap = BitmapFactory.decodeFile(path);
                                        image.setImageBitmap(bitmap);
                                        image.setVisibility(View.VISIBLE);
                                    }
                                });
                                break;
                            case 1:
                                MainActivity.main.takeFromGlarey(new MainActivity.TakeListener() {
                                    @Override
                                    public void photoPath(String path) {
                                        pathPhoto = path;

                                        Bitmap bitmap = BitmapFactory.decodeFile(path);
                                        image.setImageBitmap(bitmap);
                                        image.setVisibility(View.VISIBLE);

                                    }
                                });
                                break;
                        }
                    }
                });
                dialog.show();
            }
        });

        textPostLimit = (TextView)rootView.findViewById(R.id.text_post_limit);

        progressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
        image = (ImageView) rootView.findViewById(R.id.image);
        editPost = (EditText) rootView.findViewById(R.id.edit_post);
        editPost.setFilters(new InputFilter[] { new InputFilter.LengthFilter(300) });

        editPost.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                int limit = 300 - charSequence.length();
                textPostLimit.setText(String.valueOf(limit));
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        Button btnShare = (Button) rootView.findViewById(R.id.btn_share);
        btnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pathPhoto.equals(""))
                    sendPost();
                else
                    sendPostWithPhoto(pathPhoto);
            }
        });

        return rootView;
    }


    private void sendPost() {
        String postdata = "&text=" + PostClass.encodeString(editPost.getText().toString());
        PostClass.PostData("user/post", postdata, new PostClass.OnDataLoaded() {
            @Override
            public void onDataLoad(Boolean success, String response) {
                MainActivity.main.closeProgress();
                if (success == true) {
                    JSONObject obj = null;
                    try {
                        obj = new JSONObject(response);
                        if (obj.has("result")){
                            if (obj.getString("result").equals("error")){
                                JSONObject message = obj.getJSONObject("message");
                                Settings.showError(message.getString("text"), message.getString("button"));
                            }else
                                goBack();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void sendPostWithPhoto(String path_file) {
        MainActivity.main.showProgress();

        List<ModelField> fields = new ArrayList<>();
        ModelField field = new ModelField();
        field.setKey("uid");
        field.setField(Settings.uid);
        fields.add(field);

        field = new ModelField();
        field.setKey("text");
        field.setField(editPost.getText().toString());
        fields.add(field);

        List<ModelFile> files = new ArrayList<>();
        ModelFile file = new ModelFile();
        file.setKey("photo");
        file.setFile(new File(path_file));
        files.add(file);

        MultipartUtility.postFile(Settings.api_url + "user/post", fields, files, new MultipartUtility.PostFileListener() {
            @Override
            public void response(String response) {
                MainActivity.main.closeProgress();

                JSONObject obj = null;
                try {
                    obj = new JSONObject(response);
                    if (obj.has("result")){
                        if (obj.getString("result").equals("error")){
                            JSONObject message = obj.getJSONObject("message");
                            Settings.showError(message.getString("text"), message.getString("button"));
                        }else
                            goBack();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void upload(int total, int per) {

            }

            @Override
            public void uploadfinish() {

            }

        });
    }

    private void goBack(){
        Settings.activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                F_Tab1.f.refreshList();
                MainActivity.main.onBackPressed();
            }
        });
    }
}
