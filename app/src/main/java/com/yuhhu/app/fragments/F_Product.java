package com.yuhhu.app.fragments;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.yuhhu.app.R;
import com.yuhhu.app.adapters.A_F_Product;
import com.yuhhu.app.fontsUtils.FontsText;
import com.yuhhu.app.main.MainActivity;
import com.yuhhu.app.model.ModelProduct;
import com.yuhhu.app.utils.CustomViewPager;
import com.yuhhu.app.utils.PostClass;
import com.yuhhu.app.utils.Settings;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class F_Product extends Fragment {

    View visible_view;

    A_F_Product a_f_product;
    RecyclerView r_list;

    CustomPagerAdapter mCustomPagerAdapter;
    CustomViewPager mViewPager;
    int positon1 = 0;

    View rootView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (visible_view != null) return visible_view;

        rootView = inflater.inflate(R.layout.f_product, container, false);
        Typeface tf = Typeface.createFromAsset(Settings.activity.getAssets(), "fonts/Ubuntu-Regular.ttf");
        ViewGroup myMostParentLayout = (ViewGroup) rootView.findViewById(R.id.fontroot);
        FontsText.setFontToAllChilds(myMostParentLayout, tf);

        LinearLayout btnBack = (LinearLayout) rootView.findViewById(R.id.btn_back);
        btnBack.setVisibility(View.VISIBLE);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.main.onBackPressed();
            }
        });

        TextView text_description = (TextView) rootView.findViewById(R.id.text_description);

        r_list = (RecyclerView) rootView.findViewById(R.id.r_list);
        r_list.setHasFixedSize(true);
        r_list.setLayoutManager(new LinearLayoutManager(getActivity()));

        getData();

        mViewPager = (CustomViewPager) rootView.findViewById(R.id.viewpager);
        mViewPager.setPagingEnabled(true);
        mViewPager.setOffscreenPageLimit(4);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                positon1 = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        setUpAdapterPager();

        visible_view = rootView;
        return rootView;
    }

    private void setUpAdapterPager() {
        Settings.activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mCustomPagerAdapter = new CustomPagerAdapter(Settings.activity);
                mViewPager.setAdapter(mCustomPagerAdapter);
                mViewPager.setCurrentItem(0);

                mViewPager.addOnPageChangeListener(viewPagerPageChangeListener);
                setUiPageViewController(4);
            }
        });

    }

    private LinearLayout dotsLayout;
    private int dotsCount;
    private TextView[] dots;

    private void setUiPageViewController(int size) {
        dotsLayout = (LinearLayout) rootView.findViewById(R.id.viewPagerCountDots);
        dotsLayout.removeAllViews();
        dotsCount = size;
        dots = new TextView[dotsCount];

        for (int i = 0; i < size; i++) {
            dots[i] = new TextView(Settings.activity);

            if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.N){
                dots[i].setText( Html.fromHtml("&#8226;", Html.FROM_HTML_MODE_LEGACY));
            }else{
                dots[i].setText(Html.fromHtml("&#8226;"));
            }

            dots[i].setTextSize(25);
            dots[i].setTextColor(Color.parseColor("#959191"));
            dotsLayout.addView(dots[i]);
        }

        dots[0].setTextColor(Color.parseColor("#0087b4"));
    }

    // page change listener
    CustomViewPager.OnPageChangeListener viewPagerPageChangeListener = new CustomViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            for (int i = 0; i < dotsCount; i++) {
                dots[i].setTextColor(Color.parseColor("#959191"));
            }
            dots[position].setTextColor(Color.parseColor("#0087b4"));
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };

    class CustomPagerAdapter extends PagerAdapter {

        Context mContext;
        LayoutInflater mLayoutInflater;

        public CustomPagerAdapter(Context context) {
            mContext = context;
            mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return 4;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View view = mLayoutInflater.inflate(R.layout.f_product_item2, container, false);
            Typeface tf = Typeface.createFromAsset(Settings.activity.getAssets(), "fonts/Ubuntu-Regular.ttf");
            ViewGroup myMostParentLayout = (ViewGroup) rootView.findViewById(R.id.fontroot);
            FontsText.setFontToAllChilds(myMostParentLayout, tf);

            ImageView imageView = (ImageView) view.findViewById(R.id.image);
            TextView text = (TextView)view.findViewById(R.id.text_desc);

            if (position == 0) {
                imageView.setBackgroundResource(R.drawable.vip1);
                text.setText(Settings.dilBilgisi("vip_title1", getString(R.string.vip_title1)));
            }else if (position == 1) {
                imageView.setBackgroundResource(R.drawable.vip2);
                text.setText(Settings.dilBilgisi("vip_title2", getString(R.string.vip_title2)));
            }else if (position == 2) {
                imageView.setBackgroundResource(R.drawable.vip3);
                text.setText(Settings.dilBilgisi("vip_title3", getString(R.string.vip_title3)));
            }else if (position == 3) {
                imageView.setBackgroundResource(R.drawable.vip4);
                text.setText(Settings.dilBilgisi("vip_title4", getString(R.string.vip_title4)));
            }

            container.addView(view);

            return view;

        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }
    }


    List<ModelProduct> products = new ArrayList<>();

    private void getData() {
        MainActivity.main.showProgress();

        PostClass.PostData("device/inapp", null, new PostClass.OnDataLoaded() {
            @Override
            public void onDataLoad(Boolean success, String response) {
                MainActivity.main.closeProgress();
                if (success == true) {
                    JSONObject obj = null;
                    try {
                        obj = new JSONObject(response);
                        if (obj.has("data")) {
                            JSONObject data = obj.getJSONObject("data");

                            if (data.has("inapp")) {
                                JSONArray list = data.getJSONArray("inapp");

                                Gson gson = new Gson();

                                for (int i = 0; i < list.length(); i++) {
                                    JSONObject c = list.getJSONObject(i);
                                    products.add(gson.fromJson(c.toString(), ModelProduct.class));
                                }

                                Settings.activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        setUpAdapter();
                                    }
                                });
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });

    }

    private void setUpAdapter() {
        a_f_product = new A_F_Product(products);
        r_list.setAdapter(a_f_product);
    }

}
