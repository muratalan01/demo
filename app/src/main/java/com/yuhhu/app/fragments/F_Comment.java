package com.yuhhu.app.fragments;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.yuhhu.app.R;
import com.yuhhu.app.adapters.A_F_Comment;
import com.yuhhu.app.fontsUtils.FontsText;
import com.yuhhu.app.main.MainActivity;
import com.yuhhu.app.model.ModelComment;
import com.yuhhu.app.utils.PostClass;
import com.yuhhu.app.utils.Settings;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class F_Comment extends Fragment {

    public int postId;
    public int postPosition;

    /*
        1 -> F_Tab1 deki herkes kısmının yorum sayısı yenilenecek
        2 -> F_Tab1 deki favori kısmının yorum sayısı yenilenecek
        3 -> F_Tab2detay deki yorum sayısı yenilenecek
        4 -> F_Tab5Profil deki yorum sayısı yenilenecek
     */
    public int refPageCommentCount = 0 ;

    A_F_Comment a_f_comment;
    RecyclerView r_list;
    TextView textEmpty;
    EditText editComment;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.f_comment, container, false);
        Typeface tf = Typeface.createFromAsset(Settings.activity.getAssets(), "fonts/Ubuntu-Regular.ttf");
        ViewGroup myMostParentLayout = (ViewGroup) rootView.findViewById(R.id.fontroot);
        FontsText.setFontToAllChilds(myMostParentLayout, tf);

        final LinearLayout btnLeft = (LinearLayout) rootView.findViewById(R.id.btn_left);
        btnLeft.setVisibility(View.VISIBLE);
        btnLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.main.onBackPressed();
            }
        });

        TextView text_bar = (TextView) rootView.findViewById(R.id.text_bar);
        Typeface tf_bar = Typeface.createFromAsset(Settings.activity.getAssets(), "fonts/Ubuntu-Bold.ttf");
        text_bar.setTypeface(tf_bar);
        text_bar.setVisibility(View.VISIBLE);
        text_bar.setText(Settings.dilBilgisi("title_comment", getString(R.string.title_comment)));

        r_list = (RecyclerView) rootView.findViewById(R.id.r_list);
        r_list.setHasFixedSize(true);
        r_list.setLayoutManager(new LinearLayoutManager(getActivity()));

        editComment = (EditText) rootView.findViewById(R.id.edit_comment);
        textEmpty = (TextView) rootView.findViewById(R.id.text_emty);

        TextView btnGonder = (TextView) rootView.findViewById(R.id.text_btn_gonder);
        btnGonder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendComment();
                editComment.setText("");
            }
        });

        getData();

        return rootView;
    }

    List<ModelComment> comments;

    private void getData() {
        if (comments == null) {
            comments = new ArrayList<>();
            MainActivity.main.showProgress();
        }

        String postdata = "&post_id=" + PostClass.encodeString(String.valueOf(postId));
        PostClass.PostData("profile/comments", postdata, new PostClass.OnDataLoaded() {
            @Override
            public void onDataLoad(Boolean success, String response) {
                MainActivity.main.closeProgress();
                if (success == true) {
                    JSONObject obj = null;
                    try {
                        obj = new JSONObject(response);
                        if (obj.has("data")) {
                            JSONObject data = obj.getJSONObject("data");

                            if (data.has("list")) {
                                JSONArray list = data.getJSONArray("list");

                                Gson gson = new Gson();

                                for (int i = 0; i < list.length(); i++) {
                                    JSONObject c = list.getJSONObject(i);
                                    comments.add(gson.fromJson(c.toString(), ModelComment.class));
                                }

                                Settings.activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        a_f_comment = new A_F_Comment(comments);
                                        r_list.setAdapter(a_f_comment);
                                        r_list.scrollToPosition(comments.size() - 1);
                                    }
                                });

                            }
                        } else {
                            Settings.activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (comments.size() < 1) {
                                        textEmpty.setText(Settings.dilBilgisi("empty_comment", getString(R.string.empty_comment)));
                                        textEmpty.setVisibility(View.VISIBLE);
                                    }
                                }
                            });

                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void sendComment() {
        comments.clear();

        String postdata = "&post_id=" + PostClass.encodeString(String.valueOf(postId))
                + "&text=" + PostClass.encodeString(editComment.getText().toString());
        PostClass.PostData("profile/comment_send", postdata, new PostClass.OnDataLoaded() {
            @Override
            public void onDataLoad(Boolean success, String response) {
                MainActivity.main.closeProgress();
                if (success == true) {
                    JSONObject obj = null;
                    try {
                        obj = new JSONObject(response);
                        if (obj.has("data")) {
                            JSONObject data = obj.getJSONObject("data");

                            if (data.has("list")) {
                                JSONArray list = data.getJSONArray("list");

                                Gson gson = new Gson();

                                for (int i = 0; i < list.length(); i++) {
                                    JSONObject c = list.getJSONObject(i);
                                    comments.add(gson.fromJson(c.toString(), ModelComment.class));
                                }

                                Settings.activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (a_f_comment == null) {
                                            a_f_comment = new A_F_Comment(comments);
                                            r_list.setAdapter(a_f_comment);
                                            textEmpty.setVisibility(View.GONE);
                                        } else
                                            a_f_comment.notifyDataSetChanged();

                                        r_list.scrollToPosition(comments.size() - 1);

                                        switch (refPageCommentCount){
                                            case 1:
                                                F_Tab1.f.refreshCommentCount(postPosition, comments.size());
                                                break;
                                            case 2:
                                                F_Tab1.f.refreshCommentCountFollow(postPosition, comments.size());
                                                break;
                                            case 3:
                                                F_Tab2Detay.f.refreshCommentCount(postPosition, comments.size());
                                                break;
                                            case 4:
                                                F_Tab5Profil.f.refreshCommentCount(postPosition, comments.size());
                                                break;
                                        }

                                    }
                                });

                            }
                        } else {
                            Settings.activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (comments.size() < 1) {
                                        textEmpty.setText(Settings.dilBilgisi("empty_comment", getString(R.string.empty_comment)));
                                        textEmpty.setVisibility(View.VISIBLE);
                                    }
                                }
                            });

                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

}
