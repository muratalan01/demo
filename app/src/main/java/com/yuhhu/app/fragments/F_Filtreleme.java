package com.yuhhu.app.fragments;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.yuhhu.app.R;
import com.yuhhu.app.fontsUtils.FontsText;
import com.yuhhu.app.main.MainActivity;
import com.yuhhu.app.utils.Settings;
import com.yuhhu.app.utils.SharedPreferenceClass;

import org.florescu.android.rangeseekbar.RangeSeekBar;

public class F_Filtreleme extends Fragment {

    String cinsiyet;
    int minYas;
    int maxYas;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.f_filtreleme, container, false);
        Typeface tf = Typeface.createFromAsset(Settings.activity.getAssets(), "fonts/Ubuntu-Regular.ttf");
        ViewGroup myMostParentLayout = (ViewGroup) rootView.findViewById(R.id.fontroot);
        FontsText.setFontToAllChilds(myMostParentLayout, tf);

        cinsiyet = Settings.filterCinsiyet;
        minYas = Settings.filterMinYas;
        maxYas =Settings.filterMaxYas;

        final LinearLayout btnLeft = (LinearLayout) rootView.findViewById(R.id.btn_left);
        btnLeft.setVisibility(View.VISIBLE);
        btnLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.main.onBackPressed();
            }
        });

        TextView text_bar = (TextView)rootView.findViewById(R.id.text_bar);
        Typeface tf_bar = Typeface.createFromAsset(Settings.activity.getAssets(), "fonts/Ubuntu-Bold.ttf");
        text_bar.setTypeface(tf_bar);
        text_bar.setVisibility(View.VISIBLE);
        text_bar.setText(Settings.dilBilgisi("title_filtreleme", getString(R.string.title_filtreleme)));

        final CheckBox checkBoxErkek = (CheckBox) rootView.findViewById(R.id.checkbox_male);
        final CheckBox checkBoxKadin = (CheckBox) rootView.findViewById(R.id.checkbox_female);
        final CheckBox checkBoxHepsi = (CheckBox) rootView.findViewById(R.id.checkbox_mix);

        if (Settings.filterCinsiyet.equals("male"))
            checkBoxErkek.setChecked(true);
        else if (Settings.filterCinsiyet.equals("female"))
            checkBoxKadin.setChecked(true);
        else
            checkBoxHepsi.setChecked(true);

        checkBoxErkek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkBoxErkek.isChecked()) {
                    checkBoxErkek.setChecked(true);
                    checkBoxKadin.setChecked(false);
                    checkBoxHepsi.setChecked(false);
                    cinsiyet = "male";
                } else {
                    checkBoxErkek.setChecked(false);
                }
            }
        });

        checkBoxKadin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkBoxKadin.isChecked()) {
                    checkBoxErkek.setChecked(false);
                    checkBoxKadin.setChecked(true);
                    checkBoxHepsi.setChecked(false);
                    cinsiyet = "female";
                } else {
                    checkBoxKadin.setChecked(false);
                }
            }
        });

        checkBoxHepsi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkBoxHepsi.isChecked()) {
                    checkBoxErkek.setChecked(false);
                    checkBoxKadin.setChecked(false);
                    checkBoxHepsi.setChecked(true);
                    cinsiyet = "mix";
                } else {
                    checkBoxHepsi.setChecked(false);
                }
            }
        });

        final TextView textAgeFilter = (TextView)rootView.findViewById(R.id.text_age_filter);
        textAgeFilter.setText(Settings.dilBilgisi("filter_age_range", getString(R.string.filter_age_range)).replace("[]", String.valueOf(Settings.filterMinYas + " - " + Settings.filterMaxYas)));

        // Setup the new range seek bar
        RangeSeekBar<Integer> rangeSeekBar = new RangeSeekBar<>(Settings.activity);
        // Set the range
        rangeSeekBar.setRangeValues(18, 80);
        rangeSeekBar.setSelectedMinValue(minYas);
        rangeSeekBar.setSelectedMaxValue(maxYas);
        rangeSeekBar.setTextAboveThumbsColorResource(android.R.color.holo_blue_bright);

        rangeSeekBar.setOnRangeSeekBarChangeListener(new RangeSeekBar.OnRangeSeekBarChangeListener<Integer>() {
            @Override
            public void onRangeSeekBarValuesChanged(RangeSeekBar<?> bar, Integer minValue, Integer maxValue) {
                minYas = minValue;
                maxYas = maxValue;

                textAgeFilter.setText(Settings.dilBilgisi("filter_age_range", getString(R.string.filter_age_range) ).replace("[]", String.valueOf(minValue + " - " + maxValue) ));
            }
        });

        // Add to layout
        LinearLayout layout = (LinearLayout) rootView.findViewById(R.id.seekbar_placeholder);
        layout.addView(rangeSeekBar);

        Button btnFiltrele = (Button)rootView.findViewById(R.id.btn_filtrele);
        btnFiltrele.setText(Settings.dilBilgisi("btn_filtrele", getString(R.string.btn_filtrele)));
        btnFiltrele.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Settings.filterCinsiyet = cinsiyet;
                Settings.filterMinYas = minYas;
                Settings.filterMaxYas = maxYas;

                SharedPreferenceClass.saveValue("filterCinsiyet", cinsiyet);
                SharedPreferenceClass.saveValueInt("filterMinYas", minYas);
                SharedPreferenceClass.saveValueInt("filterMaxYas", maxYas);

                MainActivity.main.onBackPressed();
                F_Tab2.f.refreshList();
            }
        });

        return rootView;
    }


}