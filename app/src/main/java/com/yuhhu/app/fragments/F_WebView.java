package com.yuhhu.app.fragments;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;

import com.yuhhu.app.R;
import com.yuhhu.app.fontsUtils.FontsText;
import com.yuhhu.app.main.MainActivity;
import com.yuhhu.app.utils.KeyBoardClose;
import com.yuhhu.app.utils.Log;
import com.yuhhu.app.utils.PostClass;
import com.yuhhu.app.utils.Settings;

public class F_WebView extends Fragment {

    public String page = "";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Settings.barChange("#dd000000");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.f_web_view, container, false);
        Typeface tf = Typeface.createFromAsset(Settings.activity.getAssets(), "fonts/Ubuntu-Regular.ttf");
        ViewGroup myMostParentLayout = (ViewGroup) rootView.findViewById(R.id.fontroot);
        FontsText.setFontToAllChilds(myMostParentLayout, tf);

        LinearLayout btnBack = (LinearLayout) rootView.findViewById(R.id.btn_back);
        btnBack.setVisibility(View.VISIBLE);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                KeyBoardClose.close();
                Settings.fragmentActivity.getSupportFragmentManager().popBackStack();
            }
        });

        String data = "&packet=" + PostClass.encodeString(Settings.packet)
                + "&device=" + PostClass.encodeString(Settings.device)
                + "&version=" + Settings.version
                + "&language=" + PostClass.encodeString(Settings.language)
                + "&country=" + PostClass.encodeString(Settings.country)
                + "&uuid=" + PostClass.encodeString(Settings.uuid)
                + "&did=" + PostClass.encodeString(Settings.did)
                + "&uid=" + PostClass.encodeString(Settings.uid);

        String url = Settings.api_url + "device/content" + data + "&page=" + page;
        Log.i("request", url);

        WebView webview = (WebView) rootView.findViewById(R.id.webview);
        webview.getSettings().setJavaScriptEnabled(true);
        webview.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                MainActivity.main.closeProgress();
            }
        });

        MainActivity.main.showProgress();
        webview.loadUrl(url);

        return rootView;
    }


    @Override
    public void onPause() {
        super.onPause();
        Settings.barDefault();
    }

}
