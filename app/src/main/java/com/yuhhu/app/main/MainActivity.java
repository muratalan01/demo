package com.yuhhu.app.main;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.yuhhu.app.BuildConfig;
import com.yuhhu.app.R;
import com.yuhhu.app.database.DataBaseHelper;
import com.yuhhu.app.fontsUtils.FontsText;
import com.yuhhu.app.fragments.F_Login;
import com.yuhhu.app.fragments.F_MesajDetay;
import com.yuhhu.app.fragments.F_Product;
import com.yuhhu.app.fragments.F_SignUp;
import com.yuhhu.app.fragments.F_Tab1;
import com.yuhhu.app.fragments.F_Tab2;
import com.yuhhu.app.fragments.F_Tab3;
import com.yuhhu.app.fragments.F_Tab4;
import com.yuhhu.app.fragments.F_Tab5;
import com.yuhhu.app.model.ModelCity;
import com.yuhhu.app.model.ModelMessage;
import com.yuhhu.app.model.ModelUser;
import com.yuhhu.app.utils.GPSTracker;
import com.yuhhu.app.utils.KeyBoardClose;
import com.yuhhu.app.utils.MyDialogCenter;
import com.yuhhu.app.utils.PermissionUtil;
import com.yuhhu.app.utils.PostClass;
import com.yuhhu.app.utils.Settings;
import com.yuhhu.app.utils.SharedPreferenceClass;
import com.yuhhu.app.utils_image.ImageFilePath;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    RelativeLayout imageT1, imageT2, imageT3, imageT4, imageT5;
    TextView textT1, textT2, textT3, textT4, textT5;
    RelativeLayout r_ProgresBar;
    RelativeLayout r_splash;

    public TabBarController barController;
    public static MainActivity main;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/Ubuntu-Regular.ttf");
        ViewGroup myMostParentLayout = (ViewGroup) findViewById(R.id.fontroot);
        FontsText.setFontToAllChilds(myMostParentLayout, tf);

        main = this;

        Settings.activity = this;
        Settings.fragmentActivity = this;

        if (Settings.language.equals("") || Settings.language == null)
            Settings.cihazBilgileri(this);
        Settings.did = SharedPreferenceClass.getValue("did", "");
        Settings.uid = SharedPreferenceClass.getValue("uid", "");

        Settings.filterCinsiyet = SharedPreferenceClass.getValue("filterCinsiyet", "female");
        Settings.filterMinYas = SharedPreferenceClass.getValueInt("filterMinYas", 18);
        Settings.filterMaxYas = SharedPreferenceClass.getValueInt("filterMaxYas", 80);

        Settings.push_register();

        r_ProgresBar = (RelativeLayout) findViewById(R.id.r_progressBar);
        r_splash = (RelativeLayout) findViewById(R.id.r_splash);

        register();

    }

    private void register() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                regis();
            }
        }, 1000);
    }

    private void regis() {
        PostClass.PostData("device/register", null, new PostClass.OnDataLoaded() {
            @Override
            public void onDataLoad(Boolean success, String response) {
                if (success == true) {
                    try {
                        JSONObject obj = new JSONObject(response);
                        if (obj.has("data")) {
                            JSONObject data = obj.getJSONObject("data");

                            if (data.has("did")) {
                                SharedPreferenceClass.saveValue("did", data.getString("did"));
                                Settings.did = data.getString("did");
                            }
                            if (data.has("banner")) {
                                SharedPreferenceClass.saveValue("banner", data.getString("banner"));
                                Settings.banner = data.getString("banner");
                            }
                            if (data.has("inters")) {
                                SharedPreferenceClass.saveValue("inters", data.getString("inters"));
                                Settings.inters = data.getString("inters");
                            }
                            if (data.has("rate")) {
                                SharedPreferenceClass.saveValueInt("rate", data.getInt("rate"));
                                Settings.rate = data.getInt("rate");
                            }
                            if (data.has("premium")) {
                                SharedPreferenceClass.saveValueBoolean("premium", data.getBoolean("premium"));
                                Settings.premium = data.getBoolean("premium");
                            }
                        }

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                dil_ayarla();
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                }
            }
        });
    }

    public void dil_ayarla() {
        PostClass.PostData("device/language", null, new PostClass.OnDataLoaded() {
            @Override
            public void onDataLoad(Boolean success, String response) {
                JSONObject obj = null;
                if (success == true) {
                    try {
                        obj = new JSONObject(response);
                        if (obj.has("data")) {
                            JSONObject js = obj.getJSONObject("data");

                            Iterator keys = js.keys();

                            while (keys.hasNext()) {
                                // loop to get the dynamic key
                                String key = (String) keys.next();

                                SharedPreferenceClass.saveValue(key, js.getString(key));
                            }

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (Settings.uid.equals(""))
                                        getSupportFragmentManager().beginTransaction().replace(R.id.container_extra, new F_Login()).commit();
                                    else
                                        init();

                                    r_splash.setVisibility(View.GONE);
                                }
                            });
                        }
                    } catch (Exception v) {
                        v.printStackTrace();
                    }
                }
            }
        });
    }

    public void init() {
        Fragment[] fragments = new Fragment[5];
        fragments[0] = new F_Tab1();
        fragments[1] = new F_Tab2();
        fragments[2] = new F_Tab3();
        fragments[3] = new F_Tab4();
        fragments[4] = new F_Tab5();

        barController = new TabBarController(this, R.id.container, fragments);

        imageT1 = (RelativeLayout) findViewById(R.id.image_t0);
        imageT2 = (RelativeLayout) findViewById(R.id.image_t1);
        imageT3 = (RelativeLayout) findViewById(R.id.image_t2);
        imageT4 = (RelativeLayout) findViewById(R.id.image_t3);
        imageT5 = (RelativeLayout) findViewById(R.id.image_t4);

        imageT1.performClick();

        textT1 = (TextView) findViewById(R.id.text_i0);
        textT2 = (TextView) findViewById(R.id.text_i1);
        textT3 = (TextView) findViewById(R.id.text_i2);
        textT4 = (TextView) findViewById(R.id.text_i3);
        textT5 = (TextView) findViewById(R.id.text_i4);


        textT1.setText(Settings.dilBilgisi("tab_1", getString(R.string.tab_1)));
        textT2.setText(Settings.dilBilgisi("tab_2", getString(R.string.tab_2)));
        textT3.setText(Settings.dilBilgisi("tab_3", getString(R.string.tab_3)));
        textT4.setText(Settings.dilBilgisi("tab_4", getString(R.string.tab_4)));
        textT5.setText(Settings.dilBilgisi("tab_5", getString(R.string.tab_5)));

        getUser();
        getGPS();
    }

    int sayfa = 1;

    public void tabclicks(View view) {
        int selected = view.getId();
        int yer = 0;

        switch (selected) {
            case R.id.image_t0:
                yer = 0;
                imageT1.setSelected(true);
                imageT2.setSelected(false);
                imageT3.setSelected(false);
                imageT4.setSelected(false);
                imageT5.setSelected(false);

                if (F_Tab1.f !=null){
                    if (F_Tab1.f.isRefreshFollow) {
                        F_Tab1.f.refreshFollow();
                        F_Tab1.f.isRefreshFollow = false;
                    }
                }

                break;
            case R.id.image_t1:
                yer = 1;
                imageT1.setSelected(false);
                imageT2.setSelected(true);
                imageT3.setSelected(false);
                imageT4.setSelected(false);
                imageT5.setSelected(false);

                break;
            case R.id.image_t2:
                yer = 2;
                imageT1.setSelected(false);
                imageT2.setSelected(false);
                imageT3.setSelected(true);
                imageT4.setSelected(false);
                imageT5.setSelected(false);
                break;
            case R.id.image_t3:
                yer = 3;
                imageT1.setSelected(false);
                imageT2.setSelected(false);
                imageT3.setSelected(false);
                imageT4.setSelected(true);
                imageT5.setSelected(false);
                break;
            case R.id.image_t4:
                yer = 4;
                imageT1.setSelected(false);
                imageT2.setSelected(false);
                imageT3.setSelected(false);
                imageT4.setSelected(false);
                imageT5.setSelected(true);
                break;
        }

        if (sayfa == yer) {
            barController.clearTabToFirst();
        }

        sayfa = yer;
        barController.changeTab(yer);
    }

    public void clearTabToFirst() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                barController.clearTabToFirst();
                barController.changeTab(sayfa);
            }
        });
    }

    public void sendMessage(String postdata) {
        PostClass.PostData("message/send", postdata, new PostClass.OnDataLoaded() {
            @Override
            public void onDataLoad(Boolean success, String response) {
                if (success == true) {
                    JSONObject obj = null;
                    try {
                        obj = new JSONObject(response);
                        if (obj.has("data")) {
                            JSONObject data = obj.getJSONObject("data");

                            Gson gson = new Gson();

                            ModelMessage message = gson.fromJson(data.toString(), ModelMessage.class);

                            DataBaseHelper db = new DataBaseHelper(Settings.activity);
                            for (ModelMessage.User row : message.getUser()) {
                                db.addUser(row);
                            }

                            for (ModelMessage.MessageList row : message.getList()) {
                                db.addMessage(row);

                                try {
                                    F_MesajDetay fr = (F_MesajDetay) Settings.fragmentActivity.getSupportFragmentManager().findFragmentById(R.id.container_extra);
                                    if (fr != null && fr.isVisible()) {
                                        if (fr.getUser() == row.getUser())
                                            fr.getMessagesDb();
                                        else
                                            playGetMesaj();
                                    } else
                                        playGetMesaj();
                                } catch (Exception e) {
                                }
                            }

                            db.closeDB();

                            if (!message.getLast_id().equals(""))
                                messageSync(message.getLast_id());

                        }

                        if (obj.has("result")){
                            if (obj.getString("result").equals("error") && obj.has("vip")){
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        List<String> list = new ArrayList<>(Arrays.asList(Settings.dilBilgisi("btn_vip", getString(R.string.btn_vip)), Settings.dilBilgisi("cancel", getString(R.string.cancel)))); // butonlar
                                        MyDialogCenter dialog = new MyDialogCenter(Settings.activity, R.style.MyDialogCenter, "!", "", Settings.dilBilgisi("", getString(R.string.vip_title_dialog_message)), list, new MyDialogCenter.OnActionButtonListener() {
                                            @Override
                                            public void onClickList(int position) {
                                                if (position == 0){
                                                    Settings.fragmentActivity.getSupportFragmentManager().beginTransaction().add(R.id.container_extra, new F_Product()).addToBackStack("").commit();
                                                }
                                            }

                                            @Override
                                            public void onClickListWithEdit(int position, String text) {

                                            }
                                        });
                                        dialog.show();
                                    }
                                });
                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    public void getMessages() {
        PostClass.PostData("message/get", null, new PostClass.OnDataLoaded() {
            @Override
            public void onDataLoad(Boolean success, String response) {
                if (success == true) {
                    JSONObject obj = null;
                    try {
                        obj = new JSONObject(response);
                        if (obj.has("data")) {
                            JSONObject data = obj.getJSONObject("data");

                            Gson gson = new Gson();

                            ModelMessage message = gson.fromJson(data.toString(), ModelMessage.class);

                            DataBaseHelper db = new DataBaseHelper(Settings.activity);
                            for (ModelMessage.User row : message.getUser()) {
                                db.addUser(row);
                            }

                            for (ModelMessage.MessageList row : message.getList()) {
                                db.addMessage(row);

                                try {
                                    F_MesajDetay fr = (F_MesajDetay) Settings.fragmentActivity.getSupportFragmentManager().findFragmentById(R.id.container_extra);
                                    if (fr != null && fr.isVisible()) {
                                        if (fr.getUser() == row.getUser())
                                            fr.getMessagesDb();
                                        else
                                            playGetMesaj();
                                    } else
                                        playGetMesaj();
                                } catch (Exception e) {
                                }

                                if (sayfa == 1) {
                                    F_Tab3.f.sonMesajlar();
                                }
                            }

                            db.closeDB();

                            if (!message.getLast_id().equals(""))
                                messageSync(message.getLast_id());

                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void messageSync(String last_id) {
        String postdata = "&last_id=" + PostClass.encodeString(last_id);
        PostClass.PostData("message/sync", postdata, new PostClass.OnDataLoaded() {
            @Override
            public void onDataLoad(Boolean success, String response) {
                if (success == true) {
                    JSONObject obj = null;
                    try {
                        obj = new JSONObject(response);
                        if (obj.has("data")) {
                            JSONObject data = obj.getJSONObject("data");
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    MediaPlayer mediaPlayer;

    public void playGetMesaj() {
        Settings.activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mediaPlayer != null) {
                    if (mediaPlayer.isPlaying())
                        return;
                } else {
                    mediaPlayer = new MediaPlayer();
                    mediaPlayer = MediaPlayer.create(Settings.activity, R.raw.message_beep);
                    mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                    mediaPlayer.setLooping(false);
                }

                //mediaPlayer.start();
            }
        });

    }

    private void getUser() {
        PostClass.PostData("user/account", null, new PostClass.OnDataLoaded() {
            @Override
            public void onDataLoad(Boolean success, String response) {
                if (success == true) {
                    JSONObject obj = null;
                    try {
                        obj = new JSONObject(response);
                        if (obj.has("data")) {
                            JSONObject data = obj.getJSONObject("data");

                            Gson gson = new Gson();
                            ModelUser user = gson.fromJson(data.toString(), ModelUser.class);
                            Settings.user = user;

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    PostClass.PostData("device/cities", null, new PostClass.OnDataLoaded() {
                                        @Override
                                        public void onDataLoad(Boolean success, String response) {
                                            if (success == true) {
                                                JSONObject obj = null;
                                                try {
                                                    obj = new JSONObject(response);
                                                    if (obj.has("data")) {
                                                        JSONObject data = obj.getJSONObject("data");

                                                        Gson gson = new Gson();

                                                        if (data.has("list")) {
                                                            JSONArray list = data.getJSONArray("list");

                                                            Settings.cities.clear();
                                                            for (int i = 0; i < list.length(); i++) {
                                                                JSONObject c = list.getJSONObject(i);

                                                                Settings.cities.add(gson.fromJson(c.toString(), ModelCity.class));
                                                            }
                                                        }

                                                    }

                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        }
                                    });
                                }
                            });
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        Settings.activity = this;
        Settings.device_islem("onResume");

        getMessages();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Settings.device_islem("onPause");
    }

    public void showProgress() {
        Settings.activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                r_ProgresBar.setVisibility(View.VISIBLE);
            }
        });
    }

    public void closeProgress() {
        Settings.activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                r_ProgresBar.setVisibility(View.INVISIBLE);
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (r_ProgresBar.getVisibility() == View.VISIBLE)
            return;

        KeyBoardClose.close();
        try {
            F_SignUp frl = (F_SignUp) getSupportFragmentManager().findFragmentById(R.id.container_extra);
            if (frl != null && frl.isVisible()) {
                Settings.fragmentActivity.getSupportFragmentManager().beginTransaction().replace(R.id.container_extra, new F_Login()).commit();
                return;
            }
        } catch (Exception e) {
        }

        try {
            F_Login frl = (F_Login) getSupportFragmentManager().findFragmentById(R.id.container_extra);
            if (frl != null && frl.isVisible()) {
                super.onBackPressed();
                return;
            }
        } catch (Exception e) {
        }

        try {
            Fragment fr = getSupportFragmentManager().findFragmentById(R.id.container_extra);

            if (fr != null && fr.isVisible()) {
                fr.getFragmentManager().popBackStack();
                return;
            }
        } catch (Exception e) {
        }

        if (barController.popBackFragment() == true) return;
        super.onBackPressed();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_IMAGE_REQUEST1) {
            if (resultCode != 0) {
                listener.photoPath(path_image);
            }
        } else if (requestCode == PICK_IMAGE_REQUEST2) {
            if (resultCode == Activity.RESULT_OK && data != null) {
                try {
                    final String selectedImagePath;
                    Uri selectedImageUri = data.getData();

                    //MEDIA GALLERY
                    selectedImagePath = ImageFilePath.getPath(this, selectedImageUri);
                    listener.photoPath(selectedImagePath);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        if (Settings.inappodeme == null) return;
        if (!Settings.inappodeme.mHelper.handleActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }


    // rfotoğraf işlemleri

    public interface TakeListener {
        void photoPath(String path);
    }

    private final int PERMISSIONS_CAMERA_GALERY_REQUEST = 1;
    private String[] PERMISSIONS_CAMERA_GALERY = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};

    int PICK_IMAGE_REQUEST1 = 1;
    int PICK_IMAGE_REQUEST2 = 2;

    TakeListener listener;

    public void take(TakeListener lis) {
        listener = lis;
        if (PermissionUtil.hasSelfPermission(this, PERMISSIONS_CAMERA_GALERY)) {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            takePictureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

            if (takePictureIntent.resolveActivity(MainActivity.main.getPackageManager()) != null) {
                File photoFile = null;

                try {
                    photoFile = createImageFile();
                } catch (IOException ex) {
                }

                if (photoFile != null) {
                    Uri photoURI = FileProvider.getUriForFile(MainActivity.this, BuildConfig.APPLICATION_ID + ".provider", photoFile);

                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                    startActivityForResult(takePictureIntent, PICK_IMAGE_REQUEST1);
                }
            }
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(PERMISSIONS_CAMERA_GALERY, PERMISSIONS_CAMERA_GALERY_REQUEST);
            }
        }
    }

    public void takeFromGlarey(TakeListener lis) {
        listener = lis;

        if (PermissionUtil.hasSelfPermission(this, PERMISSIONS_CAMERA_GALERY)) {
            Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(i, PICK_IMAGE_REQUEST2);
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(PERMISSIONS_CAMERA_GALERY, PERMISSIONS_CAMERA_GALERY_REQUEST);
            }
        }
    }


    String path_image = "";

    private File createImageFile() throws IOException {
        path_image = null;
        // Create an image file name
        String imageFileName = "resim";
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        path_image = image.getAbsolutePath();
        galleryAddPic();
        return image;
    }

    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(path_image);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        MainActivity.main.sendBroadcast(mediaScanIntent);
    }


    Context context;
    GPSTracker gps;

    public void getGPS() {
        context = this;
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);

        } else {
            gps = new GPSTracker(context, this);

            // Check if GPS enabled
            if (gps.canGetLocation()) {

                double latitude = gps.getLatitude();
                double longitude = gps.getLongitude();

                upDateLocation(latitude, longitude);
            } else {
                // Can't get location.
                // GPS or network is not enabled.
                // Ask user to enable GPS/network in settings.
                gps.showSettingsAlert();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the

                    // contacts-related task you need to do.

                    gps = new GPSTracker(context, this);

                    // Check if GPS enabled
                    if (gps.canGetLocation()) {

                        double latitude = gps.getLatitude();
                        double longitude = gps.getLongitude();

                        upDateLocation(latitude, longitude);
                    } else {
                        // Can't get location.
                        // GPS or network is not enabled.
                        // Ask user to enable GPS/network in settings.
                        gps.showSettingsAlert();
                    }

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
        }
    }

    private void upDateLocation(Double lat, Double lon) {
        Settings.lat = Double.toString(lat);
        Settings.lon = Double.toString(lon);

        String postdata = "&lat=" + PostClass.encodeString(Double.toString(lat))
                + "&lon=" + PostClass.encodeString(Double.toString(lon));
        PostClass.PostData("user/location", postdata, new PostClass.OnDataLoaded() {
            @Override
            public void onDataLoad(Boolean success, String response) {
                if (success == true) {
                    JSONObject obj = null;
                    try {
                        obj = new JSONObject(response);
                        if (obj.has("data")) {
                            JSONObject data = obj.getJSONObject("data");
                        }

                    } catch (Exception e) {
                    }
                }
            }
        });
    }

}
