package com.yuhhu.app.main;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;

import java.util.ArrayList;

public class TabBarController {

    private ArrayList<Fragment> fragments[];
    int active_tab = 0;

    FragmentActivity fragmentActivity;
    int container;

    public TabBarController(FragmentActivity fragmentActivity, int container, Fragment[] f) {
        this.fragmentActivity = fragmentActivity;
        this.container = container;

        fragments = new ArrayList[f.length];
        for (int i = 0; i < f.length; i++) {
            fragments[i] = new ArrayList<>();
            fragments[i].add(f[i]);
        }

    }

    public void addFragment(Fragment fragment) {
        fragments[active_tab].add(fragment);
        fragmentActivity.getSupportFragmentManager().beginTransaction().replace(container, fragment).commit();
    }

    public void changeTab(int id) {
        active_tab = id;
        int active_index = fragments[active_tab].size() - 1;
        Fragment active_fragment = fragments[active_tab].get(active_index);
        fragmentActivity.getSupportFragmentManager().beginTransaction().replace(container, active_fragment).commit();
    }

    public Fragment getActiveFragment() {
        int active_index = fragments[active_tab].size() - 1;
        Fragment active_fragment = fragments[active_tab].get(active_index);

        return active_fragment;
    }

    public void clearTabToFirst() {
        int last_index = fragments[active_tab].size() - 1;
        for (int i = last_index; i > 0; i--) {
            fragments[active_tab].remove(i);
        }

        int active_index = fragments[active_tab].size() - 1;
        Fragment active_fragment = fragments[active_tab].get(active_index);
        fragmentActivity.getSupportFragmentManager().beginTransaction().replace(container, active_fragment).commit();
    }

    public boolean popBackFragment() {
        int last_index = fragments[active_tab].size() - 1;
        if (last_index == 0) return false;

        fragments[active_tab].remove(last_index);
        int active_index = last_index - 1;
        Fragment active_fragment = fragments[active_tab].get(active_index);
        fragmentActivity.getSupportFragmentManager().beginTransaction().replace(container, active_fragment).commit();

        return true;
    }


}
