package com.yuhhu.app.push;

import android.content.Context;
import android.os.AsyncTask;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.yuhhu.app.utils.Settings;

public class RegisterApp extends AsyncTask<Void, Void, String> {

    Context ctx;
    GoogleCloudMessaging gcm;
    String PROJECT_ID;
    String regid = null;

    public RegisterApp(Context ctx, GoogleCloudMessaging gcm, String project_id){
        this.ctx = ctx;
        this.gcm = gcm;
        this.PROJECT_ID = project_id;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(Void... arg0) {
        try {
            if (gcm == null) {
                gcm = GoogleCloudMessaging.getInstance(ctx);
            }

            regid = gcm.register(PROJECT_ID);

            Settings.push_token_alindi(regid);
            return null;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
    }

}
